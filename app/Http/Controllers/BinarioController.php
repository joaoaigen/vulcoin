<?php

namespace App\Http\Controllers;

use App\Rendimentos;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Binario;
use App\User;
use App\extratos;
use App\Pacote;
use App\Referrals;
use App\config;
use DB;

class BinarioController extends Controller {

    public $hue;
    public $ids;

    public function index() {
        
        $pacote = new Pacote();
        $Bin = new Binario();
        $usr = new User();
        $config = new config();
        $config = $config->getConfig();
        
        $usuarios = User::where('ativo', 1)->get();

        foreach ($usuarios as $value) {
            //paga binário
            $pacote = DB::table('plano_investimento')->where('usuario', '=', $value['id'])->where('porcentagem_atual', '<', 200)->first();
            
            if ($usr->qualificado($value['id']) && isset($pacote->usuario)) {
                
                $BinarioEsq = $Bin->totalEsquerda($value['id']);
                $BinarioDireita = $Bin->totalDireita($value['id']);
                $pacote = Pacote::where('id', $value['pacote'])->first();
                $binario_val = $pacote['valor_ponto_binario'];

                $teto = $pacote['teto_diario'];
                if ($BinarioDireita > $BinarioEsq and $pacote['binario'] == 1) {
                    $valorBinario = ($BinarioEsq / 100) * $binario_val;

                    if ($valorBinario > $teto) {
                        $valorBinario = $teto;
                    }
                   
                    $investimento = DB::table('plano_investimento')->where('usuario', '=', $value['id'])->where('porcentagem_atual', '<', 200)->first();

                    if (isset($investimento)) {
                        $valor = $valorBinario + $investimento->valor_investido;

                        $valor_maximo = ($investimento->valor_pacote / 100) * 200;

                        if ($valor < $valor_maximo) {
                            //$porcentagem = (int) ($valor / ( ($valor_maximo / 100) * 1)) + (int) $investimento->porcentagem_atual;
                            $porcentagem = $valor / (($investimento->valor_pacote / 100) * 1);
                            if($porcentagem > 200){
                                DB::table('plano_investimento')->where('id', '=', $investimento->id)->update([
                                    'porcentagem_atual' => 200,
                                    'valor_investido' => $valor,
                                ]);
                                
                                $total = $valor_maximo - $investimento->valor_investido;
                                
                                if ($total != 0) {
                                   // $usr->addSaldo($value['id'], $total , "Investimento: " . $investimento->id);
                                }
                            }else{                                
                                DB::table('plano_investimento')->where('id', '=', $investimento->id)->update([
                                    'porcentagem_atual' => $porcentagem,
                                    'valor_investido' => $valor,
                                ]);
                                
                                $total = $valor_maximo - $investimento->valor_investido;
                                
                                if ($total != 0) {
                                   // $usr->addSaldo($value['id'], $total , "Investimento: " . $investimento->id);
                                }
                            }                            
                        } else {
                            DB::table('plano_investimento')->where('id', '=', $investimento->id)->update([
                                'porcentagem_atual' => $investimento->porcentagem_maxima,
                                'valor_investido' => $valor_maximo ,
                            ]);
                            $total = $valor_maximo - $investimento->valor_investido;
                            
                            if ($total != 0) {
                               // $usr->addSaldo($value['id'], $total, "Investimento: " . $investimento->id);
                            }
                        }
                    }


                    $usr->removePontos($value['id'], $BinarioEsq, 'esquerda');
                    $usr->addSaldo($value['id'], $valorBinario, "Bônus Binário(esquerda)");
                    $usr->removeSaldo(1, $valorBinario - ($valorBinario * $config->binario_valor), 'Bônus Binário(' . $value['username'] . ')');
                    $usr->removePontos($value['id'], $BinarioEsq, 'direita');
                } else if ($BinarioEsq > $BinarioDireita and $pacote['binario'] == 1) {
                    $valorBinario = ($BinarioDireita / 100) * $binario_val;
                    if ($valorBinario > $teto) {
                        $valorBinario = $teto;
                    }

                    $investimento = DB::table('plano_investimento')->where('usuario', '=', $value['id'])->where('porcentagem_atual', '<', 200)->first();

                    if (isset($investimento)) {
                        $valor = $valorBinario + $investimento->valor_investido;

                        $valor_maximo = ($investimento->valor_pacote / 100) * 200;

                        if ($valor < $valor_maximo) {
                           $porcentagem = $valor / (($investimento->valor_pacote / 100) * 1);

                            if($porcentagem > 200){
                                DB::table('plano_investimento')->where('id', '=', $investimento->id)->update([
                                    'porcentagem_atual' => 200,
                                    'valor_investido' => $valor,
                                ]);
                                
                                $total = $valor_maximo - $investimento->valor_investido;
                                
                                if ($total != 0) {
                                   // $usr->addSaldo($value['id'], $total, "Investimento: " . $investimento->id);
                                }
                            }else{
                                DB::table('plano_investimento')->where('id', '=', $investimento->id)->update([
                                    'porcentagem_atual' => $porcentagem,
                                    'valor_investido' => $valor,
                                ]);
                                
                                $total = $valor_maximo - $investimento->valor_investido;
                                
                                if ($total != 0) {
                                    //$usr->addSaldo($value['id'], $total, "Investimento: " . $investimento->id);
                                }
                            }                            
                        } else {
                            DB::table('plano_investimento')->where('id', '=', $investimento->id)->update([
                                'porcentagem_atual' => $investimento->porcentagem_maxima,
                                'valor_investido' => $valor_maximo ,
                            ]);
                            
                            $total = $valor_maximo - $investimento->valor_investido;
                            
                            if ($total != 0) {
                                //$usr->addSaldo($value['id'], $total, "Investimento: " . $investimento->id);
                            }
                        }
                    }


                    $usr->removePontos($value['id'], $BinarioDireita, 'direita');
                    $usr->addSaldo($value['id'], $valorBinario, "Bônus Binário(direita)");
                    $usr->removeSaldo(1, $valorBinario - ($valorBinario * $config->binario_valor), 'Bônus Binário(' . $value['username'] . ')');
                    $usr->removePontos($value['id'], $BinarioDireita, 'esquerda');
                } else if ($BinarioEsq == $BinarioDireita and $pacote['binario'] == 1) {
                    $valorBinario = ($BinarioDireita / 100) * $binario_val;
                    if ($valorBinario > $teto) {
                        $valorBinario = $teto;
                    }

                    $investimento = DB::table('plano_investimento')->where('usuario', '=', $value['id'])->where('porcentagem_atual', '<', 200)->first();

                    if (isset($investimento)) {
                        $valor = $valorBinario + $investimento->valor_investido;

                        $valor_maximo = ($investimento->valor_pacote / 100) * 200;

                        if ($valor < $valor_maximo) {
                            $porcentagem = $valor / (($investimento->valor_pacote / 100) * 1);

                            if($porcentagem > 200){
                                DB::table('plano_investimento')->where('id', '=', $investimento->id)->update([
                                    'porcentagem_atual' => 200,
                                    'valor_investido' => $valor,
                                ]);
                                
                                $total = $valor_maximo - $investimento->valor_investido;
                                
                                if ($total != 0) {
                                    //$usr->addSaldo($value['id'], $total, "Investimento: " . $investimento->id);
                                }
                            }else{
                                DB::table('plano_investimento')->where('id', '=', $investimento->id)->update([
                                    'porcentagem_atual' => $porcentagem,
                                    'valor_investido' => $valor,
                                ]);
                                
                                $total = $valor_maximo - $investimento->valor_investido;
                                
                                if ($total != 0) {
                                   // $usr->addSaldo($value['id'], $total, "Investimento: " . $investimento->id);
                                }
                            }                            
                        } else {
                            DB::table('plano_investimento')->where('id', '=', $investimento->id)->update([
                                'porcentagem_atual' => $investimento->porcentagem_maxima,
                                'valor_investido' => $valor_maximo ,
                            ]);
                            
                            $total = $valor_maximo - $investimento->valor_investido;
                            
                            if ($total != 0) {
                                //$usr->addSaldo($value['id'], $total, "Investimento: " . $investimento->id);
                            }
                        }
                    }

                    $usr->removePontos($value['id'], $BinarioDireita, 'direita');
                    $usr->addSaldo($value['id'], $valorBinario, "Bônus Binário(direita)");
                    $usr->removeSaldo(1, $valorBinario - ($valorBinario * $config->binario_valor), 'Bônus Binário(' . $value['username'] . ')');
                    $usr->removePontos($value['id'], $BinarioDireita, 'esquerda');
                }
            }
        }

        return redirect(url('admin/home?infoBin=1'));
    }

    public function powerDirect(Request $request) {
        $pacote = new Pacote();
        $pacote->expirados();
        $valor = $request->valor;
        $usr = new User();
        if (!is_numeric($valor)) {
            echo 'Operação não realizada.';
            exit();
        }
        if ($valor == '') {
            echo 'Operação não realizada.';
            exit();
        }
        $users = User::where('ativo', 1)->where('pacote', $request->pacote_div)->get();
        foreach ($users as $value) {

            if ($value['renda_variavel'] == 1) {
                $pacote = Pacote::where('id', $value['pacote'])->first();
                $pacoteValor = str_replace('.', '', $pacote['valor']);
                $pacoteValor = str_replace(',', '.', $pacoteValor);
                $valorDv = ($pacoteValor) * ($valor / 100);
                $usr->addSaldo($value['id'], ($valorDv * 0.5), 'PowerDirect');
                User::where('id', $value['id'])->update(['carteira_b' => $value['carteira_b'] + ($valorDv * 0.5), 'qntd_divisoes' => ($value['qntd_divisoes'] - 1)]);
                $usr->removeSaldo(1, $valorDv, 'PowerDirect(' . $value['username'] . ')');
            }
        }
        echo 'Operação realizada com sucesso.';
    }

    public function divideLucro(Request $request) {
        $pacote = new Pacote();
        $pacote->expirados();
        $valor = $request->valor;
        $usr = new User();
        if (!is_numeric($valor)) {
            echo 'Operação não realizada.';
            exit();
        }
        if ($valor == '') {
            echo 'Operação não realizada.';
            exit();
        }
        $users = User::where('ativo', 1)->where('pacote', $request->pacote_div)->get();
        foreach ($users as $value) {

            if ($value['renda_variavel'] == 1) {
                $pacote = Pacote::where('id', $value['pacote'])->first();
                $pacoteValor = str_replace('.', '', $pacote['valor']);
                $pacoteValor = str_replace(',', '.', $pacoteValor);
                $valorDv = ($pacoteValor) * ($valor / 100);
                $usr->addSaldo($value['id'], ($valorDv * 0.5), 'Bônus PLS');
                User::where('id', $value['id'])->update(['carteira_b' => $value['carteira_b'] + ($valorDv * 0.5), 'qntd_divisoes' => ($value['qntd_divisoes'] - 1)]);
                $usr->removeSaldo(1, $valorDv, 'Bônus PLS(' . $value['username'] . ')');
            }
        }
        echo 'Operação realizada com sucesso.';
    }

}
?>

