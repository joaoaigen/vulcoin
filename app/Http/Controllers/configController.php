<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\config;
use Validator;
use Symfony\Component\DomCrawler\Form;

class configController extends Controller {

    public function index() {
        $config = config::where('id', 1)->first();
        $config['config'] = $config;
        return view('admin.pages.config', $config);
    }

    public function update(Request $request) {

        $data = $request->all();
        /*
          Array ( [id] => 1 [site_name] => Seven7club [site_url] => [logo_1] =>
         * [logo_2] => [logo_3] => [binario_valor] => 0.00 [indicacao] => 0.00 [limite_binario] => 6 [smtp_port] => 0 [smtp_host] => [smtp_user] => [smtp_pass] => 
         * [permitir_saque] => [saque_minimo] => [iugu_token] => [iugu_ambiente] => [gnt_status] => [iugu_status] => [deposito_status] =>
         */
        unset($data['_token']);
        unset($data['_method']);        
        
        $dados = $data;
        
        
        $conf = config::where('id', 1)->update($dados);

       
        if ($conf) {
            return redirect('/admin/config')
                            ->with('msg', 'Configurações atualizadas')
                            ->with('status', 200);
        } else {
            return redirect('/admin/config')
                            ->withErrors(['Não foi possivel atualizar, tente novamente.']);
        }
    }

}
