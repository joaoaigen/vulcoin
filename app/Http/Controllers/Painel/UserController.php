<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\SaqueRequest;
use App\Pacote;
use App\Referrals;
use App\Rendimentos;
use App\Saque;
use App\SaqueLog;
use App\User;
use App\Voucher;
use App\Binario;
use Illuminate\Http\Request;
use Validator;
use Symfony\Component\DomCrawler\Form;
use App\config;
use Mail;
use App\Trade;
use Hash;
use App\Http\Controllers\Admin\VoucherController;
use Input;
use Session;
use Redirect;
use App\extratos;
use Auth;
use DB;
use Google2FA;

class UserController extends Controller
{

    public function enable2fa(Request $request) {
        
       $valid = Google2FA::verifyKey(Auth::user()->google2fa_secret, $request->get('code'));
       
       
       if($valid == true){
           //aa
           User::where('id', '=', Auth::user()->id)->update(['auth2fa' => 1]);
           session(['google2fa', 'true']);
           return redirect('/painel/home');
       }else{
           User::where('id', '=', Auth::user()->id)->update(['auth2fa' => 0]);
           Auth::logout();
           
           return redirect('/painel/login');
       }      
    }
    
    
    public function reset2fa() {
        $user = \Auth::user();

        $google2fa = app('pragmarx.google2fa');
        $user->google2fa_secret = $google2fa->generateSecretKey();
        $user->save();
        $QR_Image = $google2fa->getQRCodeInline(
                config('app.name'), $user->username, $user->google2fa_secret
        );

        return view('painel.auth.2fa', [
            'QR_Image' => $QR_Image,
            'secret' => $user->google2fa_secret,
            'reauthenticating' => true
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (isset($_GET['tst'])) {
            $this->enviarEmail(1, 'Cadastro  efetuado com sucesso!', 'teste');
            exit();
        }

        return view('painel.pages.profile');
    }

    public function mensagemUsuarios(Request $request)
    {
        DB::table('mensagem_usuarios')->where('id', '=', 1)->update([
            'mensagem' => $request->mensagem,
            'ativo' => $request->ativo
        ]);
        
        return back()->with('status', 200)->with('msg', 'Mensagem adicionada com sucesso!');
    }
    
    public function sacar()
    {               
        return view('painel.pages.coinpayments');
    }

    private function coinpayments($valor, $log)
    {
        $usr = new User();
        $saldoVulcoins = $usr->saldoVulcoins();
        $config = new config();
        $config = $config->getConfig();
        
        /* Move as moedas para a carteira principal*/
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => "http://157.245.141.235/api/move?account=" . Auth::user()->username . "&amount=" . $valor,
            CURLOPT_HEADER => true
        ]);
        $result = json_decode(curl_exec($curl));        
        curl_close($curl);

        if($config->taxa_saque == 'sim'){
            $valor2 = $valor - ( ($valor / 100) * 3);
        }else{
            $valor2 = $valor;
        }
        
        
        /* Transfere as moedas para a carteira escolhida*/
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => "http://157.245.141.235/api/sendtoaddress?address=" . Auth::user()->bitzpayer_id . "&amount=" . $valor2
        ]);

        $result = json_decode(curl_exec($curl));        
        curl_close($curl);
       
        if (isset($result->hash)) {
            DB::table('saque_logs')->insert([
                'nome_usuario' => Auth::user()->username,
                'user_id' => Auth::user()->id,
                'bitzpayer_id' => Auth::user()->bitzpayer_id,
                'status' => 2,
                'mensagem' => 'Saque Aprovado',
                'saldo_restante' => $saldoVulcoins - $valor2,
                'saque_realizado' => $valor2,
                'saldo_anterior' => $saldoVulcoins,
                'comprovante' => $result->hash,
                'updated_at' => date("Y-m-d H:i:s")
            ]);     
            
            User::where('id', '=', Auth::user()->id)->update([
                'block_saque' => date("Y-m-d", strtotime("+". $config->saque_dias ." days")),
            ]);
        }

        if ($result->status == 200) {
            return redirect()
                ->route('sacar')
                ->with('status', 200)
                ->with('msg', 'Solicitação enviada com sucesso! Você pode ver o status em Histórico de saques.');
        } else if ($result->status == 201) {

            User::where('id', '=', Auth::user()->id)->update([
                'bitzpayer_id' => $result->carteira,
                'pub_key' => $result->pub_key,
            ]);

            return redirect()
                ->route('sacar')
                ->with('status', 200)
                ->with('msg', 'Solicitação enviada com sucesso! Você pode ver o status em Histórico de saques.');
        } else { 

            $curl = curl_init();
            curl_setopt_array($curl, [
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => "http://157.245.141.235/api/moveUser?toaccount=" . Auth::user()->username . "&amount=" . $valor
            ]);
            $result = json_decode(curl_exec($curl));
            curl_close($curl);
            
            return redirect()
                ->route('sacar')
                ->with('status', 400)
                ->with('msg', 'Não foi possível realizar o saque. Tente novamente em instantes.'); 
        }
    }

    public function validaSaque(Request $request)
    {
        $log = new SaqueLog();
        $config = new config();
        $config = $config->getConfig();
        $dataAtual = date("Y-m-d H:i:s");
        $dataBanco = Auth::user()->block_saque;
        $qntd_saque = Auth::user()->qntd_saque;
        $formatarData = date_create($dataBanco);
        $usr = new User();
        $saldo = $usr->saldoVulcoins();
        $saqueValor = $request->get('saqueValor');

        // DADOS DE LOG
        $log->saque_realizado = $saqueValor;
        $log->bitzpayer_id = Auth::user()->bitzpayer_id;
        $log->user_id = Auth::user()->id;
        $log->nome_usuario = Auth::user()->name;
        $log->saldo_anterior = $saldo;
        $log->saldo_restante = $saldo;
        //$valor_pacote = DB::table('plano_investimento')->where('usuario', '=', Auth::user()->id)->first()->valor_pacote;
        
       
        if ($saqueValor > $saldo) {
            return redirect()
                ->route('sacar')
                ->with('status', 400)
                ->with('msg', 'Você não tem saldo suficiente para sacar essa quantia de moedas.');
        }else if($config->permitir_saque == 'nao'){
            return redirect()
                ->route('sacar')
                ->with('status', 400)
                ->with('msg', 'O saque foi temporariamente bloqueado.');
        } /*else if($saqueValor > $valor_pacote){
             return redirect()
                ->route('sacar')
                ->with('status', 400)
                ->with('msg', 'O valor do saque não pode ser maior do que o valor máximo do pacote.');
             
        }*/else if (Auth::user()->bitzpayer_id == null) {
            return redirect()
                ->route('sacar')
                ->with('status', 400)
                ->with('msg', 'Você não cadastrou sua carteira.');
        } else if ($dataBanco > $dataAtual) {            
            return redirect()
                ->route('sacar')
                ->with('status', 400)
                ->with('msg', "Saque bloqueado até " . date_format($formatarData, "d/m/Y H:i:s") . ".");

        } else {            
            return $this->coinpayments($saqueValor, $log); 
            /*if ($bin_esquerda == 0 or $bin_direita == 0) {
                if (floatVal($saqueValor) > floatVal($limite_saque)) {
                    return redirect()
                                    ->route('sacar')
                                    ->with('error', "Quantidade máxima de saque é de " . $limite_saque . " vulcoins!");
                }else {
                    return $this->coinpayments($saqueValor, $log);
                }
            }else{
                return $this->coinpayments($saqueValor, $log);
            }     */       
        }
    }

    protected function saque()
    {





        /* // Fill these in from your API Keys page
      // $public_key = 'cb50180133a5fc96ac7284839f7afb73f0e36a390eba5def5cacafac2a799053';
      $private_key = '39d931848fF40d161e6d5332E2260193cbc835Ac3f22e198bC3350B64fBfed98';

      // // Set the API command and required fields
      // $req['version'] = 1;
      // $req['cmd'] = $cmd;
      // $req['key'] = $public_key;
      // $req['currency'] = 'BTC';
      // $req['format'] = 'json'; //supported values are json and xml

      // Generate the query string
      $post_data = http_build_query($req, '', '&');

      // Calculate the HMAC signature on the POST data
      $hmac = hash_hmac('sha512', $post_data, $private_key);

      // Create cURL handle and initialize (if needed)
      static $ch = NULL;
      if ($ch === NULL) {
          $ch = curl_init('https://www.coinpayments.net/api.php');
          curl_setopt($ch, CURLOPT_FAILONERROR, TRUE);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      }
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('HMAC: '.$hmac));
      curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);

      // Execute the call and close cURL handle
      $data = curl_exec($ch);
      // Parse and return data if successful.
      if ($data !== FALSE) {
          if (PHP_INT_SIZE < 8 && version_compare(PHP_VERSION, '5.4.0') >= 0) {
              // We are on 32-bit PHP, so use the bigint as string option. If you are using any API calls with Satoshis it is highly NOT recommended to use 32-bit PHP
              $dec = json_decode($data, TRUE, 512, JSON_BIGINT_AS_STRING);
          } else {
              $dec = json_decode($data, TRUE);
          }
          if ($dec !== NULL && count($dec)) {
              return $dec;
          } else {
              // If you are using PHP 5.5.0 or higher you can use json_last_error_msg() for a better error message
              return array('error' => 'Unable to parse JSON result ('.json_last_error().')');
          }
      } else {
          return array('error' => 'cURL error: '.curl_error($ch));
      } */ }

    public function novachave()
    {
        $rand = rand(111111, 999999);
        User::where('id', \Auth::user()->id)->update(['pin' => Hash::make($rand)]);

        $headers = "MIME-Version: 1.1\r\n";
        $headers .= "Content-type: text/plain; charset=UTF-8\r\n";
        $headers .= "From: noreply@bo.worldcryptocoin.io\r\n"; // remetente       
        $envio = mail(\Auth::user()->email, "Novo Pin", "Seu novo código de segurança é " . $rand , $headers);
        
        if ($envio == true)
            echo 'O seu código foi enviado com sucesso.';
        else
            echo 'Não foi possível enviar o e-mail';
    }

    public function ativarConta(Request $request)
    {
        $post = $request->all();

        $atulizaVoucher = Voucher::where('voucher', $post['ativarVoucher'])
            //->where('user_id', \Auth::user()->pai_id)
            ->where('status', 0)
            ->update(['status' => 1, 'activated_id' => \Auth::user()->id]);

        if ($atulizaVoucher) {

            if (\Auth::user()->update(['ativo' => 1])) {
                return redirect('/painel/home')
                    ->with('success', 'Parabéns! Sua conta foi ativada com sucesso!');
            } else {
                return redirect('/painel/home')
                    ->withErrors(['Não foi possivel ativar sua conta. Por favor entre em contato com o Suporte.']);
            }
        } else {
            return redirect('/painel/home')
                ->withErrors(['Voucher não encontrado. Ou já foi utilizado.']);
        }
    }
    
    public function confirmarMensalidade(Request $request){
        
        User::where('id', '=', $request->usuario)->update([
            'ativo' => 1,
            'mensalidade' => date("Y-m-d", strtotime("+31 days")),
        ]);
        
        return redirect('/painel/home')->with('status', 200)->with('msg', 'Mensalidade paga com sucesso!');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $data = $request->all();

        $valida = [
            'cpf' => 'cpf',
            'endereco' => '',
            'bairro' => '',
            'sexo' => '',
            'cidade' => '',
            'estado' => '',
            'nascimento' => 'date',
            'bitzpayer_email' => 'email'
        ];

        if (!empty($data['password'])) {
            $valida = array_merge($valida, ['password' => 'required|confirmed|min:6',]);
        }

        $validator = Validator::make($data, $valida);

        if ($validator->fails()) {

            return redirect('/painel/meus-dados')
                ->withErrors($validator)
                ->withInput();
        }
        if (!isset($data['saque'])) {
            $dados = [
                'endereco' => $data['endereco'],
                'bairro' => $data['bairro'],
                'cidade' => $data['cidade'],
                'sexo' => $data['sexo'],
                'estado' => $data['estado'],
                'telefone' => $data['telefone'],
                'direcao' => $data['direcao'],
                'segtitula_cpf' => @$data['segtitula_cpf'],
                'segtitula_nm' => @$data['segtitula_nm'],
                'bitzpayer_id' => @$data['bitzpayer_id'],
                'bitzpayer_email' => @$data['bitzpayer_email'],
                'fa_ativo' => @$data['fa_ativo'],
                'pin' => bcrypt($data['pin'])
                // 'banco' => $data['banco'],
                // 'agencia' => $data['agencia'],
                // 'conta' => $data['conta'],
                // 'tipo_conta' => $data['tipo_conta'],
                // 'operacao' => $data['operacao'],
            ];
        } else {
            $dados = [
                'segtitula_cpf' => $data['segtitula_cpf'],
                'segtitula_nm' => $data['segtitula_nm'],
                'endereco' => $data['endereco'],
                'bairro' => $data['bairro'],
                'cidade' => $data['cidade'],
                'sexo' => $data['sexo'],
                'estado' => $data['estado'],
                'telefone' => $data['telefone'],
                'direcao' => $data['direcao'],
                'saque' => $data['saque'],
                'pin' => bcrypt($data['pin']),
                'bitzpayer_id' => @$data['bitzpayer_id'],
                'bitzpayer_email' => @$data['bitzpayer_email'],
                'fa_ativo' => @$data['fa_ativo'],
                // 'banco' => $data['banco'],
                // 'agencia' => $data['agencia'],
                // 'conta' => $data['conta'],
                // 'tipo_conta' => $data['tipo_conta'],
                // 'operacao' => $data['operacao'],
            ];
        }

        if (!empty($data['password'])) {
            $senha = \DB::table('users')->where('id', \Auth::user()->id)->first()->password;
            if (\Hash::check($data['current_password'], $senha)) {
                $dados = array_merge($dados, ['password' => bcrypt($data['password']),]);
            } else {
                return redirect('/painel/meus-dados')->withErrors(['Senha inválida, tente novamente.']);
            }
        }

        $user = \Auth::user()->update($dados);

        if ($user) {

            return redirect('/painel/meus-dados')->with('success', 'Perfil Atualizado');
        } else {
            return redirect('/painel/meus-dados')->withErrors(['Não foi possivel atualizar, tente novamente.']);
        }
    }

    public function addUpgrade(Request $request)
    {
        $login = $request->valor;
        $pacoteNovo = $request->pacote;
        $moeda = $request->moeda;
        $usr = new User();
        $data = date('Y-m-d');

        $verificaSaldo = \DB::table('users')->where('username', $login)->first()->saldo;
        $verificarSaldoVulcoin = \DB::table('users')->where('username', $login)->first()->saldo_vulcoins;
        $idIndicacao = \DB::table('users')->where('username', $login)->first()->id;

        if ($verificaSaldo < $pacoteNovo) {
            echo 'Saldo Insuficiente';
        } else if ($verificarSaldoVulcoin < $pacoteNovo) {
            echo 'Saldo Insuficiente';
        } else {

            $valorInvestidoAtual = \DB::table('users')->where('username', $login)->first()->valorinvestido;

            if ($moeda == "dollar") {
                $saldoAntigo = \DB::table('users')->where('username', $login)->first()->saldo;
                $valorNovo = $valorInvestidoAtual + $pacoteNovo;
                $saldoNovo = $saldoAntigo - $pacoteNovo;
                \DB::table('users')->where('username', $login)->update(['saldo' => $saldoNovo, 'valorinvestido' => $valorNovo]);
                \DB::table('extratos')->insert(['user_id' => Auth::user()->id, 'data' => $data, 'descricao' => 'Reinvestimento com Dollar', 'valor' => $pacoteNovo, 'beneficiado' => $idIndicacao]);
            } else {
                $saldoAntigo = \DB::table('users')->where('username', $login)->first()->saldo_vulcoins;
                $valorNovo = $valorInvestidoAtual + $pacoteNovo;
                $saldoNovo = $saldoAntigo - $pacoteNovo;
                \DB::table('users')->where('username', $login)->update(['saldo_vulcoins' => $saldoNovo, 'valorinvestido' => $valorNovo]);
                \DB::table('extratos')->insert(['user_id' => Auth::user()->id, 'data' => $data, 'descricao' => 'Reinvestimento com Vulcoins', 'valor' => $pacoteNovo, 'beneficiado' => $idIndicacao]);
            }

            $usr->indicacaoUpgrade($idIndicacao, $pacoteNovo);

            echo 'Investimento Realizado com Sucesso';
        }
    }


    public function buscar(Request $request)
    {
        $usuario = User::where('id', $request->busca)->orWhere('username', $request->busca)->first();
        if ($usuario) {
            return '<meta http-equiv="refresh" content="0;URL=' . url('painel/minha-rede/' . $usuario->id) . '">';
        } else {
            return "<div class='alert alert-danger'>Sem resultados</div>";
        }
    }

    public function muda_lado(Request $request)
    {
        $data = $request->all();
        
        if ($data['lado'] == 'esquerda' or $data['lado'] == 'direita') {
            \Auth::user()->update(['direcao' => $data['lado']]);
            echo 'ok';
        } else {
            echo 'fail';
        }
    }

    public function view($id)
    {
        $dados['dados'] = User::where('id', $id)->first();
        $usrTotal = User::where('id', $id)->count();
        return view('admin.pages.usuarios.edit', $dados);
    }

    public function salvar(Request $request)
    {
        $data = $request->all();
        $valida = [
            'id' => 'integer|required',
            'name' => 'required|max:255',
            'cpf' => 'cpf',
            'saldo' => 'required',
            'admin' => 'required',
            'ativo' => 'required',
            'email' => 'required|email|max:255',
            'direcao' => 'required',
            'pacote' => 'required'
        ];
        if (!empty($data['password'])) {
            $valida = array_merge($valida, ['password' => 'required|confirmed|min:6',]);
        }

        $validator = Validator::make($data, $valida);

        if ($validator->fails()) {
            $res = $validator->errors()->getMessages();
            $erros = '';
            foreach ($res as $key) {
                $erros .= $key[0] . '<br>';
            }
            return back()->withErrors($erros);
        } else {
            $id = $data['id'];
            unset($data['id']);
            unset($data['pago']);

            unset($data['_token']);
            unset($data['password_confirmation']);

            if (!empty($data['password'])) {
                $data['password'] = bcrypt($data['password']);
            } else {
                unset($data['password']);
            }
            User::where('id', $id)->update($data);
            return back()->with('status', 200)->with('msg', 'Usuário atualizado com sucesso!');
        }
    }

    public function pin(Request $request)
    {
        $data = $request->all();
        if (\Auth::user()->pin == '') {

            $validator = Validator::make($data, ['pin' => 'required|confirmed|min:6']);

            if ($validator->fails()) {
                return redirect('/painel/home')->withErrors($validator);
            } else {
                User::where('id', \Auth::user()->id)->update(['pin' => bcrypt($data['pin'])]);
                return redirect('/painel/home')->with('success', 'Código de segurança definido com sucesso!');
            }
        } else {
            return redirect('/painel/home')->withErrors(['Você já possui um código de segurança.']);
        }
    }

    public function powerDirect(Request $request)
    {
        $valor = $request->valor;


        if (!is_numeric($valor)) {
            echo 'Operação não realizada.';
            exit();
        }
        if ($valor == '') {
            echo 'Operação não realizada.';
            exit();
        }
        $usr = new User();
        $usuarios = User::where('ativo', 1)->get();
        foreach ($usuarios as $value) {
            if (User::IsManagerDirect($value['id'])) {
                $usr->addPontoConsumo($value['id'], $valor, 'Manager Direct');
            }
        }
        echo "Operação realizada com sucesso.";
    }

    public function addSaldo(Request $request)
    {
        $valor = $request->valor;


        if (!is_numeric($valor)) {
            echo 'Operação não realizada.';
            exit();
        }
        if ($valor == '') {
            echo 'Operação não realizada.';
            exit();
        }
        $usr = new User();
        $usuarios = User::where('ativo', 1)->where('pacote', $request->pacote)->get();
        foreach ($usuarios as $value) {
            $pacoteValor = \DB::table('pacotes')->where('id', $value['pacote'])->first()->valor;
            $totalTasks = \DB::table('tarefas')->join('anuncios', 'anuncios.id', '=', 'tarefas.anuncio_id')->where('tarefas.user_id', $value['id'])->where('anuncios.status', '=', 1)->count();
            $completedTask = \DB::table('tarefas')->join('anuncios', 'anuncios.id', '=', 'tarefas.anuncio_id')->where('tarefas.user_id', $value['id'])->where('tarefas.status', '=', 1)->where('anuncios.status', '=', 1)->count();
            if ($completedTask == $totalTasks and $pacoteValor > 0) {
                echo $usr->userInfo($value['id'])->username;
                $usr->addSaldo($value['id'], $pacoteValor * ($valor / 100), 'Divisão de lucros');
            }
        }
        echo "Operação realizada com sucessoo.";
    }

    public function manage_usr()
    {
        $usuarios = User::all();
        return view('admin.pages.usuario', compact('usuarios'));
    }

    public function converter_saldo(Request $request)
    {

        $config = new config();
        $config = $config->getConfig();
        
        $valor = $request->valor_conver;

        $dataAtual = date("Y-m-d H:i:s");
        $dataBanco = Auth::user()->block_conversao;
        $qntd_saque = Auth::user()->qntd_saque;
        $formatarData = date_create($dataBanco);

        if ($dataBanco > $dataAtual){
            return back()->with('status', 400)->with('msg', "Conversão bloqueada até " . date_format($formatarData, "d/m/Y H:i:s") . ".");            
        }

        $preco = \App\Ico::all()->last()->price;
        //$preco = file_get_contents('https://vulcoin.io/api/ico/price');
        $usr = new User();

        
        $usr->verificarPin($request->pin);

        if (is_numeric($valor) and \Auth::user()->saldo >= $valor and $valor > 0) {

            $usr->removeSaldo(\Auth::user()->id, $valor, 'Conversão de saldo (Débito Saldo)');
            $valorVulcoins = $valor / $preco;

            $usr->addVulcoins(\Auth::user()->id, $valorVulcoins, 'Conversão de saldo (Crédito Vulcoin)');

            
            
            User::where('id', '=', Auth::user()->id)->update([
                'block_conversao' => date("Y-m-d H:i:s", strtotime("+". $config->conversao_dias ." days")),
            ]);

            
            return back()->with('status', 200)->with('msg', 'Operação realizada com sucesso.');            
        } else {
            return back()->with('status', 400)->with('msg', 'Operação não realizada.');           
        }
    }

    public function ver_user(Request $request)
    {
        $usr = new User();
        $usr->verificarPin($request->pin, 1);
        $dados = User::where('username', $request->usuario)->first();
        if (!isset($dados['email'])) {
            echo '<div class="alert alert-danger fade in">
                     O usuário não existe
                 </div>';
        } else if ($dados['ativo'] == 1) {
            echo '<div class="alert alert-danger fade in">
                    O usuário já esta ativo.
                 </div>';
        } else {
            $pacote = \App\Pacote::where('id', $dados['pacote'])->first();
            $dados = 'Nome Completo: ' . $dados['name'] . '<br>' . 'Username: ' . $dados['username'] . "<br>Valor minimo: $ 30 <br>Valor: <input class='form_control' type='text' id='valorPacote' name='valor'> <br> " . "<br><button type='button' onclick='ativar_user();' class='btn btn-primary' >Ativar Usuário({$dados['name']})</button>";

            echo $dados;
        }
    }

    public function ativar_user(Request $request)
    {
        if ($request->usuario) {
            $resp = 0;
            if (floatVal($request->valor) < floatVal(50)) {
                return back()->with('status', 400)->with('msg', utf8_decode('Valor minimo de ativacao e de: 50 vulcoins'));
            }

            $usuarios[] = [];
            $pacote = new Pacote();
            $Bin = new Binario();
            $usr = new User();
            $config = new config();
            //$usr->verificarPin($request->pin, 1);
            $ico_price = str_replace(',', '.', \App\Ico::all()->last()->price);
            $vulcoins = $request->valor;
            $quantidade = $usr->saldoVulcoins();


            if ( $quantidade < $vulcoins) {

                $saldo = Auth::user()->saldo / $ico_price;

                //verifica se tem saldo suficiente  para ativação
                if($saldo < $vulcoins){
                    return back()->with('status', 400)->with('msg', 'Saldo de vulcoins insuficiente');
                }else{
                    $curl = curl_init();
                    curl_setopt_array($curl, [
                        CURLOPT_RETURNTRANSFER => 1,
                        CURLOPT_URL => "http://157.245.141.235/api/moveUser?amount=" . $vulcoins . "&toaccount=" . $request->usuario
                    ]);

                    $resp = json_decode(curl_exec($curl));
                    curl_close($curl);

                    if(isset($resp->info) && $resp->info  == 1 ){
                        $saldo = $vulcoins * $ico_price;

                        $remove_saldo = DB::table('users')->where('id', '=', Auth::user()->id)->update([
                            'saldo' => Auth::user()->saldo - $saldo
                        ]);

                        extratos::criarExtrato(Auth::user()->id, -1 * $saldo, 'Conversão de Vulcoins para ativação do Usuário: ' . $request->usuario, '', '');
                    }
                }
            }else{
                $curl = curl_init();
                curl_setopt_array($curl, [
                    CURLOPT_RETURNTRANSFER => 1,
                    CURLOPT_URL => "http://157.245.141.235/api/moveUser?amount=" . $vulcoins . "&toaccount=" . $request->usuario ."&account=".Auth::user()->username
                ]);

                $resp = json_decode(curl_exec($curl));
                curl_close($curl);

                extratos::criarExtrato(Auth::user()->id, -1 * $vulcoins, 'Valor de vulcoins creditado para a conta: ' . $request->usuario . '. Para ser feito a ativação.', '', '');
            }

            $dados = \DB::table('users')->where('username', '=', $request->usuario)->where('ativo', '=', 0)->get();

            if (empty($dados)) {
                return back()->with('status', 400)->with('msg', 'Usuario inexistente');
            }

            if ($resp->info == 1) {

                foreach ($dados as $i => $row) {
                    $curl = curl_init();
                    curl_setopt_array($curl, [
                        CURLOPT_RETURNTRANSFER => 1,
                        CURLOPT_URL => "http://157.245.141.235/api/getbalance?account=" . $row->username
                    ]);

                    $result1 = json_decode(curl_exec($curl));
                    curl_close($curl);

                    if ($result1 != null) {
                        $ico_price = str_replace(',', '.', \App\Ico::all()->last()->price);
                        $quantidade = 30 / $ico_price;
                        $valor = $result1->quantidade * $ico_price;
                    }

                    if ($result1 != null && floatval($result1->quantidade) >= $quantidade) {
                        $id_usuario = User::where('id', '=', $row->id)->select('id', 'pacote', 'direcao', 'binario_esquerda', 'binario_direita', 'pai_id')->first();
                        $pai_id = User::where('id', '=', $id_usuario->pai_id)->select('id', 'pacote', 'direcao', 'binario_esquerda', 'binario_direita', 'saldo', 'username')->first();

                        $BinarioEsq = $pai_id->binario_esquerda;
                        $BinarioDireita = $pai_id->binario_direita;
                        $pacote = Pacote::where('id', $pai_id->pacote)->first();
                        $binario_val = $pacote['valor_ponto_binario'];

                        $teto = $pacote['teto_diario'];

                        if ($BinarioDireita > $BinarioEsq and $pacote['binario'] == 1) {

                            $usr->addPontos($pai_id->id, $valor, 'Pontuação de binário', 'esquerda');
                        } else if ($BinarioEsq > $BinarioDireita and $pacote['binario'] == 1) {

                            $usr->addPontos($pai_id->id, $valor, 'Pontuação de binário', 'direita');
                        } else if ($BinarioEsq == $BinarioDireita and $pacote['binario'] == 1) {

                            $usr->addPontos($pai_id->id, $valor, 'Pontuação de binário', 'direita');
                        }

                        $binario_valor = intval(($valor / 100) * 10) + intval($valor);

                        $unilevel = intVal(($valor / 100) * 1);

                        $valorIndicacao = ($valor / 100) * 6;
                        $valor_atual = User::where('id', '=', $pai_id->id)->first();

                        if (User::where('id', $pai_id->id)->update(['saldo' => $valor_atual->saldo + $valorIndicacao])) {
                            $result = extratos::create(['user_id' => 1, 'data' => date("Y-m-d"), 'descricao' => 'Bônus de indicação', 'valor' => $valorIndicacao, 'beneficiado' => $pai_id->id]);
                        }

                        $curl = curl_init();
                        curl_setopt_array($curl, [
                            CURLOPT_RETURNTRANSFER => 1,
                            CURLOPT_URL => "http://157.245.141.235/api/move?account=" . $row->username . "&amount=" . $result1->quantidade
                        ]);

                        $result1 = json_decode(curl_exec($curl));
                        curl_close($curl);

                        $ativar = User::where('id', '=', $row->id)->update([
                            'valorinvestido' => $valor,
                            'ativo' => 1,
                        ]);

                        $binario = $Bin->dist_binario($pai_id->id, $binario_valor, 1000000000);
                        $binario = $Bin->dist_binario($pai_id->id, $unilevel, 10);


                        return back()->with('status', 200)->with('msg', 'Usuario ativado com sucesso!');
                    } else {
                        return back()->with('status', 400)->with('msg', 'Quantidade de vulcoins insuficientes.');
                    }
                }
            }
        } else {
            $usuarios[] = [];
            $pacote = new Pacote();
            $Bin = new Binario();
            $usr = new User();
            $config = new config();

            $dados = \DB::table('users')->where('username', '=', $request->username)->get();

            $curl = curl_init();
            curl_setopt_array($curl, [
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => "http://157.245.141.235/api/moveUser?amount=" . $request->valor . "&toaccount=" . $request->username
            ]);

            $result1 = json_decode(curl_exec($curl));
            curl_close($curl);

            if (isset($result1->info) && $result1->info == 1) {

                foreach ($dados as $i => $row) {
                    $curl = curl_init();
                    curl_setopt_array($curl, [
                        CURLOPT_RETURNTRANSFER => 1,
                        CURLOPT_URL => "http://157.245.141.235/api/getbalance?account=" . $row->username
                    ]);

                    $result1 = json_decode(curl_exec($curl));
                    curl_close($curl);

                    if ($result1 != null) {
                        $ico_price = str_replace(',', '.', \App\Ico::all()->last()->price);
                        $quantidade = 30 / $ico_price;
                        $valor = $result1->quantidade * $ico_price;
                    }

                    if ($result1 != null && floatval($result1->quantidade) >= $quantidade) {
                        $id_usuario = User::where('id', '=', $row->id)->select('id', 'pacote', 'direcao', 'binario_esquerda', 'binario_direita', 'pai_id')->first();
                        $pai_id = User::where('id', '=', $id_usuario->pai_id)->select('id', 'pacote', 'direcao', 'binario_esquerda', 'binario_direita', 'saldo', 'username')->first();

                        $BinarioEsq = $pai_id->binario_esquerda;
                        $BinarioDireita = $pai_id->binario_direita;
                        $pacote = Pacote::where('id', $pai_id->pacote)->first();
                        $binario_val = $pacote['valor_ponto_binario'];

                        $teto = $pacote['teto_diario'];

                        if ($BinarioDireita > $BinarioEsq and $pacote['binario'] == 1) {

                            $usr->addPontos($pai_id->id, $valor, 'Pontuação de binário', 'esquerda');
                        } else if ($BinarioEsq > $BinarioDireita and $pacote['binario'] == 1) {

                            $usr->addPontos($pai_id->id, $valor, 'Pontuação de binário', 'direita');
                        } else if ($BinarioEsq == $BinarioDireita and $pacote['binario'] == 1) {

                            $usr->addPontos($pai_id->id, $valor, 'Pontuação de binário', 'direita');
                        }

                        $binario_valor = intVal(($valor / 100) * 10);
                        $unilevel = intVal(($valor / 100) * 1);

                        $valorIndicacao = ($valor / 100) * 6;
                        $valor_atual = User::where('id', '=', $pai_id->id)->first();

                        if (User::where('id', $pai_id->id)->update(['saldo' => $valor_atual->saldo + $valorIndicacao])) {
                            $result = extratos::create(['user_id' => 1, 'data' => date("Y-m-d"), 'descricao' => 'Bônus de indicação', 'valor' => $valorIndicacao, 'beneficiado' => $pai_id->id]);
                        }

                        $curl = curl_init();
                        curl_setopt_array($curl, [
                            CURLOPT_RETURNTRANSFER => 1,
                            CURLOPT_URL => "http://157.245.141.235/api/move?account=" . $row->username . "&amount=" . $result1->quantidade
                        ]);

                        $result1 = json_decode(curl_exec($curl));
                        curl_close($curl);

                        $ativar = User::where('id', '=', $row->id)->update([
                            'valorinvestido' => $valor,
                            'ativo' => 1,
                        ]);

                        $valor = intVal($valor);
                        $binario = $Bin->dist_binario($pai_id->id, $binario_valor + $valor, 100000000);
                        $binario = $Bin->dist_binario($pai_id->id, $unilevel, 10);


                        return back()->with('status', 200)->with('msg', 'Usuário ativado com sucesso!');
                    }
                }
            }
        }
    }

    private function replaceKeys($oldKey, $newKey, array $input)
    {
        $return = array();
        foreach ($input as $key => $value) {
            if ($key === $oldKey)
                $key = $newKey;

            if (is_array($value))
                $value = replaceKeys($oldKey, $newKey, $value);

            $return[$key] = $value;
        }
        return $return;
    }

    public function getCidade(Request $request)
    {
        $id = $request->id;
        $nome = $request->nome;
        $uf = $request->uf;
        if ($request->tp == 2) {
            $data = \DB::table('cidade')->select('nome', 'id')->where('estado', $id)->get();
            $i = 0;
            $json = '';
            foreach ($data as $value) {

                $json[$i]['id'] = $value->id;
                $json[$i]['value'] = $value->nome;
                $i++;
            }
            foreach ($json as $value) {

                echo "<option value='{$value['id']}'>{$value['value']}</option>";
            }
        } else {
            if ($id == '') {
                $estadoId = \DB::table('estado')->select('id')->where('uf', $uf)->first()->id;
                $data = \DB::table('cidade')->select('nome', 'id')->where('nome', $nome)->where('estado', $estadoId)->get();
            } else {
                $data = \DB::table('cidade')->select('nome', 'id')->where('estado', $id)->get();
            }
            $i = 0;
            $json = '';
            foreach ($data as $value) {

                $json[$i]['id'] = $value->id;
                $json[$i]['value'] = $value->nome;
                $i++;
            }
            echo json_encode($json);
        }
    }

    public function getEstados(Request $request)
    {
        $uf = $request->uf;
        if ($uf == '') {
            $data = \DB::table('estado')->select('nome', 'id')->get();
        } else {
            $data = \DB::table('estado')->select('nome', 'id')->where('uf', $uf)->get();
        }
        $i = 0;
        $json = '';

        foreach ($data as $value) {

            $json[$i]['id'] = $value->id;
            $json[$i]['value'] = $value->nome;
            $i++;
        }
        echo json_encode($json);
    }

    public function mudar_foto(Request $request)
    {
        // getting all of the post data
        $file = array('image' => $request->file('image'));
        // setting up rules
        $rules = array('image' => 'required|image',); //mimes:jpeg,bmp,png and for max size max:10000
        // doing the validation, passing post data, rules and the messages
        $validator = Validator::make($file, $rules);
        if ($validator->fails()) {
            // send back to the page with the input data and errors
            return Redirect::to('painel/home')->withInput()->withErrors($validator);
        } else {
            // checking file is valid.
            if ($request->file('image')->isValid()) {
                $destinationPath = 'uploads'; // upload path
                $extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
                $fileName = rand(11111, 99999) . '.' . $extension; // renameing image
                $request->file('image')->move($destinationPath, $fileName); // uploading file to given path
                $photo = asset($destinationPath . '/' . $fileName);
                User::where('id', \Auth::user()->id)->update(['photo' => $photo]);
                // sending back with message
                Session::flash('success', 'Foto alterada com sucesso!');
                return Redirect::to('painel/home');
            } else {
                // sending back with error message.
                Session::flash('error', 'O upload não foi realizado com sucesso.');
                return Redirect::to('painel/home');
            }
        }
    }

    public function transferir_saldo(Request $request)
    {

        $usr = new User();
        $valor = str_replace('.', '', $request->valor);
        $valor = str_replace(',', '.', $valor);
        $username = $request->username;
        $tipoSaldo = $request->tipo_saldo;
        $pinSalvo = Auth::user()->pin;

        if (Hash::check($request->pin, $pinSalvo)) {

            $userBeneficiado = User::where("username", $username)->first();

            if ($tipoSaldo == 1 && Auth::user()->saldo >= $valor) {
                $usr->removeSaldo(\Auth::user()->id, $valor, 'Transferência de saldo');
                $usr->addSaldo($userBeneficiado['id'], $valor, 'Transferência de Saldo');
                return redirect('/painel/home')
                    ->with("sucessoTransferir", "Operação realizada com sucesso");
            } else if ($tipoSaldo == 2 && Auth::user()->saldo_vulcoins >= $valor) {
                $usr->removeVulcoins(\Auth::user()->id, $valor, 'Transferência de Vulcoins');
                $usr->addVulcoins($userBeneficiado['id'], $valor, 'Transferência de Vulcoins');
                return redirect('/painel/home')
                    ->with("sucessoTransferir", "Operação realizada com sucesso");
            } else {
                return redirect('/painel/home')
                    ->with("erroSaldoTransferir", "Operação não realizada, você não tem saldo suficiente para essa transferência.");
            }
        } else {
            return redirect('/painel/home')
                ->with("erroPin", "Operação não realizada, o PIN informado está incorreto.");
        }
    }

    public function ver_user_transferencia(Request $request)
    {
        $usr = new User();
        $usr->verificarPin($request->pin, 1);
        $dados = User::where('username', $request->username)->first();
        $valor = str_replace('.', '', $request->valor);
        $valor = str_replace(',', '.', $valor);
        $username = $request->username;
        $tipo = $request->tipo;

        if (is_numeric($valor)) {

            if (!isset($dados['email'])) {
                echo '<div class="alert alert-danger fade in">
	                     O usuário não existe
	                 </div>';
            } else if ($dados['ativo'] == 0) {
                echo '<div class="alert alert-danger fade in">
	                    O usuário  esta inativo.
	                 </div>';
            } else {
                $pacote = \App\Pacote::where('id', $dados['pacote'])->first();

                if ($tipo == 1) {
                    $dados = 'Nome Completo: ' . $dados['name'] . '<br>' . 'Username: ' . $dados['username'] . "<br>Valor a pagar: R$" . $valor . "<br>O usuário irá receber: R$" . $valor . "<br><button  onclick='transferir();' class='btn btn-primary' >Transferir para {$dados['name']}</button>";
                } else {
                    $dados = 'Nome Completo: ' . $dados['name'] . '<br>' . 'Username: ' . $dados['username'] . "<br>Valor a pagar: " . $valor . " Vulcoins<br>O usuário irá receber: " . $valor . " Vulcoins<br><button  onclick='transferir();' class='btn btn-primary' >Transferir para {$dados['name']}</button>";
                }


                echo $dados;
            }
        } else {
            echo 'Por favor, informa um valor.';
        }
    }

    public function pagar_fatura(Request $request)
    {
        $usr = new User();

        $usr->verificarPin($request->pin);
        $dadosUsr = User::where('username', $request->username)->first();
        if (isset($dadosUsr->id)) {
            $fatura = \DB::table('faturas')->where('id', $request->fatura)->where('status', 0)->where('user_id', $dadosUsr->id)->first();
            if (!isset($fatura->id)) {
                return 'Fatura não encontrada.';
            }
            $pagamentoInfo = \DB::table('pagamentos')->where('id', $fatura->pagamento_id)->first();
            if (!isset($pagamentoInfo->id)) {
                return 'Ocorreu um erro. Gere uma nova fatura e tente novamente.';
            }
            if ($pagamentoInfo->tipo == 'Pagar fatura') {
                echo "Operação não permitida";
            } elseif (isset($fatura->id)) {
                $pacote = \App\Pacote::where('id', $pagamentoInfo->pacote)->first();
                $pacoteValor = $pacote->valor;

                if (\Auth::user()->saldo >= $pacoteValor) {
                    \Auth::user()->removeSaldo(\Auth::user()->id, $pacoteValor, 'Pagamento de fatura');
                    $pacote = new \App\Http\Controllers\Admin\PacoteController();
                    $pacote->liberar_fatura($fatura->id);
                } else {
                    echo 'Saldo insuficiente';
                }
            } else {
                echo 'Fatura não encontrada';
            }
        } else {
            echo 'Usuário não encontrado.';
        }
    }

    public function ativarRenda(Request $request)
    {
        $usr = new User();
        $usr->verificarPin($request->pin);
        $pacote = \App\Pacote::where('id', \Auth::user()->pacote)->first();
        if (\Auth::user()->renda_variavel == 1) {
            echo 'Você já participa da renda variavél.';
        } elseif (\Auth::user()->carteira_b >= \Auth::user()->removeVirgula(\App\Pacote::where('id', \Auth::user()->id)->first()['pontos_consumo'])) {
            if (\Auth::user()->update(['renda_variavel' => 1, 'carteira_b' => (\Auth::user()->carteira_b - $pacote['renda_variavel'])])) {
                echo 'Operaçao realizada com sucesso.';
            } else {
                echo 'Ocorreu um erro.';
            }
        } else {
            echo 'Para aderir a Renda Variável, você deverá possuir a mesma quantidade de ADSCOINS que recebeu no ato do cadastro.';
        }
    }
}
