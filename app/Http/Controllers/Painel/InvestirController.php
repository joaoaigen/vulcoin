<?php

namespace App\Http\Controllers\Painel;

use Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Rendimentos;
use App\extratos;
use App\Pacote;
use App\Binario;
use App\config;
use App\Referrals;
use Illuminate\Support\Facades\DB;

class InvestirController extends Controller {

    public function index() {
        $dados = \DB::table('investimentos')
                ->where('status', '=', 1)
                ->join('users', 'users.id', '=', 'investimentos.usuario')
                ->select('investimentos.id', 'investimentos.ico_price', 'investimentos.qntd_vulcoins', 'investimentos.hash_transacao', 'investimentos.status', 'users.username')
                ->orderBy('investimentos.id', 'asc')
                ->get();

        return view('admin.pages.investirUsuarios')->with('investimentos', $dados);
    }

    public function investir(Request $request) {
        $dados = $request->all();

        $duplicado = \DB::table('investimentos')->where('hash_transacao', '=', $dados['hash_transacao'])->first();
        if (!empty($duplicado)) {
            return response()->json(false);
        }
        $resp = \DB::table('investimentos')->insert([
            'usuario' => $dados['id_user'],
            'ico_price' => $dados['id_ico'],
            'hash_transacao' => $dados['hash_transacao'],
            'carteira' => $dados['carteira'],
            'criado_em' => date('Y-m-d H:i:s'),
            'atualizado_em' => date('Y-m-d H:i:s'),
            'status' => 1
        ]);

        if ($resp == 1) {
            return response()->json(true);
        } else {
            return response()->json(false);
        }
    }

    public function ativo($id) {
        $dados = \DB::table('investimentos')->where('id', '=', $id)->select('usuario', 'qntd_vulcoins', 'ico_price')->first();
        $resp = \DB::table('investimentos')->where('id', '=', $id)->update([
            'status' => 2
        ]);

        if ($resp == 1) {
            $user = \DB::table('users')->where('id', '=', $dados->usuario)->update([
                'ativo' => 1,
                'valorinvestido' => ($dados->qntd_vulcoins * $dados->ico_price),
                'saldo_vulcoins' => $dados->qntd_vulcoins,
            ]);
        }

        if ($resp == 1)
            return back()->with('status', 200)->with('msg', 'Usuário ativado com sucesso! As moedas foram adicionadas na conta.');
        else
            return response()->json(false);
    }

    public function rendimentos() {
        $rendimentos = Rendimentos::where('rendimento_id_usuario', '=', \Auth::user()->id)->get();

        return view('painel.pages.rendimentos')->with('rendimentos', $rendimentos);
    }

    public function programaInvestimentos(Request $request) {
        $usr = new User();
        $data = $request->all();
        $config = new config();
        $config = $config->getConfig();

        //pega os dados do usuário
        $usuario = DB::table('users')->where('username', '=', $data['username'])->first();

        //valor do pacote que foi escolhido
        $data['valor_invest'] = DB::table('pacotes_investimentos')->where('id', '=', $data['id'])->first()->valor;

        //verifica se o usuário tem um planto já
        $verifica_plano = DB::table('plano_investimento')->where('usuario', '=', $usuario->id)->first();

        $mesmo_pacote = DB::table('plano_investimento')->where('usuario', '=', $usuario->id)->where('pacote', '=', $data['id'])->first();

        if (isset($mesmo_pacote->id)) {

            $curl = curl_init();
            curl_setopt_array($curl, [
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => "http://157.245.141.235/api/getbalance?account=" . $data['username']
            ]);
            $result = json_decode(curl_exec($curl));
            curl_close($curl);


            $resp = '';
            $data['qntd_vulcoins'] = $mesmo_pacote->valor_pacote / str_replace(',', '.', \App\Ico::all()->last()->price);

            //verifica se a quantidade de vulcoins na carteira é compativel com o valor escolhido do pacote
            if ($result->quantidade < $data['qntd_vulcoins']) {
                return back()->with('status', 400)->with('msg', 'Saldo de vulcoins insuficiente!');
            }

            $resp = $usr->move($data['username'], $data['qntd_vulcoins']);

            if ($resp == 1) {
                $invest = DB::table('plano_investimento')->where('usuario', '=', $usuario->id)->update([
                    'porcentagem_atual' => 0,
                ]);
                
                if ($usuario->ativo == 0) {
                    
                    User::where('id', '=', $usuario->id)->update([
                        'ativo' => 1
                    ]);
                    
                    $planoPai = DB::table('plano_investimento')->where('usuario', '=', $usuario->pai_id)->first();
                    $pai_id = User::where('id', '=', $usuario->pai_id)->select('id', 'pacote', 'direcao', 'binario_esquerda', 'binario_direita', 'saldo', 'username')->first();
                    $valorIndicacao = ($data['valor_invest'] / 100) * $config->indicacao;
                    
                    $valor_atual = User::where('id', '=', $pai_id->id)->first();
                    
                    DB::table('plano_investimento')->where('usuario', '=', $usuario->pai_id)->update([
                       'valor_investido' => $planoPai->valor_investido + $valorIndicacao, 
                       'porcentagem_atual' => ( $planoPai->valor_investido / ($planoPai->valor_pacote / 100) ), 
                    ]);

                    if (User::where('id', $pai_id->id)->update(['saldo' => $valor_atual->saldo + $valorIndicacao])) {
                        $result = extratos::create(['user_id' => 1, 'data' => date("Y-m-d"), 'descricao' => 'Bônus de indicação', 'valor' => $valorIndicacao, 'beneficiado' => $pai_id->id]);
                    }
                    
                }

                $Bin = new Binario();


                $BinarioEsq = $usuario->binario_esquerda;
                $BinarioDireita = $usuario->binario_direita;

                if ($BinarioDireita > $BinarioEsq) {

                    $binario = $Bin->dist_binario($usuario->id, ( ($data['valor_invest'] / 100) * $config->binario ) + $data['valor_invest'], 1000000000);
                } else if ($BinarioEsq > $BinarioDireita) {

                    $binario = $Bin->dist_binario($usuario->id, ( ($data['valor_invest'] / 100) * $config->binario ) + $data['valor_invest'], 1000000000);
                } else if ($BinarioEsq == $BinarioDireita) {

                    $binario = $Bin->dist_binario($usuario->id, ( ($data['valor_invest'] / 100) * $config->binario ) + $data['valor_invest'], 1000000000);
                }

                session()->flash('status', 200);
                session()->flash('msg', 'Investimento realizado com sucesso!');

                return redirect('/painel/home')->with('status', 200)->with('msg', 'Investimento realizado com sucesso!');
            }
        } else {
            //se o usuário tiver um plano e for  menor o valor então é cancelado a solicitação
            /*if (isset($verifica_plano) && $data['valor_invest'] < $verifica_plano->valor_pacote) {
                session()->flash('status', 400);
                session()->flash('msg', "Escolha um plano de investimento maior! ");
                return back();
            }*/

            if (isset($verifica_plano->valor_pacote)) {
                //quantidade de vulcoins a ser descontada   ----   cálculo: (pact_escolhido - pact_anterior) / preco atual da moeda
                $data['qntd_vulcoins'] = (DB::table('pacotes_investimentos')->where('id', '=', $data['id'])->first()->valor - $verifica_plano->valor_pacote ) / str_replace(',', '.', \App\Ico::all()->last()->price);            
            } else {
                //quantidade de vulcoins a ser descontada   ----   cálculo: (pact_escolhido - pact_anterior) / preco atual da moeda
                $data['qntd_vulcoins'] = intval($data['valor_invest']) / str_replace(',', '.', \App\Ico::all()->last()->price);                    
            }
            
            if (isset($verifica_plano) && isset($verifica_plano->valor_pacote) && $data['valor_invest'] <= $verifica_plano->valor_pacote) {
                $data['qntd_vulcoins'] = intval($data['valor_invest']) / str_replace(',', '.', \App\Ico::all()->last()->price);
            }
                        
            //pega a quantidade de vulcoins na carteira
            $curl = curl_init();
            curl_setopt_array($curl, [
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => "http://157.245.141.235/api/getbalance?account=" . $data['username']
            ]);
            $result = json_decode(curl_exec($curl));
            curl_close($curl);

            $resp = '';

            //verifica se a quantidade de vulcoins na carteira é compativel com o valor escolhido do pacote
            if ($result->quantidade < $data['qntd_vulcoins']) {
                return back()->with('status', 400)->with('msg', 'Saldo de vulcoins insuficiente!');
            }

            //move as moedas 
            $resp = $usr->move($data['username'], $data['qntd_vulcoins']);
            
            if ($resp == 1) {

                //Se já existir um plano investido então somente atualiza para o atual, se não cria o primeiro plano
                if(isset($verifica_plano) && isset($verifica_plano->valor_pacote) && $data['valor_invest'] <= $verifica_plano->valor_pacote){
                    $invest = DB::table('plano_investimento')->where('usuario', '=', $usuario->id)->update([
                        'pacote' => $data['id'],
                        'porcentagem_atual' => 0, //intval($verifica_plano->valor_investido / ( ( $data['valor_invest'] / 100 ) * DB::table('pacotes_investimentos')->where('id', '=', $data['id'])->first()->porcentagem_maxima ) * 100),
                        'valor_pacote' => $data['valor_invest'],
                        'porcentagem_maxima' => DB::table('pacotes_investimentos')->where('id', '=', $data['id'])->first()->porcentagem_maxima
                    ]);

                    extratos::criarExtrato($usuario->id, $data['valor_invest'], 'Compra do pacote de investimento', '', '');
                } else if (isset($verifica_plano) && $verifica_plano->porcentagem_atual == 200) {
                    $invest = DB::table('plano_investimento')->where('usuario', '=', $usuario->id)->update([
                        'pacote' => $data['id'],
                        'porcentagem_atual' =>  0,//intval($verifica_plano->valor_investido / ( ( $data['valor_invest'] / 100 ) * DB::table('pacotes_investimentos')->where('id', '=', $data['id'])->first()->porcentagem_maxima ) * 100),
                        'valor_pacote' => $data['valor_invest'],
                        'porcentagem_maxima' => DB::table('pacotes_investimentos')->where('id', '=', $data['id'])->first()->porcentagem_maxima
                    ]);

                    extratos::criarExtrato($usuario->id, $data['valor_invest'], 'Compra do pacote de investimento', '', '');
                } else if (isset($verifica_plano)) {
                    $invest = DB::table('plano_investimento')->where('usuario', '=', $usuario->id)->update([
                        'pacote' => $data['id'],
                        'porcentagem_atual' => intval($verifica_plano->valor_investido / ( ( $data['valor_invest'] / 100 ) * DB::table('pacotes_investimentos')->where('id', '=', $data['id'])->first()->porcentagem_maxima ) * 100),
                        'valor_pacote' => $data['valor_invest'],
                        'porcentagem_maxima' => DB::table('pacotes_investimentos')->where('id', '=', $data['id'])->first()->porcentagem_maxima
                    ]);

                    extratos::criarExtrato($usuario->id, $data['valor_invest'], 'Compra do pacote de investimento', '', '');
                } else {
                    $invest = DB::table('plano_investimento')->insert([
                        'usuario' => $usuario->id,
                        'pacote' => $data['id'],
                        'valor_pacote' => $data['valor_invest'],
                        'porcentagem_maxima' => DB::table('pacotes_investimentos')->where('id', '=', $data['id'])->first()->porcentagem_maxima
                    ]);

                    extratos::criarExtrato($usuario->id, $data['valor_invest'], 'Compra do pacote de investimento', '', '');
                }

                if ($usuario->ativo == 0) {
                    User::where('id', '=', $usuario->id)->update([
                        'ativo' => 1
                    ]);

                    $planoPai = DB::table('plano_investimento')->where('usuario', '=', $usuario->pai_id)->first();
                    $pai_id = User::where('id', '=', $usuario->pai_id)->select('id', 'pacote', 'direcao', 'binario_esquerda', 'binario_direita', 'saldo', 'username')->first();
                    $valorIndicacao = ($data['valor_invest'] / 100) * $config->indicacao;
                    
                    $valor_atual = User::where('id', '=', $pai_id->id)->first();
                    
                    DB::table('plano_investimento')->where('usuario', '=', $usuario->pai_id)->update([
                       'valor_investido' => $planoPai->valor_investido + $valorIndicacao, 
                       'porcentagem_atual' => ( $planoPai->valor_investido / ($planoPai->valor_pacote / 100) ), 
                    ]);

                    if (User::where('id', $pai_id->id)->update(['saldo' => $valor_atual->saldo + $valorIndicacao])) {
                        $result = extratos::create(['user_id' => 1, 'data' => date("Y-m-d"), 'descricao' => 'Bônus de indicação', 'valor' => $valorIndicacao, 'beneficiado' => $pai_id->id]);
                    }
                } else {
                    $pai_id = User::where('id', '=', $usuario->pai_id)->select('id', 'pacote', 'direcao', 'binario_esquerda', 'binario_direita', 'saldo', 'username')->first();
                    $valor_pai = ($data['valor_invest'] / 100) * $config->indicacao;
                    $valor_atual = User::where('id', '=', $pai_id->id)->first();
                    User::where('id', $pai_id->id)->update(['saldo' => $valor_atual->saldo + $valor_pai]);
                    extratos::criarExtrato($pai_id->id, $valor_pai, $config->indicacao .'% do novo pacote adquirido pelo usuário: ' . $data['username'], '', '');
                }

                $Bin = new Binario();


                $BinarioEsq = $usuario->binario_esquerda;
                $BinarioDireita = $usuario->binario_direita;

                if ($BinarioDireita > $BinarioEsq) {

                    $binario = $Bin->dist_binario($usuario->id, ( ($data['valor_invest'] / 100) * $config->binario ) + $data['valor_invest'], 1000000000);
                } else if ($BinarioEsq > $BinarioDireita) {

                    $binario = $Bin->dist_binario($usuario->id, ( ($data['valor_invest'] / 100) * $config->binario ) + $data['valor_invest'], 1000000000);
                } else if ($BinarioEsq == $BinarioDireita) {

                    $binario = $Bin->dist_binario($usuario->id, ( ($data['valor_invest'] / 100) * $config->binario ) + $data['valor_invest'], 1000000000);
                }

                session()->flash('status', 200);
                session()->flash('msg', 'Investimento realizado com sucesso!');

                return redirect('/painel/home')->with('status', 200)->with('msg', 'Investimento realizado com sucesso!');
            }

            session()->flash('status', 400);
            session()->flash('msg', 'Não foi possível realizar o investimento!');

            return redirect('/painel/home')->with('status', 400)->with('msg', 'Não foi possível realizar o investimento!');
        }
    }

    public function gerarRendimentos() {
        $config = new config();
        $config = $config->getConfig();
        
        $rendimentos = DB::table('plano_investimento')->get();

        foreach ($rendimentos as $rend) {
            if ($rend->porcentagem_atual < $rend->porcentagem_maxima) {
                $att = DB::table('plano_investimento')->where('id', '=', $rend->id)->update([
                    'valor_investido' => ( ($rend->valor_pacote / 100) * $config->taxa_diaria ) + $rend->valor_investido,
                    'porcentagem_atual' => $rend->porcentagem_atual + 1,
                ]);

                $saldo = \DB::table('users')->where('id', '=', $rend->usuario)->select('saldo')->first();
                $rentabilidade = ($rend->valor_pacote / 100) * $config->taxa_diaria;
                $total = $saldo->saldo + $rentabilidade;


                \DB::table('users')->where('id', $rend->usuario)->update([
                    'saldo' => $total,
                ]);

                \DB::table('extratos')->insert(['user_id' => 1, 'data' => date("Y-m-d"), 'descricao' => 'Rendimento diário. N° do rendimento: ' . $rend->id, 'valor' => (($rend->valor_pacote / 100) * $config->taxa_diaria), 'beneficiado' => $rend->usuario]);
            }
        }
    }

    public function gerarRendimentosAntigo() {
        $rendimentos = Rendimentos::where('rendimento_percentual', '<', 300)->get();

        foreach ($rendimentos as $rend) {

            Rendimentos::where('id_rendimento', '=', $rend->id_rendimento)->update([
                'rendimento_percentual' => $rend->rendimento_percentual + 1,
                'rendimento_valor_investido_atual' => (($rend->rendimento_valor_investido / 100) * 1) + $rend->rendimento_valor_investido_atual,
            ]);

            $saldo = \DB::table('users')->where('id', '=', $rend->rendimento_id_usuario)->select('saldo')->first();
            $rentabilidade = ($rend->rendimento_valor_investido / 100) * 1;
            $total = $saldo->saldo + $rentabilidade;

            \DB::table('users')->where('id', $rend->rendimento_id_usuario)->update([
                'saldo' => $total,
            ]);

            \DB::table('extratos')->insert(['user_id' => 1, 'data' => date("Y-m-d"), 'descricao' => 'Rendimento diário. N° do rendimento: ' . $rend->id_rendimento, 'valor' => (($rend->rendimento_valor_investido / 100) * 1), 'beneficiado' => $rend->rendimento_id_usuario]);
        }
    }

    public function reajustarRendimentos() {
        $usuarios = DB::table('users')->get();

        foreach ($usuarios as $row) {
            $investimento = DB::table('plano_investimento')->where('usuario', '=', $row->id)->first();
            if (isset($investimento)) {
                $result = extratos::where('beneficiado', '=', $row->id)->where('data', '=', '2019-07-30')->where('descricao', '=', 'Investimento: ' . $investimento->id)->first();

                if (isset($result->valor)) {
                    /* DB::table('users')->where('id', '=', $row->id)->update([
                      'saldo' => $row->saldo - $result->valor
                      ]); */
                }else{
                    print_r($row->id);
                    print_r('<br>');
                }
            }
        }
    }

}
