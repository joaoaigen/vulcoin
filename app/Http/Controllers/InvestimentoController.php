<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Investimento;
use App\Rendimentos;

class InvestimentoController extends Controller {

    public function ativar($username, $valor) {
	if (Investimento::ativar($username, $valor))
	{
		echo 'true';
	}
	else
	{
		echo 'false';
	}
    }
    
    
    
    public function rodaLocacaoPacotes() {

        $selecionaUsers = \DB::table("users")->where('valorinvestido', '>', 0)->get();

        foreach ($selecionaUsers as $usr) {
            if ($usr->porcentagem_valor_investido < 300) {
                $data = date('Y-m-d');

                $valorInvestido = $usr->valorinvestido;
                $rentabilidade = ($valorInvestido * 1) / 100;

                
                $saldoAtual = $usr->saldo;
                $novoSaldo = $saldoAtual + $rentabilidade;                
                
                \DB::table('users')->where('id', $usr->id)->update(['saldo' => $novoSaldo, 'porcentagem_valor_investido' => $usr->porcentagem_valor_investido + 1]);
                \DB::table('extratos')->insert(['user_id' => 1, 'data' => $data, 'descricao' => 'Rendimento Diário', 'valor' => $rentabilidade, 'beneficiado' => $usr->id]);
            }
        }
    }

}

?>