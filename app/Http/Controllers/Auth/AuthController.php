<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Validator;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;

class AuthController extends Controller {
    /*
      |—————————————————————————
      | Registration & Login Controller
      |—————————————————————————
      |
      | This controller handles the registration of new users, as well as the
      | authentication of existing users. By default, this controller uses
      | a simple trait to add these behaviors. Why don't you explore it?
      |
     */

    use  AuthenticatesUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/admin/home';
    protected $username = 'username';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data) {
        return Validator::make($data, [
                    'name' => 'required|max:255',
                    'username' => 'required|max:255|unique:users',
                    'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return User
     */
    protected function create(array $data) {
        $usr = new User();        
        dd($user->getAccountAddress());
        
        return User::create([
                    'name' => $data['name'],
                    'username' => $data['username'],
                    'password' => bcrypt($data['password']),
        ]);
    }

    public function showLoginUser() {
        return view('painel.auth.login');
    }

    public function logout() {
       User::where('id', '=', Auth::user()->id)->update(['auth2fa' => 0]);
        Auth::logout();

        return redirect('/painel/login');
    }

    public function login_user(Request $request) {

        $validator = Validator::make($request->all(), ['username' => 'required', 'password' => 'required', 'g-recaptcha-response' => 'required|recaptcha']);
        if ($validator->fails()) {
            return redirect('/painel/login')->withErrors($validator);
        } else {
            if (Auth::attempt(['username' => $request->username, 'password' => $request->password, 'admin' => 0])) {
                                
                
                $fa_ativo = auth()->user()->fa_ativo;
                
                if($fa_ativo == 0){
						dd("logou");
                    return redirect('/painel/home');
                }
                
                $user = \Auth::user();

                // initialise the 2FA class
                $google2fa = app('pragmarx.google2fa');

                // generate a new secret key for the user
                $user->google2fa_secret = $google2fa->generateSecretKey();

                // save the user
                $user->save();

                // generate the QR image
                $QR_Image = $google2fa->getQRCodeInline(
                        config('app.name'), $user->email, $user->google2fa_secret
                );

                // Pass the QR barcode image to our view.
                return view('painel.auth.2fa', [
                    'QR_Image' => $QR_Image,
                    'secret' => $user->google2fa_secret,
                    'reauthenticating' => true
                ]);

              
            } else {
                return redirect('/painel/login')->withErrors(['Credenciais informadas não correspondem com nossos registros.']);
            }
        }
    }

    public function login_admin(Request $request) {
        $validator = Validator::make($request->all(), ['username' => 'required', 'password' => 'required', 'g-recaptcha-response' => 'required|recaptcha']);
        if ($validator->fails()) {
            return redirect('/admin/home')->withErrors($validator);
        } else {
            if (Auth::attempt(['username' => $request->username, 'password' => $request->password, 'admin' => 1])) {
                return redirect()->intended('admin/home');
            } else {
                return redirect('/admin/login')->withErrors(['Credenciais informadas não correspondem com nossos registros.']);
            }
        }
    }

    public function confirmar_codigo(Request $request){
        $data = $request->all();
        $usuario = User::where('username', $data['username'])->select('id')->first()->id;
        $cod = random_bytes(3);
        $codigo = strtoupper(bin2hex($cod));

        $this->enviarEmail($usuario, 'Seu código de verificação é: ' . $codigo, 'Utilize o código acima para entrar em nosso sistema.');

        User::where('id', $usuario)->update([
            'codigo_login' => $codigo,
        ]);

        if(isset($data['ajax'])){
            return response()->json(200);
        }else{
            return view('painel.auth.confirmar_codigo', $data);
        }        
    }
    
    

}
