<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Validator;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use Google2FA;

class AuthController extends Controller {
    /*
      |—————————————————————————
      | Registration & Login Controller
      |—————————————————————————
      |
      | This controller handles the registration of new users, as well as the
      | authentication of existing users. By default, this controller uses
      | a simple trait to add these behaviors. Why don't you explore it?
      |
     */

use AuthenticatesUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/admin/home';
    protected $username = 'username';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    
    private $fileName = 'google2fasecret.key';
    private $name = 'PragmaRX';
    private $email = 'google2fa@pragmarx.com';
    private $secretKey;
    private $keySize = 25;
    private $keyPrefix = '';

    public function __construct() {
        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data) {
        return Validator::make($data, [
                    'name' => 'required|max:255',
                    'username' => 'required|max:255|unique:users',
                    'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return User
     */
    protected function create(array $data) {
        return User::create([
                    'name' => $data['name'],
                    'username' => $data['username'],
                    'password' => bcrypt($data['password']),
        ]);
    }

    public function showLoginUser() {
        return view('painel.auth.login');
    }

    public function logout() {
        User::where('id', '=', Auth::user()->id)->update(['auth2fa' => 0]);
        Auth::logout();

        return redirect('/painel/login');
    }

    public function login_user(Request $request) {

        $validator = Validator::make($request->all(), ['username' => 'required', 'password' => 'required', 'g-recaptcha-response' => 'required|recaptcha']);
        if ($validator->fails()) {
            return redirect('/painel/login')->withErrors($validator);
        } else {
            if (Auth::attempt(['username' => $request->username, 'password' => $request->password, 'admin' => 0])) {
                
                /*  $curl = curl_init();
                curl_setopt_array($curl, [
                    CURLOPT_RETURNTRANSFER => 1,
                    CURLOPT_URL => "http://157.245.141.235/api/getnewaddress?account=" . auth()->user()->username
                ]);
                $result = json_decode(curl_exec($curl));
                curl_close($curl);

                \Illuminate\Foundation\Auth\User::where('id', '=', auth()->user()->id)->update([
                    'carteira' => $result->erro
                ]);*/

                $fa_ativo = auth()->user()->fa_ativo;
                //verifica se a 2fa está ativa, se não estiver redireciona para a pagina principal. Caso estiver ativa, verifica se já existe um código 2fa e continua com os procedimentos de  validação.
                if ($fa_ativo == 0) {
                    return redirect('/painel/home');
                }    
                
                $user = \Auth::user();
                // initialise the 2FA class
                $google2fa = app('pragmarx.google2fa');

                if (empty($user->google2fa_secret)) {
                    return redirect('/novo-qrcode');
                }else{
                    return view('painel.auth.2fa');
                }
            } else {
                return redirect('/painel/login')->withErrors(['Credenciais informadas não correspondem com nossos registros.']);
            }
        }
    }

    public function login_admin(Request $request) {
        $validator = Validator::make($request->all(), ['username' => 'required', 'password' => 'required', 'g-recaptcha-response' => 'required|recaptcha']);
        if ($validator->fails()) {
            return redirect('/admin/login')->withErrors($validator);
        } else {
            if (Auth::attempt(['username' => $request->username, 'password' => $request->password, 'admin' => 1])) {
                
                return redirect('admin/home');
            } else {
                return redirect('/admin/login')->withErrors(['Credenciais informadas não correspondem com nossos registros.']);
            }
        }
    }

    public function confirmar_codigo(Request $request) {
        $data = $request->all();
        $usuario = User::where('username', $data['username'])->select('id')->first()->id;
        $cod = random_bytes(3);
        $codigo = strtoupper(bin2hex($cod));

        $this->enviarEmail($usuario, 'Seu código de verificação é: ' . $codigo, 'Utilize o código acima para entrar em nosso sistema.');

        User::where('id', $usuario)->update([
            'codigo_login' => $codigo,
        ]);

        if (isset($data['ajax'])) {
            return response()->json(200);
        } else {
            return view('painel.auth.confirmar_codigo', $data);
        }
    }
}
