<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Licencas;

class LicencasController extends Controller {

    public function index() {
        $licencas = Licencas::all();
        return view('admin.pages.licencas', compact('licencas'));
    }
    
    public function create(){
        
        $data = $_POST['date'];
        $cliente = $_POST['client'];
        Licencas::create(['cliente' => $cliente,'data' => $data, 'licenca' => md5($cliente . '-' . $data), 'created_at' => date('y-m-d'), 'update_at' => date('y-m-d')]);
        return redirect('admin/licencas');
    }
    
    public function delete($id)
    {
        Licencas::where('Id', $id)->delete();
        return redirect('admin/licencas');
    }
    
    public function check()
    {
        $licence = $_FILES['licence'];
        
        move_uploaded_file($licence['tmp_name'], public_path() . '/uploads/' . $licence['name']);
        
        return redirect('admin/logout');
        
    }
    
    public function add()
    {
        return view('admin.pages.addLicencas');
    }
    
    private function gravar($licenca){
        
        $licencas = $this->ler();
        
        array_push($licencas, $licenca);
        
        $arquivo = public_path() . "/uploads/licences.json";
        $fp = fopen($arquivo, "w");
        fwrite($fp, json_encode($licencas));
        fclose($fp);
    }
    
    private function ler(){
        $arquivo = public_path() . "/uploads/licences.json";
        $fp = fopen($arquivo, "r");
        $conteudo = fread($fp, filesize($arquivo));
        fclose($fp);
        return json_decode($conteudo);
    }
}
