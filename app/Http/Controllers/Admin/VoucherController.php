<?php

namespace App\Http\Controllers\Admin;

use App\Voucher;
use Illuminate\Http\Request;
use App\User;
use App\Pacote;
use App\extratos;
use App\Referrals;
use App\Pagamentos;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Binario;
use App\config;
use App\BoletoMydas;

class VoucherController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {


        $usuarios = User::all();
        return view('admin.pages.vouchers', compact('usuarios'));
    }

    public function index2() {


        $usuarios = User::all();
        return view('admin.pages.vouchersSite', compact('usuarios'));
    }

    public function active($id, $user){
        $voucher = new Voucher();
        echo $voucher->active($id, $user);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($user) {
        $usuario = User::find($user);
        $voucher = new Voucher();
        $prices = $voucher->getAllPrices();
        return view('admin.pages.addvoucher', compact('usuario', 'prices'));
    }

    public function create2($user) {
        $usuario = User::find($user);
        return view('admin.pages.addVoucherSite', compact('usuario'));
    }

    public function remover($user) {
        $usuario = User::find($user);
        return view('admin.pages.removevoucher', compact('usuario'));
    }

    public function remover2($user) {
        $usuario = User::find($user);
        return view('admin.pages.removeVoucherSite', compact('usuario'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id, \Faker\Generator $faker) {
        $data = \Input::all();

        $data['user_id'] = $id;

        for ($i = 0; $i < $data['quantidade']; $i++) {
            $data['voucher'] = $faker->sha1;
            $create = Voucher::create($data);
        }

        if ($create) {
            echo <<<EOL
                 <div class="alert alert-success fade in">
                      Vouchers Gerados Com Successo!
                 </div>
EOL;
        } else {
            echo <<<EOL
       <div class="alert alert-danger fade in">
                      Ocorreu um erro! Por favor tente novamente.
                 </div>
EOL;
        }
    }

    public function store2(Request $request, $id, \Faker\Generator $faker) {
        $data = \Input::all();

        $data['user_id'] = $id;

        for ($i = 0; $i < $data['quantidade']; $i++) {
            $data['code'] = md5(time()) . $i . $id . rand(0, 999);
            $create = \DB::table('vouchers_site')->insert(['user_id' => $id, 'code' => $data['code']]);
        }

        if ($create) {
            echo <<<EOL
                 <div class="alert alert-success fade in">
                      Vouchers Gerados Com Successo!
                 </div>
EOL;
        } else {
            echo <<<EOL
       <div class="alert alert-danger fade in">
                      Ocorreu um erro! Por favor tente novamente.
                 </div>
EOL;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($user) {
        $data = \Input::all();
        for ($i = 0; $i < $data['quantidade']; $i++) {
            $voucher = Voucher::where('user_id', $user)->first();
            if ($voucher) {
                $destroy = Voucher::destroy($voucher->id);
            } else {
                $destroy = true;
                break;
            }
        }

        if ($destroy) {
            echo <<<EOL
                 <div class="alert alert-success fade in">
                      Vouchers removidos com sucesso!
                 </div>
EOL;
        } else {
            echo <<<EOL
       <div class="alert alert-danger fade in">
                      Ocorreu um erro! Por favor tente novamente.
                 </div>
EOL;
        }
    }

    public function destroy2($user) {
        $data = \Input::all();
        for ($i = 0; $i < $data['quantidade']; $i++) {
            $voucher = \DB::table('vouchers_site')->where('user_id', $user)->first();
            if ($voucher) {
                $destroy = \DB::table('vouchers_site')->delete($voucher->id);
            } else {
                $destroy = true;
                break;
            }
        }

        if ($destroy) {
            echo <<<EOL
                 <div class="alert alert-success fade in">
                      Vouchers removidos com sucesso!
                 </div>
EOL;
        } else {
            echo <<<EOL
       <div class="alert alert-danger fade in">
                      Ocorreu um erro! Por favor tente novamente.
                 </div>
EOL;
        }
    }

    public function addValidadePacote($usr) {
        $dataUs = User::where('id', $usr)->first();
        $validade = date('Y-m-d', strtotime('+ 1 month', strtotime(date('Y-m-d'))));
        $validade = date('Y-m-d', strtotime('+ 3 days', strtotime($validade)));
        User::where('id', $usr)->update(['validade_pacote' => $validade]);
    }

    public function addSemanas($usr) {
        $usr = new User();
        $dataUs = User::where('id', $usr)->first();
        $pacoteData = Pacote::where('id', $dataUs['pacote'])->first();

        $usuarios = User::where('ativo', 1)->where('pai_id', $usr)->get();
        $total = 0;
        foreach ($usuarios as $value) {
            $pacoteUser = '';
            $pacoteUser = Pacote::where('id', $value['pacote'])->first();
            if ($pacoteData['valor'] <= $pacoteUser['valor']) {
                $total = $total + 1;
            }
        }
        $val_pac = \DB::table("cotas")->where('user_id', $usr)->first()->data;
        $validade = date('Y-m-d', strtotime('+ 1 month', strtotime($val_pac)));
        $validade = strtotime($validade);
        $hoje = strtotime(date('Y-m-d'));

        if ($total > 0 and $dataUs['total_div'] <= 16 and $validade <= $hoje) {
            if ($total % 2 == 0) {
                $usr->addDiv($usr, 4);
            }
        }
    }

    public function teste() {
        $this->addSemanas(1);
    }

    public function verificaStatusBoletoMydas()
    {
        $b = new BoletoMydas();

        $boletos = $b->ObterTodos();

        foreach($boletos as $boleto)
        {
            $url = env('MYDAS_URL_VERIFICA_PAGAMENTO');
            $id_admin = env('MYDAS_ADMIN_ID');

            $cpf = str_replace('.', '', $boleto->user_cpf);
            $cpf = str_replace('-', '', $cpf);

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => $url . $id_admin . '/' . $cpf,
                CURLOPT_USERAGENT => 'Checar Status dos Pagamentos'
            ));
            $resp = curl_exec($curl);
            curl_close($curl);
            $status = json_decode($resp);
            echo $resp . '<br>';
            if($b->ValidarStatusUser($boleto->id_user)->ativo == 0 && $status->success && $status->result == 'Não a faturas para esse cliente')
            {
                $b->ValidarPagamento($boleto->id);
                $this->ativarUsr($boleto->id_user);
            }
        }

        echo "Processamento concluído!<br>";

    }

    public function ativarUsr($id = '') {
        if (@$id == '') {
            $id = @$_GET['id'];
        }
        $idAct = $id;
        $usr = new User();
        $bin = new Binario();
        $dataUs = User::where('id', $id)->first();
        $status = 1;
        if ($status == 0 or $status == 1) {
            
        } else {
            echo 'Operação Não realizada';
            exit();
        }
        if (is_numeric($id) and $dataUs['ativo'] <> 1) {
            extratos::where("descricao", '*Renovação automática')->where('beneficiado', $idAct)->update(['status' => 1]);
            $dataUser = User::where('id', $id)->first();
            $pacote = $dataUser['pacote'];

            $userPai = User::where('id', $dataUser['pai_id'])->first();
            $usuarioQueNaoRecebe = '';
            if (isset($userPai['id'])) {
                if (!$userPai->qualificado()) {
                    $usuarioQueNaoRecebe = $userPai->id;
                }
            }


            $pacoteData = Pacote::where('id', $pacote)->first();
            $valorBinario = $pacoteData['valor_binario'];
            $userInfo = User::where('id', $idAct)->first();
            session(['sendEmail' => $userInfo['email'], 'sendName' => $userInfo['name']]);
            $dataMail['subject'] = 'Conta ativada/Atualizada com sucesso!';
            $dataMail['content'] = "<h5>Estamos muito felizes por isso!</h5>
                    <b>
          Seus dados de acesso são:<br>
          Login: " . $userInfo['username'] . "<br>
          Email: " . $userInfo['email'] . "<br>
          Seu site: <a href='" . url('site/' . $userInfo['username']) . "'>site</a>
<br>
          Página: <a href='" . url('painel/login') . "'>Login</a>";
            $this->enviarEmail($idAct, 'Conta ativada/atualizada com sucesso!', $dataMail['content']);
            User::where('id', $id)->update(['pacote' => $dataUs['pacote'], 'dataAtivacao' => date('Y-m-d')]);
            // extratos::create(['user_id' => $id, 'data' => $today, 'descricao' => 'Indicação Direta', 'valor' => $valorBeneficiado, 'beneficiado' => $dataUser['pai_id']]);
            //ativa usr
            $user = new User();
            $user = User::find($id);
            $user->ativaUser();
            // User::where('id', $dataUser['pai_id'])->update(['saldo' => $novoSaldo]);
            $user->addSaldo(1, $pacoteData['valor'], 'Ativação de cadastro');

            if ($pacoteData['binario'] == 1) {

                $bin->dist_binario($idAct, $valorBinario, 6, $usuarioQueNaoRecebe);
                ///$this->renova($pai);
            }
            $this->addValidadePacote($idAct);
            $user->indicacao($idAct);
            $res = $user->produto_pacote($idAct);
            if (isset($res['id'])) {
                $dataMail['subject'] = 'Pedido criado com sucesso!';
                $dataMail['content'] = "<h5>Os produtos inclusos em seu KIT já estão sendo processados!</h5>
                    <b>
                              ID: #" . $res['id'] . $res['descricao'];
                $this->enviarEmail($idAct, $dataMail['subject'], $dataMail['content']);
            }
        }
        //echo "Operação  realizada com sucesso";
    }

    function mudarPacote($id = '', $pacote = '') {
        if (@$id <> '') {
            $_GET['id'] = $id;
        }
        if (@$pacote <> '') {
            $_GET['pacote'] = $pacote;
        }
        $bin = new Binario();
        $pacote = $_GET['pacote'];
        $id = $_GET['id'];
        $idAct = $id;
        if (is_numeric($id) and is_numeric($pacote)) {
            $dataUser = User::where('id', $id)->first();
            $userPai = User::where('id', $dataUser['pai_id'])->first();
            $today = date("Y-m-d");
            $pacoteData = Pacote::where('id', $pacote)->first();
//pacotePai
            $pacoteValor = $pacoteData['valor'];

            $porcentagemUsr = Pacote::where('id', $pacote)->first()['porcentagem'];
            $porcentagemAdm = 1 - $porcentagemUsr;
            $valorBeneficiado = ($pacoteValor) * ($porcentagemUsr);
            $valorBeneficiado2 = ($pacoteValor) * ($porcentagemAdm);
            $valorBinario = ($pacoteValor) * ($pacoteData['porcentagem_bin']);
//ativa usr
            $user = new User();
            $user = User::find($id);
            $user->ativaUser();
//atualiza
//User::where('id', $id)->update(['pacote' => $dataUser['pacote'], 'pacote_status' => 1]);
            User::where('id', $id)->update(['pacote' => $pacote]);
            //extratos::create(['user_id' => $id, 'data' => $today, 'descricao' => 'Novo pagamento', 'valor' => $valorBeneficiado, 'beneficiado' => $dataUser['pai_id']]);
            $novoSaldo = $valorBeneficiado + $userPai['saldo'];
            //User::where('id', $dataUser['pai_id'])->update(['saldo' => $novoSaldo, 'pacote' => $pacote]);
            $beneficiado = Binario::where('user_id', $userPai['id'])->first();
            $pontosBeneficiado = $beneficiado['pontos'] + $valorBeneficiado;

            $userInfo = User::where('id', $idAct)->first();
            session(['sendEmail' => $userInfo['email'], 'sendName' => $userInfo['name']]);
            $dataMail['subject'] = 'Upgrade realizado com sucesso!';
            $dataMail['content'] = "<h5>Estamos muito felizes por isso!</h5>
                    <b>
          Agora seu novo plano  é :{$pacoteData['nome']}<br>

<br>
          Login: <a href='" . url('painel/login') . "'>Login</a>";

            $this->enviarEmail($idAct, $dataMail['subject'], $dataMail['content']);

            $user = new User();
            $user = User::find($id);
            $user->ativaUser();
            // User::where('id', $dataUser['pai_id'])->update(['saldo' => $novoSaldo]);
            $user->addSaldo(1, $pacoteData['valor'], 'Upgrade');

            if ($pacoteData['binario'] == 1) {

                $bin->dist_binario($idAct, $valorBinario);
                ///$this->renova($pai);
            }
            $this->addValidadePacote($idAct);
            $user->indicacao($idAct);
            $res = $user->produto_pacote($idAct);
            if (isset($res['id'])) {
                $dataMail['subject'] = 'Pedido criado com sucesso!';
                $dataMail['content'] = "<h5>Os produtos inclusos em seu KIT já estão sendo processados!</h5>
                    <b>
                              ID: #" . $res['id'] . $res['descricao'];
                $this->enviarEmail($idAct, $dataMail['subject'], $dataMail['content']);
            }
            //$user->criarCotas($pacote, $idAct, 0);
            echo 'Operação realizada.';
            return true;
        } else {
            echo 'Operação não realizada.';
            return false;
        }
    }

    function renova($id) {
        $config = new config();
        $config = $config->getConfig();
        if ($config['renovacao_status'] == 'sim') {
            $dataUser = User::where('id', $id)->first();
            $pacote = $dataUser['pacote'];
            $pacoteData = Pacote::where('id', $pacote)->first();
            $today = date("Y-m-d");
            if (!\Auth::user()->aptoGanhoTotal()) {
                if (\Auth::user()->saldo >= $pacoteData['valor']) {
                    \Auth::user()->removeSaldo(\Auth::user()->id, $pacoteData['valor'], "Renovação automática");
                    \Auth::user()->update(['ativo' => 0]);
                    echo "<div style='display:none'>";
                    $this->ativarUsr($id);
                    echo "</div>";
                } else {
                    if (\Auth::user()->saldo > 0) {
                        \Auth::user()->removeSaldo(\Auth::user()->id, \Auth::user()->saldo, "*Renovação automática");
                        \Auth::user()->update(['ativo' => 0]);
                    } else {
                        \Auth::user()->update(['ativo' => 0]);
                    }
                }
                $desc = ['Bônus Binário(direita)', 'Bônus Binário(esquerda)', 'Divisão de lucros', 'Bônus de indicação'];
                extratos::where('beneficiado', $id)->whereIn('descricao', $desc)->update(['status' => 1]);
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
        return false;
    }

    function statusSaque() {
        $id = $_GET['id'];
        if ($id == '' or ! is_numeric($id)) {
            echo 'Erro';
        } else {
            $dataUser = User::where('id', $id)->first();
            if ($dataUser['saque'] == 'ativo') {
                $status = 'inativo';
            } else {
                $status = 'ativo';
            }
            $data['saque'] = $status;
            User::where('id', $id)->update($data);
            echo "Operação realizada com sucesso";
        }
    }

}
