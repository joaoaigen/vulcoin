<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Pacote;
use Validator;
use App\User;
use App\config;
use Session;
use Redirect;
use DB;
use App\extratos;
use App\Binario;

class PacoteController extends Controller {

    public function index() {
        $usr = new User();

        $pacotes = Pacote::all();
        return view('admin.pages.pacote', compact('pacotes'));
    }

    public function create() {
        return view('admin.pages.pacotes.create');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request) {
        $pacote = DB::table('plano_investimento')->where('usuario', '=', $request->usuario)->first(); 
        $valor = DB::table('pacotes_investimentos')->where('id', '=', $request->pacote)->first()->valor;
        $usuario = DB::table('users')->where('id', '=', $request->usuario)->first();
        
        if (isset($pacote->id)) {
            DB::table('plano_investimento')->where('id', '=', $pacote->id)->update([
                'pacote' => $request->pacote,
                'porcentagem_atual' => 0,
                'porcentagem_maxima' => $pacote->porcentagem_maxima,
                'valor_pacote' => $valor
                    
            ]);

            extratos::criarExtrato($usuario->id, $valor, 'Pacote de investimento', '', '');
            if ($usuario->ativo == 0) {
                User::where('id', '=', $usuario->id)->update([
                    'ativo' => 1
                ]);

                $pai_id = User::where('id', '=', $usuario->pai_id)->select('id', 'pacote', 'direcao', 'binario_esquerda', 'binario_direita', 'saldo', 'username')->first();
                $valorIndicacao = ($valor / 100) * 6;
                $valor_atual = User::where('id', '=', $pai_id->id)->first();


                if (User::where('id', $pai_id->id)->update(['saldo' => $valor_atual->saldo + $valorIndicacao])) {
                    $result = extratos::create(['user_id' => 1, 'data' => date("Y-m-d"), 'descricao' => 'Bônus de indicação', 'valor' => $valorIndicacao, 'beneficiado' => $pai_id->id]);
                }
            }
            
            $Bin = new Binario();

            $BinarioEsq = $usuario->binario_esquerda;
            $BinarioDireita = $usuario->binario_direita;

            if ($BinarioDireita > $BinarioEsq) {
                $binario = $Bin->dist_binario($usuario->id, ( ($valor / 100) * 10 ) + $valor, 1000000000);
            } else if ($BinarioEsq > $BinarioDireita) {
                $binario = $Bin->dist_binario($usuario->id, ( ($valor / 100) * 10 ) + $valor, 1000000000);
            } else if ($BinarioEsq == $BinarioDireita) {
                $binario = $Bin->dist_binario($usuario->id, ( ($valor / 100) * 10 ) + $valor, 1000000000);
            }

            session()->flash('status', 200);
            session()->flash('msg', 'Pacote adicionado!');

            return redirect('/admin/home')->with('status', 200)->with('msg', 'Pacote adicionado!');
        }else{
            $pacote = DB::table('pacotes_investimentos')->where('id', '=', $request->pacote)->first();
            DB::table('plano_investimento')->insert([
                'usuario' => $request->usuario,
                'pacote' => $request->pacote,
                'valor_investido' => 0,
                'valor_pacote' => $valor,
                'porcentagem_atual' => 0,
                'porcentagem_maxima' => $pacote->porcentagem_maxima
            ]);

            extratos::criarExtrato($usuario->id, $valor, 'Pacote de investimento', '', '');
            if ($usuario->ativo == 0) {
                User::where('id', '=', $usuario->id)->update([
                    'ativo' => 1
                ]);

                $pai_id = User::where('id', '=', $usuario->pai_id)->select('id', 'pacote', 'direcao', 'binario_esquerda', 'binario_direita', 'saldo', 'username')->first();
                $valorIndicacao = ($valor / 100) * 6;
                $valor_atual = User::where('id', '=', $pai_id->id)->first();


                if (User::where('id', $pai_id->id)->update(['saldo' => $valor_atual->saldo + $valorIndicacao])) {
                    $result = extratos::create(['user_id' => 1, 'data' => date("Y-m-d"), 'descricao' => 'Bônus de indicação', 'valor' => $valorIndicacao, 'beneficiado' => $pai_id->id]);
                }
            }

            $Bin = new Binario();

            $BinarioEsq = $usuario->binario_esquerda;
            $BinarioDireita = $usuario->binario_direita;

            if ($BinarioDireita > $BinarioEsq) {
                $binario = $Bin->dist_binario($usuario->id, ( ($valor / 100) * 10 ) + $valor, 1000000000);
            } else if ($BinarioEsq > $BinarioDireita) {
                $binario = $Bin->dist_binario($usuario->id, ( ($valor / 100) * 10 ) + $valor, 1000000000);
            } else if ($BinarioEsq == $BinarioDireita) {

                $binario = $Bin->dist_binario($usuario->id, ( ($valor / 100) * 10 ) + $valor, 1000000000);
            }

            session()->flash('status', 200);
            session()->flash('msg', 'Pacote adicionado!');

            return redirect('/admin/home')->with('status', 200)->with('msg', 'Pacote adicionado!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function view($id) {
        $dados['dados'] = Pacote::where('id', $id)->first();
        $pacoteTotal = Pacote::where('id', $id)->count();
        return view('admin.pages.pacotes.edit', $dados);
    }

    public function salvar() {
        $data = \Input::all();
        $usr = new User();
        $valida = [
            'id' => 'integer|required',
            'nome' => 'required|max:255',
        ];

        $validator = Validator::make($data, $valida);

        if ($validator->fails()) {
            $res = $validator->errors()->getMessages();
            $erros = '';
            foreach ($res as $key) {
                $erros.=$key[0] . '<br>';
            }
            return <<<EOL
                 <div class="alert alert-danger fade in">
                     $erros
                 </div>
EOL;
        } else {
            $id = $data['id'];
            unset($data['id']);
            unset($data['_token']);
            Pacote::where('id', $id)->update($data);
            return <<<EOL
                 <div class="alert alert-success fade in">
                     Pacote Atualizado
                 </div>
EOL;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        //
    }

    public function expirados() {
        $pacote = new Pacote();
        $pacote->expirados();
    }

    public function faturas() {
        $bar = true;
        /* $str = "TEST". ($bar ? 'true' : 'false') ."TEST"; */

        $config = new config();
        $config = $config->getConfig();
        $faturas = \DB::table('faturas')->where('user_id', \Auth::user()->id)->get();
        return view('painel.pages.faturas', compact('faturas', 'config'));
    }

    public function excluir_fatura($id) {
        if (\DB::table('faturas')->where('id', $id)->where('user_id', \Auth::user()->id)->count() > 0) {
            $dados = \DB::table('faturas')->where('id', $id)->first();
            \DB::table('faturas')->where('id', $id)->delete();
            \DB::table('pagamentos')->where('id', $dados->pagamento_id)->delete();
            Session::flash('success', 'Deletado com sucesso!');
            return Redirect::to('painel/faturas');
        } else {
            return redirect('/painel/faturas')->withErrors(['Fatura não encontrada.']);
        }
    }

    //admin

    public function faturas_admin() {
        $config = new config();
        $config = $config->getConfig();
        $faturas = \DB::table('faturas')->get();
        return view('admin.pages.faturas', compact('faturas', 'config'));
    }

    public function liberar_fatura($id) {
        $voucher = new VoucherController();
        $faturaInfo = \DB::table('faturas')->where('id', $id)->first();
        if (isset($faturaInfo->id) and $faturaInfo->status <> 0553) {
            //ativa a fatura
            $pagamentoInfo = \DB::table('pagamentos')->where('id', $faturaInfo->pagamento_id)->first();

            if ($pagamentoInfo->tipo == 'Upgrade') {
                $pacoteInfo = Pacote::where('id', $pagamentoInfo->pacote)->first();
                $validade = date('Y-m-d', strtotime('+' . $pacoteInfo->duracao_meses . 'month', strtotime('Y-m')));
                $validade = date('Y-m-d', strtotime('+ 3 days', strtotime($validade)));
                \DB::table('faturas')->where('id', $id)->update(['data_pagamento' => date('Y-m-d'), 'status' => 1, 'data' => date('Y-m-d'), 'validade' => $validade]);

                $voucher->mudarPacote($faturaInfo->user_id, $pagamentoInfo->pacote);
            } elseif ($pagamentoInfo->tipo == 'Compra') {
                $validade = date('Y-m-d', strtotime('+ 3 days', strtotime(date('Y-m-d'))));
                $validade = date('Y-m-d', strtotime('+ 3 days', strtotime($validade)));
                \DB::table('faturas')->where('id', $id)->update(['data_pagamento' => date('Y-m-d'), 'status' => 1, 'data' => date('Y-m-d'), 'validade' => $validade]);
                \App\Pedidos::where('id', $pagamentoInfo->pacote)->update(['status' => 'Pago']);
                $produtos = json_decode(\App\Pedidos::where('id', $pagamentoInfo->pacote)->first()->produtos);
                $use = new User();
             
                $pontosCompra = 0;
                foreach ($produtos as $value) {
                    for ($i = 1; $i <= $value->quantidade; $i++) {
                        $infoProduto = \App\Produtos::where('id', $value->product_id)->first();
                        if (isset($infoProduto['id'])) {
                            $pontosCompra = $pontosCompra + $value->preco * $infoProduto->porcentagem_pontos;
                        }
                    }
                    //
                }
                if ($pontosCompra > 0) {
                    $bin = new \App\Binario();
                    $bin->dist_binario($faturaInfo->user_id, $pontosCompra);
                }

                $produ = new \App\Produtos();
                foreach ($produtos as $value) {
                    $produ->add_venda($value->product_id, $value->quantidade);
                }
              //  $this->enviarEmail($userInfo['id'], $dataMail['subject'], $dataMail['content']);
                /* foreach ($produtos as $value) {
                  for ($i = 1; $i <= $value->quantidade; $i++) {
                  $use->bonus_compra($value->product_id, $faturaInfo->user_id);
                  }
                  //
                  }
                  $produ = new \App\Produtos();
                  foreach ($produtos as $value) {
                  $produ->add_venda($value->product_id, $value->quantidade);
                  } */
            }
            $dataMail['subject'] = 'Fatura liberada com sucesso!';
            $dataMail['content'] = "<h5>Estamos muito felizes por seu cadastro!</h5>
                    <b>
          A fatura #" . $faturaInfo->id . " foi liberada com sucesso<br>

          Ver fatura: <a href='" . url('painel/faturas') . "'>Fatura</a>";


            $this->enviarEmail($faturaInfo->user_id, $dataMail['subject'], $dataMail['content']);
            return redirect('/admin/faturas')->with('success', 'Operação realizada com sucesso');
        } else {
            return redirect('/admin/faturas')->withErrors(['Fatura não foi encontrada ou já foi paga.']);
        }
    }

    public function excluir_fatura_admin($id) {
        if (\DB::table('faturas')->where('id', $id)->where('user_id', \Auth::user()->id)->count() > 0) {
            $dados = \DB::table('faturas')->where('id', $id)->first();
            \DB::table('faturas')->where('id', $id)->delete();
            \DB::table('pagamentos')->where('id', $dados->pagamento_id)->delete();
            Session::flash('success', 'Deletado com sucesso!');
            return Redirect::to('painel/faturas');
        } else {
            return redirect('/painel/faturas')->withErrors(['Fatura não encontrada.']);
        }
    }

}
