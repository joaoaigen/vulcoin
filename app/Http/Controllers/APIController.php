<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ico;
use App\Saldo;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\ComprarPacote;
use Illuminate\Support\Facades\DB;
use App\config;

class APIController extends Controller
{  
    public function priceDOGE() {
        $req['amount'] = 30;
        $req['currency1'] = 'USD';
        $req['currency2'] = 'DOGE';

        $return = $this->coinpayments_api_call('create_transaction', $req);

        return $return['result'];
    }

    public function priceBTC(){        
        $return = $this->coinpayments_api_call('rates'); 
        
        if($return['result']['BTC']){            
            $ico = Ico::all()->last();
            $ico->price_btc = $return['result']['USD']['rate_btc'];
            $ico->save();
        }
    }
    
    public function depositar($amount, $email, $id, $pacote){
        $config = new config();
        $config = $config->getConfig();
        
        $req['amount'] = $amount;
        $req['currency1'] = 'USD'; //$currency1;
        $req['currency2'] = $config->crypto_recebimento;//$currency2;
        $req['buyer_email'] = $email;//$buyer_email;
        $req['buyer_name'] = $id;//$buyer_name; //id do usuário
        $req['success_url'] = 'https://bo.workmoney.io/painel/confirmar-pacote/?pacote='.$pacote.'&usuario='.$id;//$buyer_name; //id do usuário
        
        $return = $this->coinpayments_api_call('create_transaction', $req);       
        
        if($return['error'] == 'ok'){
            ComprarPacote::create([
               'txnid' => $return['result']['txn_id'], 
               'usuario' => $id, 
               'buyer_email' => $email, 
               'amount' => $return['result']['amount'], 
               'timeout' => $return['result']['timeout'], 
               'confirms_needed' => $return['result']['confirms_needed'], 
               'address' => $return['result']['address'], 
               'status' => 0,
               'created_at' => date('Y-m-d H:i:s'),
               'updated_at' => date("Y-m-d H:i:s"),
            ]);
            
            
            return $return['result']['checkout_url'];
                    
        }        
    }
        
    public function pesquisaTransacao(Request $request){        
        $txnid = ComprarPacote::where('status', '!=', -1)->where('status', '!=', 100)->get();
        
        foreach ($txnid as $row){
            
            $req['txid'] = $row->txnid;
            
            $resp = $this->coinpayments_api_call('get_tx_info', $req);
            
            
            
           
            if($resp['result']['status'] == 100){
                 
                ComprarPacote::where('id', '=', $row->id)->update([
                    'status' => $resp['result']['status'],
                ]);
                $saldo = Saldo::where('usuario', '=', $row->usuario)->where('moeda', '=', $resp['result']['coin'])->first();
                
                if (isset($saldo)) {
                   Saldo::where('usuario', '=', $row->usuario)->where('moeda', '=', $resp['result']['coin'])->update([
                        'quantidade' => $resp['result']['amountf'] + $saldo->quantidade,
                        'updated_at' => date('Y-m-d H:i:s'),
                    ]); 
                } else {
                    Saldo::create([
                        'quantidade' => $resp['result']['amountf'],
                        'moeda' => $resp['result']['coin'],
                        'usuario' => $row->usuario,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                    ]);
                }
            }else if ($resp['result']['status'] == 0){
                ComprarPacote::where('id', '=', $row->id)->update([
                    'status' => $resp['result']['status'],
                ]);
            }else{
               ComprarPacote::where('id', '=', $row->id)->update([
                    'status' => $resp['result']['status'],
                ]); 
            }
        }   
    }
            
    public function coinpayments_api_call($cmd, $req = array()) {
        // Fill these in from your API Keys page
        $public_key = '2c34739ac74056939502a6095168c4c4785b85faf8f034275f09d8720ece1c3f';
        $private_key = '92208474edC72CaA0590fAD0c7694ceB5d33624448831913B66f77e50dcE7e0E';

        // Set the API command and required fields
        $req['version'] = 1;
        $req['cmd'] = $cmd;
        $req['key'] = $public_key;
        $req['format'] = 'json'; //supported values are json and xml
        // Generate the query string
        $post_data = http_build_query($req, '', '&');

        // Calculate the HMAC signature on the POST data
        $hmac = hash_hmac('sha512', $post_data, $private_key);

        // Create cURL handle and initialize (if needed)
        static $ch = NULL;
        if ($ch === NULL) {
            $ch = curl_init('https://www.coinpayments.net/api.php');
            curl_setopt($ch, CURLOPT_FAILONERROR, TRUE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        }
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('HMAC: ' . $hmac));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);

        // Execute the call and close cURL handle     
        $data = curl_exec($ch);
        // Parse and return data if successful.
        if ($data !== FALSE) {
            if (PHP_INT_SIZE < 8 && version_compare(PHP_VERSION, '5.4.0') >= 0) {
                // We are on 32-bit PHP, so use the bigint as string option. If you are using any API calls with Satoshis it is highly NOT recommended to use 32-bit PHP
                $dec = json_decode($data, TRUE, 512, JSON_BIGINT_AS_STRING);
            } else {
                $dec = json_decode($data, TRUE);
            }
            if ($dec !== NULL && count($dec)) {
                return $dec;
            } else {
                // If you are using PHP 5.5.0 or higher you can use json_last_error_msg() for a better error message
                return array('error' => 'Unable to parse JSON result (' . json_last_error() . ')');
            }
        } else {
            return array('error' => 'cURL error: ' . curl_error($ch));
        }
    }

}
