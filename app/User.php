<?php

namespace App;

use Avatar;

use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Str;
use App\Binario;
use App\extratos;
use App\config;
use Hash;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    private $naoQualificaPck = [];

    protected $connection= 'mysql';

    protected $table = 'users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'mensalidade',
        'mensalidade_ativo',
        'bonus_cashbak',
        'fa_ativo',
        'cep',
        'dataAtivacao',
        'photo',
        'carteira_b',
        'validade_pacote',
        'total_bin_esq',
        'total_bin_dir',
        'graduacao',
        'name',
        'email',
        'password',
        'cpf',
        'endereco',
        'cidade',
        'sexo',
        'bairro',
        'estado',
        'nascimento',
        'telefone',
        'pai_id',
        'bitzpayer_id',
        'banco',
        'agencia',
        'conta',
        'operacao',
        'pin',
        'admin',
        'ativo',
        'tipo_conta',
        'pago',
        'segtitular_nm',
        'segtitula_cpf',
        'direcao',
        'username',
        'pacote',
        'pacote_status',
        'block_saque',
        'created_at',
        'saque',
        'binario_direita',
        'binario_esquerda',
        'codigo_login',
        'pub_key',
        'carteira',
        'qntd_saque'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function saqueLog()
    {
        return $this->hasMany(SaqueLog::class);
    }

    public function saques()
    {
        return $this->hasMany(Saque::class);
    }

    public function isAdmin()
    {
        return $this->admin; // this looks for an admin column in your users table
    }

    public function saldoVulcoins()
    {
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => "http://157.245.141.235/api/getbalance?account=" . \auth()->user()->username
        ]);
        $result = json_decode(curl_exec($curl));
        curl_close($curl);
        if (isset($result->quantidade))
            return $result->quantidade;
        else
            return 'Atualize a página...';
    }

    public function getTransaction($hash)
    {
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => "http://157.245.141.235/api/gettransaction?hash=" . $hash
        ]);
        $result = curl_exec($curl);
        curl_close($curl);

        return $result;
    }

    //Vt4JkHhVJ1JMBn6FhccSkCELS9csKejp26
    public function move($username, $quantidade)
    {
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => "http://157.245.141.235/api/move?account=" . $username . "&amount=" . $quantidade
        ]);

        $result = json_decode(curl_exec($curl));
        curl_close($curl);
        
        
        if ($result->status == 200)
            return 1;
    }

    public function aptoGanhoDiario($user_id = '')
    {
        if ($user_id == '') {
            $user_id = $this->id;
        }
        $userInfo = $this->userInfo($user_id);

        $desc = ['Bônus Binário(direita)', 'Bônus Binário(esquerda)', 'Divisão de lucros', 'Bônus de indicação'];
        $ganhos = \DB::table("extratos")->where('valor', '>', 0)->where('status', 0)->where('data', date('Y-m-d'))->where('beneficiado', $user_id)->whereIn('descricao', $desc)->sum('valor');
        $ganhoTotal = $ganhos;
        $valorPacote = Pacote::where('id', $userInfo['pacote'])->first()->valor;
        $limite = $valorPacote * 0.4;
        if ($ganhoTotal >= $limite) {
            return true;
        } else {
            return true;
        }
    }

    public function getUserIndicador($user_id)
    {
        $refer = Referrals::where('user_id', $user_id)->first();
        $user = new User();
        if (isset($refer->pai_id)) {
            $user = $this->where('id', $refer->pai_id)->first();
        }

        return $user;
    }

    public function totalGanhos($user_id = '')
    {
        if ($user_id == '') {
            $user_id = $this->id;
        }
        $userInfo = $this->userInfo($user_id);

        $desc = ['Bônus Binário(direita)', 'Bônus Binário(esquerda)', 'Divisão de lucros', 'Bônus de indicação DIRETA', 'Rendimento Diário', 'Bônus de indicação INDIRETA', 'Reinvestimento'];
        $ganhos = \DB::table("extratos")->where('valor', '>', 0)->where('status', 0)->where('beneficiado', $user_id)->whereIn('descricao', $desc)->sum('valor');
        $ganhoTotal = $ganhos;
        return $ganhoTotal;
    }

    public function aptoGanhoTotal($user_id = '')
    {
        if ($user_id == '') {
            $user_id = $this->id;
        }
        $userInfo = $this->userInfo($user_id);

        $ganhoTotal = $this->totalGanhos($user_id);
        $valorPacote = Pacote::where('id', $userInfo['pacote'])->first()->valor;
        $limite = $valorPacote * 3;
        if ($ganhoTotal >= $limite) {
            return false;
        } else {
            return true;
        }
    }

    public function indicacaoUpgrade($usr, $valor)
    {
        $config = new config();
        $config = $config->getConfig();
        $bin = new Binario();
        $sup = $bin->getPaisLinear($usr, 12);

        $userInfo = $this->userInfo($usr);
        $desc = "Bônus de indicação DIRETA";
        $desc1 = "Bônus de indicação Indireto";
        $valorBonus = ($valor * 6) / 100;
        $valorIndiretos = ($valor * 1) / 100;

        //indicação direta
        $beneficiado = $this->userInfo($userInfo['pai_id']);


        if ($beneficiado->ativo == 1) {
            $this->addSaldo($userInfo['pai_id'], $valorBonus, $desc);
            $this->addSaldoLoja($userInfo['pai_id'], $valor, 'Ativação de cadastro');
        }

        //indicação indireta
        $i = 1;
        foreach ($sup as $value) {

            if ($i < 11) {
                $this->addSaldo($value, $valorIndiretos, $desc1);
            } else {
                break;
            }
            $i++;
        }


        //Rodar os Pontos Binário
        $bin->dist_binario($usr, $valor, 120);
    }


    public function addSaldoLoja($id, $valor, $desc)
    {
        $today = date("Y-m-d");
        $userInfo = $this->userInfo($id);
        extratos::create(['user_id' => 1, 'data' => $today, 'descricao' => $desc, 'valor' => $valor, 'beneficiado' => 1]);
    }




    function getStatus($status, $nm = '')
    {
        if ($nm <> '') {
            if ($status == 1) {
                return 'Sim';
            } else {
                return 'Não';
            }
        } else {
            if ($status == 1) {
                return 'Ativo';
            } else {
                return 'Inativo';
            }
        }
    }

    public function ativado()
    {
        return $this->ativo;
    }

    public function getShortName()
    {
        return strlen(Str::words($this->name, 1, '')) <= 5 ? Str::words($this->name, 2, '') : Str::words($this->name, 1, '');
    }

    public function getNascimento()
    {
        return date('d-m-Y', strtotime($this->nascimento));
    }

    public function memberSince()
    {
        return date('d F, Y', strtotime($this->created_at));
    }

    public function nIndicados()
    {
        return Referrals::where('pai_id', $this->id)->count();
    }

    public function apto_bonus($id, $text = '')
    {
        return $this->qualificado($id);
    }

    public function descontoUpgrade()
    {
        if ($this->pacote == 49) {
            return 0;
        } else {
            return $this->getPacote()->valor;
        }
    }

    public function getPacote()
    {
        return Pacote::where('id', $this->pacote)->first();
    }

    public function meu_desconto($produto, $user_id = '')
    {
        if ($user_id == '') {
            $user_id = \Auth::user()->id;
        }
        $userInfo = $this->userInfo($user_id);
        $dados = Produtos::where('id', $produto)->first();
        $descontos = json_decode($dados['descontos']);
        $pacote_id = $userInfo->pacote;

        $statusBin = $this->apto_bonus($userInfo->id);

        if (strtotime($userInfo->dataAtivacao) >= strtotime("2017-11-06")) {
            if ($statusBin) {
                return false;
            } else {
                return false;
            }
        } else {
            if (isset($descontos->$pacote_id) and $statusBin && $descontos->$pacote_id <> $dados->preco) {

                return false;
            } else {
                return false;
            }
        }
    }

    public function nIndicadosDir()
    {
        $dir['esquerda'] = Referrals::where('pai_id', $this->id)->where('direcao', 'esquerda')->count();
        $dir['direita'] = Referrals::where('pai_id', $this->id)->where('direcao', 'direita')->count();

        return $dir;
    }

    public function getIndicados($id = '')
    {
        if ($id == '') {
            $id = Auth::user()->id;
        }
        $reffer = Referrals::where('pai_id', $id)->get();

        $users = array();

        foreach ($reffer as $key => $r) {
            $users[$key] = $this->where('id', $r->user_id)->first();
        }

        return $users;
    }

    public function getFilhos($id = null, $count = false)
    {
        if (!$id) {
            $id = $this->id;
        }

        $reffer = Referrals::where('system_id', $id)->orderBy('direcao', 'DESC')->get();

        $users = array();

        foreach ($reffer as $key => $r) {
            $direcao = $this->getUserDirection($r->user_id);
            $user = $this->where('id', $r->user_id)->first();
            if(isset($user->direcao) && $user->direcao == 'esquerda'){
                $user->direcao = $direcao;
            }
            if(isset($user->direcao) && $user->direcao == 'direita'){
                $user->direcao = $direcao;
            }
            $users[$key] = $user;
        }

        if ($count) {
            return count($users);
        }

        return $users;
    }

    public function getUserDirection($user_id)
    {
        if (isset(Referrals::where('user_id', $user_id)->first()->direcao)) {
            return Referrals::where('user_id', $user_id)->first()->direcao;
        } else {
            return 'direita';
        }
    }

    public function getIndicador($user_id)
    {
        $refer = Referrals::where('user_id', $user_id)->first();
        $user = $this->where('id', $refer->pai_id)->first();

        return $user->id . " - " . $user->username;
    }

    public function getAvatar()
    {
        return Avatar::create($this->name)->toBase64();
    }

    public function criarFatura($pacote, $user, $preferencial = 0, $pagamento = 0, $status = 1, $pedido = '')
    {
        if ($pedido == '') {
            $pacote = Pacote::where('id', $pacote);
            $pacoteValor = str_replace('.', '', $pacote->first()->valor);
            $pacoteValor = str_replace(',', '.', $pacoteValor);
            $data = date('Y-m-d');
            $validade = date('Y-m-d', strtotime('+' . $pacote->first()->duracao_meses . 'month', strtotime($data)));
        } else {
            $data = date('Y-m-d');
            $validade = date('Y-m-d', strtotime('+ 2 days', strtotime($data)));
        }
        $validade = date('Y-m-d', strtotime('+ 3 days', strtotime($validade)));
        $values['user_id'] = $user;
        $values['status'] = $status;
        $values['validade'] = $validade;
        $values['data'] = $data;
        $values['pagamento_id'] = $pagamento;
        \DB::table('faturas')->insert($values);
    }

    public function getAccountAddress()
    {
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => "http://157.245.141.235/api/getaccountaddress?account=" . Auth::user()->username
        ]);
        $result = json_decode(curl_exec($curl));
        curl_close($curl);

        return $result->carteira;
    }

    public function getNewAddress()
    {
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => "http://157.245.141.235/api/getnewaddress?account=" . Auth::user()->username
        ]);
        $result = json_decode(curl_exec($curl));
        curl_close($curl);

        return $result->carteira;
    }

    public function getSaldo()
    {
        return number_format($this->saldo, 2, ',', '.');
    }

    public function getUserPai()
    {
        $pai = $this->where('id', $this->pai_id)->first();
        return $pai ? $pai : new $this;
    }

    public function visitas()
    {
        return Visitas::where('user_id', $this->id)->distinct()->count();
    }

    public function cVouchers()
    {
        return Voucher::where('user_id', $this->id)->where('status', 0)->count();
    }

    public function cVouchers2()
    {
        return \DB::table("vouchers_site")->where('user_id', $this->id)->count();
    }

    public function display_children($parent = null, $level = 0)
    {
        if (!$parent) {
            $parent = $this->id;
        }
        $count = 0;
        $result = \DB::select("SELECT user_id FROM referrals WHERE system_id=$parent");

        foreach ($result as $row) {
            $count += 1 + $this->display_children($row->user_id, $level + 1);
        }
        return $count;
    }

    /*  public function totalDireita($parent = null, $level = 0) {
      if (!$parent) {
      $parent = $this->id;
      }
      $count = 0;

      if ($level == 0) {
      $result = \DB::select("SELECT user_id,direcao FROM referrals WHERE system_id=$parent AND direcao ='direita'");
      } else {
      $result = \DB::select("SELECT user_id,direcao FROM referrals WHERE system_id=$parent");
      }
      foreach ($result as $row) {
      if ($level == 0) {
      if ($row->direcao == 'direita') {
      $count += 1 + $this->totalDireita($row->user_id, $level + 1);
      } else {
      $count += $this->totalDireita($row->user_id, $level + 1);
      }
      } else {
      $count += 1 + $this->totalDireita($row->user_id, $level + 1);
      }
      }
      return $count;
      }

      /*public function totalEsquerda($parent = null, $level = 0) {
      if (!$parent) {
      $parent = $this->id;
      }
      $count = 0;

      if ($level == 0) {
      $result = \DB::select("SELECT user_id,direcao FROM referrals WHERE system_id=$parent AND direcao ='esquerda'");
      } else {
      $result = \DB::select("SELECT user_id,direcao FROM referrals WHERE system_id=$parent");
      }

      foreach ($result as $row) {

      if ($row->direcao == 'esquerda') {
      $count += 1 + $this->totalEsquerda($row->user_id, $level + 1);
      } else {
      $count += $this->totalEsquerda($row->user_id, $level + 1);
      }
      }

      return $count;
      } */

    public function totalEsquerda($parent = null, $level = 0)
    {
        if (!$parent) {
            $parent = $this->id;
        }
        $netwrkHelper = new Binario();
        $res = \DB::table('referrals')->where('system_id', $parent)->where('direcao', 'esquerda');
        if ($res->count() > 0) {
            $filhoEsquerda = $res->first()->user_id;

            return count($netwrkHelper->getFilhos($filhoEsquerda)) + 1;
        } else {
            return 0;
        }
    }

    public function totalDireita($parent = null, $level = 0)
    {
        if (!$parent) {
            $parent = $this->id;
        }
        $netwrkHelper = new Binario();

        $res = \DB::table('referrals')->where('system_id', $parent)->where('direcao', 'direita');

        if ($res->count() > 0) {
            $filhoDireita = $res->first()->user_id;

            return count($netwrkHelper->getFilhos($filhoDireita)) + 1;
        } else {
            return 0;
        }
    }

    public function ativaUser()
    {
        $this->ativo = 1;
        return $this->save();
    }

    public function userInfo($val, $by = 'id')
    {
        $dados = array();
        $dados = User::where($by, $val)->first();
        return $dados;
    }

    public function removeSaldo($id, $valor, $desc = 1)
    {
        $today = date("Y-m-d");

        $userInfo = $this->userInfo($id);
        $novoSaldo = $userInfo['saldo'] - $valor;
        if (User::where('id', $id)->update(['saldo' => $novoSaldo])) {
            //cria extrato
            if ($desc <> 1) {
                extratos::create(['user_id' => 1, 'data' => $today, 'descricao' => $desc, 'valor' => (($valor) * (-1)), 'beneficiado' => $userInfo['id']]);
            }
        }
    }

    public function removeVulcoins($id, $valor, $desc = 1)
    {
        $today = date("Y-m-d");

        $userInfo = $this->userInfo($id);
        $novoSaldo = $userInfo['saldo_vulcoins'] - $valor;
        if (User::where('id', $id)->update(['saldo_vulcoins' => $novoSaldo])) {
            //cria extrato
            if ($desc <> 1) {
                extratos::create(['user_id' => 1, 'data' => $today, 'descricao' => $desc, 'valor' => (($valor) * (-1)), 'beneficiado' => $userInfo['id']]);
            }
        }
    }

    public function removePontosConsumo($id, $valor, $desc = 1)
    {
        $today = date("Y-m-d");;
        $userInfo = $this->userInfo($id);
        $novoSaldo = $userInfo['carteira_b'] - $valor;
        if (User::where('id', $id)->update(['carteira_b' => $novoSaldo])) {
            //cria extrato
            if ($desc <> 1) {
                extratos::create(['user_id' => 1, 'data' => $today, 'descricao' => $desc, 'valor' => (($valor) * (-1)), 'beneficiado' => $userInfo['id']]);
            }
        }
    }

    public function removePontos($id, $pontos, $dir = 'esquerda')
    {
        $today = date("Y-m-d");;
        $userInfo = $this->userInfo($id);
        if ($dir == 'esquerda') {
            $novoSaldo = $userInfo['binario_esquerda'] - $pontos;
            $update = User::where('id', $id)->update(['binario_esquerda' => $novoSaldo]);
        } else if ($dir == 'direita') {
            $novoSaldo = $userInfo['binario_direita'] - $pontos;
            $update = User::where('id', $id)->update(['binario_direita' => $novoSaldo]);
        }
    }

    public function addPontos($id, $pontos, $desc, $dir = 'esquerda')
    {
        $config = new config();
        $config = $config->getConfig();
        $bin = new Binario();
        $today = date("Y-m-d");

        $userInfo = $this->userInfo($id);
        if ($userInfo['ativo'] == 1) {


            $bin->logMsg($id . 'add ok' . $pontos, 'qualificado');

            if ($dir == 'esquerda') {
                $novoSaldo = $userInfo['binario_esquerda'] + $pontos;
                $update = User::where('id', $id)->update(['binario_esquerda' => $novoSaldo]);
                //cria extrato
                //extratos::create(['user_id' => 1, 'data' => $today, 'descricao' => $desc, 'valor' => $pontos, 'beneficiado' => $userInfo['id']]);
            } else if ($dir == 'direita') {
                $novoSaldo = $userInfo['binario_direita'] + $pontos;
                $update = User::where('id', $id)->update(['binario_direita' => $novoSaldo]);
                //cria extrato
                //extratos::create(['user_id' => 1, 'data' => $today, 'descricao' => $desc, 'valor' => $pontos, 'beneficiado' => $userInfo['id']]);
            }
        }
    }

    public function addPontosTotal($id, $pontos, $desc, $dir = 'esquerda')
    {

        $config = new config();
        $config = $config->getConfig();
        $bin = new Binario();
        $today = date("Y-m-d");

        $userInfo = $this->userInfo($id);
        if ($this->qualificado($id)) {


            $bin->logMsg($id . 'add ok' . $pontos, 'qualificado');
            $today = date("Y-m-d");
            $userInfo = $this->userInfo($id);
            if ($dir == 'esquerda') {
                $novoSaldo = $userInfo['total_bin_esq'] + $pontos;
                $update = User::where('id', $id)->update(['total_bin_esq' => $novoSaldo]);
                //cria extrato
            } else if ($dir == 'direita') {
                $novoSaldo = $userInfo['total_bin_dir'] + $pontos;
                $update = User::where('id', $id)->update(['total_bin_dir' => $novoSaldo]);
                //cria extrato
            }
        }
    }

    public function requisitosGraduacao($id, $graduacaoId)
    {
        $usrData = $this->userInfo($id);
        if ($usrData['total_bin_esq'] > $usrData['total_bin_dir']) {
            $pontuacao = $usrData['total_bin_dir'];
        } elseif ($usrData['total_bin_dir'] > $usrData['total_bin_esq']) {
            $pontuacao = $usrData['total_bin_esq'];
        } else {
            $pontuacao = 0;
        }
        if (isset($graduacaoAtual['name'])) {
            $gradPoints = $graduacaoAtual['pontuacao'];
        } else {
            $gradPoints = 0;
        }
        $graduacao = graduacoes::where('id', $graduacaoId)->first();
        if (isset($graduacao['name'])) {
            $gradPoints = $graduacao['pontuacao'];
        } else {
            $gradPoints = 0;
        }
        if (isset($graduacao['id'])) {
            $bin = new Binario();
            $filhos = $bin->getFilhos($id);
            $totalEncontrados = 0;
            if ($graduacao['grad_required'] == 0) {
                $totalEncontrados = Referrals::where('pai_id', $id)->count();
            } else {
                foreach ($filhos as $val) {
                    $usrData = $this->userInfo($val);
                    if ($usrData['graduacao'] == $graduacao['grad_required'] and $usrData['ativo'] == 1) {
                        $totalEncontrados = $totalEncontrados + 1;
                    }
                }
            }
            /*
             *
             *
             *
             *  if ($novaGraduacao['grad_required1'] == 0) {
              foreach ($filhos as $val) {
              $usrData = $this->userInfo($val);
              if ($usrData['ativo'] == 1) {
              $totalEncontrados1 = $totalEncontrados1 + 1;
              }
              }
              } else {
              foreach ($filhos as $val) {
              $usrData = $this->userInfo($val);
              if ($usrData['graduacao'] == $novaGraduacao['grad_required1'] and $usrData['ativo'] == 1) {
              $totalEncontrados1 = $totalEncontrados1 + 1;
              }
              }
              }
             */
            $requisitos['qntd_graduados'] = $graduacao['qntd_required'] - $totalEncontrados;
            if ($requisitos['qntd_graduados'] < 1) {
                $requisitos['qntd_graduados'] = 0;
            }
            $requisitos['pontuacao'] = $graduacao['pontuacao'] - $pontuacao;
            if ($requisitos['pontuacao'] < 1) {
                $requisitos['pontuacao'] = 0;
            }
            $requisitos['nome'] = $graduacao['name'];

            $totalEncontrados1 = 0;
            if ($graduacao['grad_required1'] == 0) {
                $totalEncontrados1 = Referrals::where('pai_id', $id)->count();
            } else {
                foreach ($filhos as $val) {
                    $usrData = $this->userInfo($val);
                    if ($usrData['graduacao'] == $graduacao['grad_required1'] and $usrData['ativo'] == 1) {
                        $totalEncontrados1 = $totalEncontrados1 + 1;
                    }
                }
            }
            $requisitos[2] = '';
            $requisitos[2]['qntd_graduados'] = $graduacao['qntd_required1'] - $totalEncontrados1;
            if ($requisitos[2]['qntd_graduados'] < 1) {
                $requisitos[2]['qntd_graduados'] = 0;
            }




            return $requisitos;
        }
    }

    public function verificar_graduacao()
    {
        $id = $this->id;
        $usrData = $this->userInfo($id);



        //pega  pontuação menor
        if ($usrData['total_bin_esq'] > $usrData['total_bin_dir']) {
            $pontuacao = $usrData['total_bin_dir'];
        } elseif ($usrData['total_bin_dir'] > $usrData['total_bin_esq']) {
            $pontuacao = $usrData['total_bin_esq'];
        } else {
            $pontuacao = 0;
        }
        $graduacaoAtual = graduacoes::where('id', $usrData['graduacao'])->first();
        if (isset($graduacaoAtual['name'])) {
            $gradPoints = $graduacaoAtual['pontuacao'];
        } else {
            $gradPoints = 0;
        }
        $graduacoes = graduacoes::where('status', 1)->where('pontuacao', '>', $pontuacao)->first();
        //verifica se esta apto para uma graduação
        $novaGraduacao = graduacoes::where('id', '<>', $usrData['graduacao'])->where('pontuacao', '<', $pontuacao)->where('pontuacao', '>', $gradPoints)->first();
        //pontuação suficiente ok
        if ((isset($novaGraduacao['name'])) and $pontuacao >= $novaGraduacao['pontuacao']) {
            $bin = new Binario();

            $filhos = $bin->getFilhos($id);
            $totalEncontrados = 0;
            $totalEncontrados1 = 0;


            if ($novaGraduacao['grad_required1'] == 0) {
                foreach ($filhos as $val) {
                    $usrData = $this->userInfo($val);
                    if ($usrData['ativo'] == 1) {
                        $totalEncontrados1 = $totalEncontrados1 + 1;
                    }
                }
            } else {
                foreach ($filhos as $val) {
                    $usrData = $this->userInfo($val);
                    //echo '<br>id: '.$usrData['id']." Status: ".$usrData['ativo'].'  graduação: '.$usrData['graduacao'].' dfgrad: '.$novaGraduacao['grad_required1'];
                    if ($usrData['graduacao'] == $novaGraduacao['grad_required1'] and $usrData['ativo'] == 1) {
                        $totalEncontrados1 = $totalEncontrados1 + 1;
                    }
                }
            }

            if ($novaGraduacao['grad_required'] == 0) {
                foreach ($filhos as $val) {
                    $usrData = $this->userInfo($val);
                    if ($usrData['ativo'] == 1) {
                        $totalEncontrados = $totalEncontrados + 1;
                    }
                }
            } else {
                foreach ($filhos as $val) {
                    $usrData = $this->userInfo($val);
                    if ($usrData['graduacao'] == $novaGraduacao['grad_required'] and $usrData['ativo'] == 1) {
                        $totalEncontrados = $totalEncontrados + 1;
                    }
                }
            }



            if (($novaGraduacao['qntd_required'] <= $totalEncontrados) and ($novaGraduacao['qntd_required1'] <= $totalEncontrados1)) {
                User::where('id', $id)->update(['graduacao' => $novaGraduacao['id']]);
                return $novaGraduacao;
            }
        } else {
            return false;
        }
    }

    public function pontosRecebidos($id)
    {
        $config = new config();
        $config = $config->getConfig();
        return extratos::where('beneficiado', $id)->where('descricao', 'Pontos Binários')->where('status', 0)->sum('valor') * $config['binario_valor'];
    }

    public function qualificado($id = "")
    {
        if ($id == "") {
            $id = $this->id;
        }
        $bin = new Binario();
        $esquerda = Referrals::where('pai_id', $id)->where('direcao', 'esquerda');

        $direita = Referrals::where('pai_id', $id)->where('direcao', 'direita');

        $userData = User::where('id', $id)->first();
        $pacoteInfo3 = Pacote::where('id', $userData->pacote)->first();
        if ($pacoteInfo3['binario'] == 0) {
            return false;
        }
        $dirOk = false;
        $esqOk = false;
        if ($esquerda->count() > 0 and $direita->count() > 0) {
            $userEsq = $this->userInfo($esquerda->first()->user_id);
            $userDir = $this->userInfo($direita->first()->user_id);
            $totalUsr = User::where('pai_id', $id)->where('ativo', 1)->count();

            $pacoteInfo1 = Pacote::where('id', 1)->first();
            $pacoteInfo2 = Pacote::where('id', 1)->first();
            
            
            return true;
            //$userData->ativo == 1 and $pacoteInfo3->binario == 1
            /*if (isset($userDir) and $userDir->ativo == 1 and $userEsq->ativo == 1 and $totalUsr >= 2 and $pacoteInfo2->binario == 1 and $pacoteInfo1->binario == 1) {
                return true;
            } else {

                if (isset($esquerda->first()->user_id)) {
                    $esquerdaFilhos = $bin->getFilhos($esquerda->first()->user_id, 0);
                    foreach ($esquerdaFilhos as $value) {
                        $value = User::where('id', $value)->first();
                        if (isset($value->pacote)) {
                            $pacoteInfo1 = Pacote::where('id', $value->pacote)->first();
                            // echo $value->id . ' ' . $pacoteInfo1['valor'] . '<br>';

                            if ($value->ativo == 1 and $pacoteInfo1->binario == 1) {

                                $esqOk = true;
                                break;
                            }
                        }
                    }
                }
                if (isset($direita->first()->user_id)) {
                    $direitaFilhos = $bin->getFilhos($direita->first()->user_id, 0);

                    foreach ($direitaFilhos as $value) {
                        $value = User::where('id', $value)->first();
                        if (isset($value->pacote)) {
                            $pacoteInfo1 = Pacote::where('id', $value->pacote)->first();
                            /// echo $value->id . ' ' . $pacoteInfo1['valor'] . '<br>';

                            if ($value->ativo == 1 and $pacoteInfo1->binario == 1) {
                                //echo 'ok1';
                                $dirOk = true;
                                break;
                            }
                        }
                    }
                }
                if ($dirOk and $esqOk) {
                    return true;
                } else {
                    return false;
                }
            }*/
        } else {
            return false;
        }
    }

    public function qualificadoBinario($id)
    {

        return $this->qualificado($id);
    }

    public function minhaGraduacao($id = '')
    {
        if ($id == '') {
            $id = $this->id;
        }
        $userData = $this->userInfo($id);

        @$graduacao = graduacoes::where('id', $userData['graduacao'])->first()->name;
        if ($graduacao) {
            return $graduacao;
        } else {
            return 'Sem graduação';
        }
    }

    public function minhaGraduacaoIcone($id = '')
    {
        if ($id == '') {
            $id = $this->id;
        }
        $userData = $this->userInfo($id);

        @$graduacao = graduacoes::where('id', $userData['graduacao'])->first()->icone;
        return $graduacao;
    }

    public function verificarPin($pin, $custom = 0)
    {
        $pin_salvo = \Auth::user()->pin;
        if (Hash::check($pin, $pin_salvo)) { } else {
            if ($custom == 0) {
                return false;

            } else {
                return false;
            }
        }
    }

    public function addDiv($userId, $valor)
    {
        $pacote->expirados();

        $cota = DB::table('cotas')->where('user_id', $userId)->where('status', 1)->first();
        if (isset($cota->id)) {
            $id = $cota->id;

            $totalDivs = $cota->qntd_divisoes + $valor;
            \DB::table('cotas')->where('id', $cota->id)->update(['qntd_divisoes' => $totalDivs]);
        }
    }

    public function removeDiv($userId, $valor)
    {
        $pacote = new Pacote();

        $pacote->expirados();
        $cota = \DB::table('cotas')->where('user_id', $userId)->where('status', 1)->first();
        if (isset($cota->id)) {
            $id = $cota->id;

            $totalDivs = $cota->qntd_divisoes - $valor;
            \DB::table('cotas')->where('id', $cota->id)->update(['qntd_divisoes' => $totalDivs]);
        }
    }

    public function totalCotas($id = '')
    {
        if ($id == '') {
            $id = \Auth::user()->id;
        }
        $pacote = new Pacote();
        $pacote->expirados();

        $total = \DB::table('cotas')->where('user_id', $id)->where('status', 1)->sum('quantidade');
        return $total;
    }

    public function totalDivs($id)
    {
        if ($id == '') {
            $id = \Auth::user()->id;
        }
        $pacote = new Pacote();
        $pacote->expirados();

        $total = \DB::table('cotas')->where('user_id', $id)->where('status', 1)->sum('qntd_divisoes');
        return $total;
    }

    public function criarCotas($pacote, $user, $preferencial = 0, $pagamento = 0, $status = 1)
    {
        /* SELECT `id`, `user_id`, `validade`, `status`, `pagamento_id`, `data` FROM `faturas` WHERE 1 */
        $pacote = Pacote::where('id', $pacote);
        $pacoteValor = str_replace('.', '', $pacote->first()->valor);
        $pacoteValor = str_replace(',', '.', $pacoteValor);
        $data = date('Y-m-d');

        $validade = date('Y-m-d', strtotime('+ 5 days', strtotime($data)));
        $values['user_id'] = $user;
        $values['status'] = $status;
        $values['validade'] = $validade;
        $values['data'] = $data;
        $values['pagamento_id'] = $pagamento;
        \DB::table('faturas')->insert($values);
    }

    //add carteira_b
    public function addValueColumn($id, $valor, $desc, $column)
    {
        $today = date("Y-m-d");

        $userInfo = $this->userInfo($id);
        $novoSaldo = $userInfo[$column] + $valor;
        if ($userInfo['ativo'] == 1) {
            if (User::where('id', $id)->update([$column => $novoSaldo])) {
                //cria extrato
                extratos::create(['user_id' => 1, 'data' => $today, 'descricao' => $desc, 'valor' => $valor, 'beneficiado' => $userInfo['id']]);
            }
        }
    }

    //add carteira_b
    public function removeValueColumn($id, $valor, $desc, $column)
    {
        $today = date("Y-m-d");

        $userInfo = $this->userInfo($id);
        $novoSaldo = $userInfo[$column] - $valor;
        if ($userInfo['ativo'] == 1) {
            if (User::where('id', $id)->update([$column => $novoSaldo])) {
                //cria extrato
                extratos::create(['user_id' => 1, 'data' => $today, 'descricao' => $desc, 'valor' => $valor, 'beneficiado' => $userInfo['id']]);
            }
        }
    }

    public function addSaldo($id, $valor, $desc)
    {
        $today = date("Y-m-d");

        $userInfo = $this->userInfo($id);
        $novoSaldo = $userInfo['saldo'] + $valor;
        if ($userInfo['ativo'] == 1) {
            if (User::where('id', $id)->update(['saldo' => $novoSaldo])) {
                //cria extrato
                extratos::create(['user_id' => 1, 'data' => $today, 'descricao' => $desc, 'valor' => $valor, 'beneficiado' => $userInfo['id']]);
            }
        }
    }

    public function addVulcoins($id, $valor, $desc) {
        $today = date("Y-m-d");

        $userInfo = $this->userInfo($id);


        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => "http://157.245.141.235/api/moveUser?toaccount=" . $userInfo['username'] . "&amount=" . $valor
        ]);
        $result = json_decode(curl_exec($curl));
        curl_close($curl);


        if ($result->status == 200) {
            //cria extrato
            extratos::create(['user_id' => 1, 'data' => $today, 'descricao' => $desc, 'valor' => $valor, 'beneficiado' => $userInfo['id']]);
        }
    }

    public function addPontoConsumo($id, $valor, $desc)
    {
        $today = date("Y-m-d");

        $userInfo = $this->userInfo($id);
        $novoSaldo = $userInfo['carteira_b'] + $valor;
        if ($userInfo['ativo'] == 1) {
            if (User::where('id', $id)->update(['carteira_b' => $novoSaldo])) {
                //cria extrato
                extratos::create(['user_id' => 1, 'data' => $today, 'descricao' => $desc, 'valor' => $valor, 'beneficiado' => $userInfo['id']]);
            }
        }
    }

    public function ativarCota($id)
    {
        $usr = new User();
        $fatura = \DB::table('faturas')->where('id', $id)->where('status', 0);

        if (isset($fatura->first()->user_id)) {
            $dadosUsr = User::where('id', $fatura->first()->user_id)->first();
            $faturaData = $fatura->first();
            $hoje = strtotime(date('Y-m-d'));

            $validade = strtotime($faturaData->validade);
            if ($hoje < $validade) {
                if (isset($faturaData->id)) {
                    $voucher = new Http\Controllers\Admin\VoucherController();
                    $pagamentoInfo = \DB::table('pagamentos')->where('id', $faturaData->pagamento_id)->first();
                    $pacoteInfo = \App\Pacote::where('id', $pagamentoInfo->pacote)->first();
                    if ($voucher->mudarPacote($dadosUsr['id'], $pagamentoInfo->pacote)) {

                        //ativa
                        $validade = date('Y-m-d', strtotime('+' . $pacoteInfo->validade . 'month', strtotime(date('Y-m-d'))));
                        $validade = date('Y-m-d', strtotime('+ 3 days', strtotime($validade)));
                        $fatura->update(['status' => 1, 'data' => date('Y-m-d'), 'validade' => $validade]);

                        return true;
                    } else {
                        return false;
                    }
                } else {
                    echo 'Fatura não encontrada';
                    return false;
                }
            } else {
                echo 'Fatura expirada';
                return false;
            }
        } else {
            echo 'Fatura não encontrada';
            return false;
        }
    }

    public function indicacao($usr)
    {
        $config = new config();
        $config = $config->getConfig();
        $bin = new Binario();
        $sup = $bin->getPaisLinear($usr, 7);

        $userInfo = $this->userInfo($usr);
        $pacoteData = Pacote::where('id', $userInfo['pacote'])->first();
        $desc = "Bônus de indicação";

        //indicação direta
        $beneficiado = $this->userInfo($userInfo['pai_id']);
        if ($beneficiado->pacote == 0) {
            if ($pacoteData->id > $beneficiado->pacote) {
                $this->addSaldo($userInfo['pai_id'], $pacoteData->indicacao_direta, $desc);
            }
        } else {
            $this->addSaldo($userInfo['pai_id'], $pacoteData->indicacao_direta, $desc);
        }



        //indicação indireta
        $i = 1;
        foreach ($sup as $value) {
            $index2 = 'nivel' . $i;
            if (isset($pacoteData[$index2])) {
                $valor = $pacoteData[$index2];
                //echo $value . ' ' . $valor . ' ' . $index2 . '<br>';
                if (!empty($value) && $userInfo['pai_id'] <> $value) {
                    $beneficiado = $this->userInfo($value);
                    if ($beneficiado->pacote == 0) {
                        if ($pacoteData->id > $beneficiado->pacote) {
                            $this->addSaldo($value, $valor, $desc);
                        }
                    } else {
                        $this->addSaldo($value, $valor, $desc);
                    }
                }
            } else {
                break;
            }
            $i++;
        }
    }

    public function removeVirgula($param)
    {
        $pacoteValor = str_replace('.', '', $param);

        $pacoteValor = str_replace(',', '.', $pacoteValor);
        return $pacoteValor;
    }

    public function isPowerDirect($id)
    {
        if ($id == '') {
            $id = \Auth::user()->id;
        }
        $userData = $this->userInfo($id);
        $filhosAtivos = User::where('pai_id', $id)->where('ativo', 1);
        $totalFilhos = 0;
        $extrato = extratos::where('user_id', $id)->where('descricao', 'PowerDirect')->orderBy('id', 'desc')->first();
        $validade = date('y-m-d', strtotime('+ 1 month', strtotime($extrato['data'])));

        if (strtotime(date('y-m-d')) < strtotime($validade)) { } else {
            if ($filhosAtivos->count() >= 10) {
                foreach ($filhosAtivos->get() as $filho) {
                    if (User::where('id', $filho['id'])->where('ativo', 1)->count() >= 2) {
                        $totalFilhos = $totalFilhos + 1;
                    }
                }
                if ($totalFilhos >= 10) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    }

    public static function pagamentoValor($id)
    {
        $pagamento = Pagamentos::where('id', $id)->first();
        $pacote = Pacote::where('id', $pagamento['pacote'])->first();
        $usuario = User::where('id', $pagamento['user_id'])->first();
        $pacoteUsuario = Pacote::where('id', $usuario['pacote'])->first();
        $valor = $pacote['valor'];
        /*  if($pagamento['tipo']=='4' and $valor>100){
          $valor=$valor-100;
          } */
        return $valor;
    }

    public static function IsManagerDirect($user_id)
    {
        $user = new User();
        $userInfo = $user->userInfo($user_id);
        if ($userInfo['pacote'] >= 3) {
            $indicadosDiretos = User::where('pai_id', $user_id)->where('ativo', 1);
            if ($indicadosDiretos->count() >= 10) {
                $encontrados = 0;
                foreach ($indicadosDiretos->get() as $value) {
                    $indicadosDiretos2 = User::where('pai_id', $value['id'])->where('ativo', 1)->count();
                    if ($indicadosDiretos2 >= 2) {
                        $encontrados = $encontrados + 1;
                    }
                }
                if ($encontrados >= 10) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function produto_pacote($user, $powerBack = false)
    {
        $userInfo = $this->userInfo($user);

        $pacote = Pacote::where('id', $userInfo['pacote'])->first();
        $produtosArray = explode(",", $pacote['produtos']);

        $j = 0;
        foreach ($produtosArray as $value) {
            if (Produtos::where('id', $value)->count() == 0) {
                unset($produtosArray[$j]);
            }
            $j++;
        }
        if (count($produtosArray) > 0) {
            $produto = Produtos::where('id', $pacote['produto_id'])->first();
            foreach ($produtosArray as $value) {
                $carrinho_id = \DB::table('carrinhos')->insertGetId(['user_id' => $userInfo['id'], 'product_id' => $value]);
            }
            if ($carrinho_id) {
                $carrinhos = Carrinho::where('pedido', 0)->where(
                    'user_id',
                    $userInfo['id']
                );
                $carrinhos = $carrinhos->select(\DB::raw('COUNT(*) as quantidade', 'product_id'), 'product_id', 'img', 'carrinhos.id', 'nome', 'preco', 'peso')->join('produtos', 'produtos.id', '=', 'carrinhos.product_id')->groupBy('product_id')->get()->toArray();
                $produto = [];
                foreach ($carrinhos as $key => $dado) {
                    $array = $dado;
                    $array['produto'] = $dado;

                    $produto[] = $array;
                }
                $produtos = $produto;

                $subtotal = 0;
                $descricao = '';

                foreach ($produtos as $produto) {
                    $quantidade = $produto['quantidade'];
                    $preco = $produto['produto']['preco'] * $quantidade;
                    $subtotal += $preco;
                    $descricao .= '<br> ' . $produto['produto']['nome'] . '  R$' . number_format($preco, 2) . ' Quantidade: ' . $quantidade;
                }
                if ($produtos) {
                    $endereco = Enderecos::where('user_id', $userInfo['id'])->first();
                    $pedido = new Pedidos;
                    $pedido->user_id = $userInfo['id'];
                    $pedido->id_endereco = $endereco['id'];
                    $pedido->status = "Pago";
                    $pedido->preco = $subtotal;
                    $pedido->produtos = json_encode($produtos);
                    $pedido->date = date("Y-m-d");
                    if (!$powerBack) {
                        $pedido->info = 'Entrega pendente,breve enviaremos as informações de entrega.';
                    } else {
                        $pedido->info = 'Bônus Cashback-Entrega pendente,breve enviaremos as informações de entrega.';
                    }
                    $pedido->save();
                    if (!$powerBack) {
                        extratos::create(['user_id' => $userInfo->id, 'data' => date('Y-m-d'), 'descricao' => 'Compra de kit<br>' . $descricao, 'valor' => $subtotal, 'beneficiado' => 1, 'tipo' => 7]);
                    } else {
                        extratos::create(['user_id' => 1, 'data' => date('Y-m-d'), 'descricao' => 'Bônus cashback-Pedido ' . $pedido->id . '<br>' . $descricao, 'valor' => $subtotal, 'beneficiado' => $userInfo->id, 'tipo' => 7]);
                        extratos::create(['user_id' => $userInfo->id, 'data' => date('Y-m-d'), 'descricao' => 'Bônus cashback-Pedido ' . $pedido->id . '<br>' . $descricao, 'valor' => $subtotal, 'beneficiado' => 1, 'tipo' => 7]);
                    }
                    $carrinhos = Carrinho::where('pedido', 0)->where(
                        'user_id',
                        $userInfo->id
                    )->update(['pedido' => $pedido->id]);
                    return ['id' => $pedido->id, 'descricao' => $descricao];
                }
                return false;
            }
            return false;
        }
        return false;
    }

    public function qualificadoPowerBack($id = '')
    {
        if ($id == '') {
            $id = $this->id;
        }
        $userData = $this->userInfo($id);
        if (isset($userData['id'])) {
            if ($userData['bonus_cashbak'] == 'Concluído') {
                return false;
            }
            $validade = date('Y-m-d', strtotime('+ 1 month', strtotime($userData->dataAtivacao)));
            $validade = date('Y-m-d', strtotime('+ 3 days', strtotime($validade)));

            if (strtotime($validade) >= strtotime(date('Y-m-d'))) {
                $usuariosIndicados = User::where('pai_id', $userData->id)->where('ativo', 1)->get();
                $pontosProdutos = 0;
                foreach ($usuariosIndicados as $indicado) {
                    $indicadoPacote = Pacote::where('id', $indicado['pacote'])->first();
                    if (isset($indicadoPacote['valor_binario'])) {
                        // echo $indicadoPacote['valor_binario'] . '<br>';
                        $pontosProdutos += $indicadoPacote['valor_binario'];
                    }
                }
                $pacoteUsuario = Pacote::where('id', $userData['pacote'])->first();
                if (isset($pacoteUsuario['id'])) {
                    if ($pontosProdutos >= ($pacoteUsuario['valor_binario'] * 4)) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function getPontosMes()
    {

        $pedidos = Pedidos::where('user_id', $this->id)->where('id_pag', '<>', '')->where('date', 'like', '%' . date('Y-m') . '%')->whereIn('status', ['Encaminhado', 'Pago'])->get();
        $pontos = 0;
        foreach ($pedidos as $pedido) {
            $pedidoData = json_decode($pedido->produtos);
            foreach ($pedidoData as $value) {
                $producData = Produtos::where('id', $value->product_id)->first();
                if (isset($producData['id'])) {
                    $pontos += ($value->quantidade * $value->preco) * $producData->porcentagem_pontos;
                }
            }
        }
        return $pontos;
    }

    public function emAtraso()
    {
        $hoje = strtotime(date('Y-m-d'));
        $vencimento = strtotime($this->validade_pacote);

        $pre_vencimento = date('Y-m-d', strtotime('-10 days', strtotime($this->validade_pacote)));
        $pos_vencimento = date('Y-m-d', strtotime('+10 days', strtotime($this->validade_pacote)));
        if ($hoje >= strtotime($pos_vencimento)) {
            return true;
        } else {
            return false;
        }
    }

    public function statusRenovacao()
    {
        $hoje = strtotime(date('Y-m-d'));
        $vencimento = strtotime($this->validade_pacote);
        $pre_vencimento = date('Y-m-d', strtotime('-10 days', strtotime($this->validade_pacote)));
        $pos_vencimento = date('Y-m-d', strtotime('+10 days', strtotime($this->validade_pacote)));
        if ($hoje >= strtotime($pre_vencimento)) {
            return true;
        } else {
            return false;
        }
    }

    public function renovaUsuario()
    {
        if ($this->statusRenovacao() or $this->emAtraso()) {
            if ($this->getPontosMes() >= 50) {
                $vouch = new Http\Controllers\Admin\VoucherController();
                $vouch->addValidadePacote($this->id);
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function atraso6meses()
    {
        $hoje = strtotime(date('Y-m-d'));
        $vencimento = strtotime($this->validade_pacote);
        $pos_vencimento = date('Y-m-d', strtotime('+ 6 month', strtotime($this->validade_pacote)));
        if ($hoje >= strtotime($pos_vencimento)) {
            return true;
        } else {
            return false;
        }
    }

    public function desativaEmAtraso()
    {
        if ($this->atraso6meses() && $this->admin == 0) {
            $this->update(['ativo' => 0]);
        }
    }

    public function obterPacote($pacote_id)
    {
        return Pacote::where('id', $pacote_id)->first();
    }
}
