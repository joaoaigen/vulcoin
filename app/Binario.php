<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\config;
use DB;

class Binario extends Model {

    protected $connection= 'mysql';

    protected $table = 'binarios';
    
    public $ids = array();
    public $ids2 = array();
    protected $fillable = ['user_id', 'pontos', 'data'];
    public $PontosFilhos = array();
    public $PontosFilhos2 = array();
    public $filhosBin = array();
    public $paisBin = array();

    public function getPontos($id) {
        $pontos = Binario::where('user_id', $id)->sum('pontos');
        if ($pontos == '') {
            $pontos = 0;
        }
        return $pontos;
    }

    public function getDirs($id) {
        $ids = Referrals::where('system_id', $id)->get();

        return $ids;
    }

    public function totalEsquerdaPt($parent = null, $level = 0) {

        if (!$parent) {
            $parent = $this->id;
        }
        $count = 0;

        if ($level == 0) {
            $result = \DB::select("SELECT user_id,direcao FROM referrals WHERE system_id=$parent");
        } else {
            $result = \DB::select("SELECT user_id,direcao FROM referrals WHERE system_id=$parent");
        }

        foreach ($result as $row) {

            if ($row->direcao == 'esquerda') {
                $count += 1 + $this->totalEsquerdaPt($row->user_id, $level + 1);
                $this->ids[$row->user_id] = $this->getPontos($row->user_id);
            } else {
                $count += $this->totalEsquerdaPt($row->user_id, $level + 1) . '<br>';
                $this->ids[$row->user_id] = $this->getPontos($row->user_id) . '<br>';
            }
        }

        return $count;
    }

    public function totalDireitaPts($parent = null, $level = 0) {
        if (!$parent) {
            $parent = $this->id;
        }
        $count = 0;

        if ($level == 0) {
            $result = \DB::select("SELECT user_id,direcao FROM referrals WHERE system_id=$parent AND direcao ='direita'");
        } else {
            $result = \DB::select("SELECT user_id,direcao FROM referrals WHERE system_id=$parent");
        }
        foreach ($result as $row) {
            if ($level == 0) {
                if ($row->direcao == 'direita') {
                    $count += 1 + $this->totalDireitaPts($row->user_id, $level + 1);
                    $this->ids[$row->user_id] = $this->getPontos($row->user_id);
                } else {
                    $count += $this->totalDireitaPts($row->user_id, $level + 1);
                    $this->ids[$row->user_id] = $this->getPontos($row->user_id);
                }
            } else {
                $count += 1 + $this->totalDireitaPts($row->user_id, $level + 1);
                $this->ids[$row->user_id] = $this->getPontos($row->user_id);
            }
        }
        return $count;
    }

    public function totalDireita($parent = null) {
        /* $this->PontosFilhos2 = array();
          $idPrin = $parent;
          $u = User::where('id', $parent)->first();
          $direita = Referrals::where('system_id', $parent)->where('direcao', 'direita')->first()['user_id'];
          if ($direita == '' or $direita < 0) {
          $totalPontos = 0;
          } else {
          $this->getFilhosPontos2($direita);
          if ($u['less_points_dir'] == '') {
          $u['less_points_dir'] = 0;
          }
          if ($u['paid_points_dir'] == '') {
          $u['paid_points_dir'] = 0;
          }

          //- $u['paid_points_dir']) - $u['less_points_dir']
          $totalPontos = ((array_sum($this->PontosFilhos2) + $this->getPontos($direita)) - $u['paid_points_dir']) - $u['less_points_dir'];
          } */
        $totalPontos = User::where('id', $parent)->first()->binario_direita;
        return $totalPontos;
    }

    public function getFilhosPontos($id) {
        $reffer = Referrals::where('system_id', $id)->get();
        $users = array();
        foreach ($reffer as $key => $r) {
            $this->PontosFilhos[$r->user_id] = $this->getPontos($r->user_id);
            $this->getFilhosPontos($r->user_id);
        }
    }

    public function getFilhosPontos2($id) {
        $reffer = Referrals::where('system_id', $id)->get();
        $users = array();
        foreach ($reffer as $key => $r) {
            $this->PontosFilhos2[$r->user_id] = $this->getPontos($r->user_id);
            $this->getFilhosPontos2($r->user_id);
        }
    }

    public function totalEsquerda($parent = null) {
        /* $idPrin = $parent;
          $this->PontosFilhos = array();
          $u = User::where('id', $parent)->first();

          $esquerda = Referrals::where('system_id', $parent)->where('direcao', 'esquerda')->first()['user_id'];
          if ($esquerda == '' or $esquerda < 0) {
          $totalPontos = 0;
          } else {
          $this->getFilhosPontos($esquerda);
          if ($u['less_points_dir'] == '') {
          $u['less_points_dir'] = 0;
          }
          if ($u['paid_points_dir'] == '') {
          $u['paid_points_dir'] = 0;
          }
          //- $u['paid_points_dir']) - $u['less_points_dir']
          $totalPontos = ((array_sum($this->PontosFilhos) + $this->getPontos($esquerda)) - $u['paid_points_esq']) - $u['less_points_esq'];
          } */
        $totalPontos = User::where('id', $parent)->first()->binario_esquerda;
        return $totalPontos;
    }

//reconstrução do binário
    public function getFilhos($id, $level = 0, $last = 0) {
        $reffer = Referrals::where('system_id', $id)->get();
        $users = array();
        $level = $level + 1;
        if ($last > 0) {
            if ($level <= $last) {
                foreach ($reffer as $key => $r) {
                    $this->filhosBin[count($this->filhosBin)] = $r->user_id;
                    //echo "<b>Id:</b> {$r->user_id} Nivel: $level <br> ";
                    $this->getFilhos($r->user_id, $level, $last);
                }
            }
        } else {
            foreach ($reffer as $key => $r) {
                $this->filhosBin[count($this->filhosBin)] = $r->user_id;
                // echo "<b>Id:</b> {$r->user_id} Nivel: $level <br> ";
                $this->getFilhos($r->user_id, $level, $last);
            }
        }
        return $this->filhosBin;
    }

    public function getFilhosLevel($id, $level = 0, $last = 0) {
        $reffer = Referrals::where('system_id', $id)->get();
        $users = array();
        $level = $level + 1;
        if ($last > 0) {
            if ($level <= $last) {
                foreach ($reffer as $key => $r) {
                    $count = count($this->filhosBin);
                    $this->filhosBin[$count]['level'] = $level;
                    $this->filhosBin[$count]['user'] = $r->user_id;
                    $this->getFilhosLevel($r->user_id, $level, $last);
                }
            }
        } else {
            foreach ($reffer as $key => $r) {
                $count = count($this->filhosBin);
                $this->filhosBin[$count]['level'] = $level;
                $this->filhosBin[$count]['user'] = $r->user_id;
                $this->getFilhosLevel($r->user_id, $level, $last);
            }
        }
        return $this->filhosBin;
    }

    function logMsg($msg, $grupo) {
        // variável que vai armazenar o nível do log (INFO, WARNING ou ERROR)
        $today = date("d-m-Y");
        $data['data'] = $today;
        $data['mensagem'] = $msg;
        $data['grupo'] = $grupo;
        DB::table('logs')->insert($data);
    }

    public function getPaisPid($id, $level = 1, $last = 0) {
        $usr = new User();

        $reffer = Referrals::where('user_id', $id)->get();
        $users = array();
        $level = $level + 1;
        if ($last > 0) {
            if ($level <= $last) {
                foreach ($reffer as $key => $r) {

                    $this->paisBin[count($this->paisBin)] = $r->pai_id;
                    $this->getPaisPid($r->pai_id, $level, $last);
                }
            }
        } else {
            foreach ($reffer as $key => $r) {
                $this->paisBin[count($this->paisBin)] = $r->pai_id;
                $this->getPaisPid($r->pai_id, $level, $last);
            }
        }
        return $this->paisBin;
    }

    private function returnPai($id) {
        $usr = new User();
        $pai = $usr->userInfo($id)['pai_id'];

        if (!empty($pai)) {
            return $pai;
        } else {
            return false;
        }
    }

    /* SELECT id, saldo, username, pai_id
      FROM  `users`
      WHERE 1
      LIMIT 0 , 30 */

    public function getPaisLinear($id, $last) {
        $usr = new User();
        for ($index = 1; $index <= $last; $index++) {
            if ($index == 1) {
                $pai[$index] = $this->returnPai($id);
            } else {
                $pai[$index] = $this->returnPai($pai[$index - 1]);
            }
            if (!$pai[$index]) {
                unset($pai[$index]);
                break;
            }
        }
        return $pai;
    }

    public function getPais($id, $level = 1, $last = 0) {
        $usr = new User();

        $reffer = Referrals::where('user_id', $id)->get();
        $users = array();
        $level = $level + 1;
        if ($last > 0) {
            if ($level <= $last) {
                foreach ($reffer as $key => $r) {

                    $this->paisBin[count($this->paisBin)] = $r->system_id;
                    $this->getPais($r->system_id, $level, $last);
                }
            }
        } else {
            foreach ($reffer as $key => $r) {
                $this->paisBin[count($this->paisBin)] = $r->system_id;
                $this->getPais($r->system_id, $level, $last);
            }
        }
        return $this->paisBin;
    }

    public function getPaisLevel($id, $level = 0, $last = 0) {
        $usr = new User();

        $reffer = Referrals::where('user_id', $id)->get();
        $users = array();
        $level = $level + 1;
        if ($last > 0) {
            if ($level <= $last) {
                foreach ($reffer as $key => $r) {
                    $count = count($this->paisBin);
                    $this->paisBin[$count]['level'] = $level;
                    $this->paisBin[$count]['user'] = $r->system_id;
                    $this->getPaisLevel($r->system_id, $level, $last);
                    /* $count = count($this->filhosBin);
                      $this->filhosBin[$count]['level'] = $level;
                      $this->filhosBin[$count]['user'] = $r->user_id;
                      $this->getFilhosLevel($r->user_id, $level, $last);
                     * */
                }
            }
        } else {
            foreach ($reffer as $key => $r) {
                $count = count($this->paisBin);
                $this->paisBin[$count]['level'] = $level;
                $this->paisBin[$count]['user'] = $r->system_id;
                $this->getPaisLevel($r->system_id, $level, $last);
            }
        }
        return $this->paisBin;
    }

    function dist_binario($pai, $pontos, $nivel = 6, $usuarioQueNaoRecebe = '') {
        $config = new config();
        $config = $config->getConfig();
        $nivel = $config['limite_binario'];
        if (!is_numeric($nivel)) {
            $nivel = '';
        }
        
        $usr = new User();
        $paiInfo = $usr->userInfo($pai);
        //$usr->addPontos($pai, $pontos, "Pontos Binários", $usr->getUserDirection($pai));

        $filhos = $this->getFilhos($pai, 0, $nivel);
        $superiores = $this->getPais($pai, 0, $nivel);
        $i = 1;
        foreach ($superiores as $key) {
            $paidId = $key;

            $paidId = $usr->userInfo($paidId);

            $id[0] = $pai;
            $dataRef = Referrals::where('user_id', $paidId['id'])->first();
            $id[$i] = $key;
            $index = $i - 1;
            $pagador = $id[$index];
            $beneficiado = $id[$i];
            if ($paidId['ativo'] == 1 && $beneficiado <> $usuarioQueNaoRecebe) {               
                $direcaoPagador = $usr->getUserDirection($pagador);                
                $usr->addPontos($beneficiado, $pontos, "Pontos Binários", $direcaoPagador);
                $usr->addPontosTotal($beneficiado, $pontos, "Pontos Binários", $direcaoPagador);
            }
            $i++;
        }

        return 1;
        /* foreach ($filhos as $key) {
          $filhoId = $key;
          $filhoInfo = $usr->userInfo($filhoId);
          if ($filhoInfo['ativo'] == 1) {
          if ($filhoInfo['id'] <> 1) {
          $dataRef = Referrals::where('user_id', $filhoInfo['id'])->first();

          $dir = $usr->addPontos($dataRef->system_id, $pontos, "Pontos Binários", $dataRef->direcao);
          }
          }
          } */
    }

}
