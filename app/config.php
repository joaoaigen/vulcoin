<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class config extends Model
{
        
    protected $connection= 'mysql';

    protected $table = 'configs';
    
    public function getConfig(){
       return $this->where('id',1)->first();
        
    }
}
