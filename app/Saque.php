<?php

namespace App;
Use Str;
use Illuminate\Database\Eloquent\Model;

class Saque extends Model {

    public $timestamps = true;
    
    protected $connection= 'mysql';

    protected $table = 'saques';

    protected $fillable = ['valor', 'status', 'user_id', 'data_deposito', 'conta', 'mensagem'];

    public function username($id) {
        $dat = User::where('id', $id)->first();
        return strlen(Str::words($dat['username'], 1, '')) <= 5 ? Str::words($dat['username'], 2, '') : Str::words($dat['username'], 1, '');
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

}
