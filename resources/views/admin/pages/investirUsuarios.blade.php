@extends('layouts.admin')
@section('title-head')
Investimentos usuários
@endsection
@section('title-body')
Investimentos usuários
@endsection
@section('page-css')
<link rel="stylesheet" href="{{ url('https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.css') }}">
@endsection
@section('main-content')
<!-- Main content -->
<section class="content">
    <div class="row">            
        <div class="col-12 col-md-12">
            <div class="box box-solid bg-dark">
                <div class="box-header with-border">
                    <h3 class="box-title">Investimentos</h3>
                    <h6 class="box-subtitle">Investimentos de todos os usuários</h6>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table id="extratos" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Usuario</th>
                                    <th>Preço</th>
                                    <th>Vulcoins</th>
                                    <th>Hash transação</th>
                                    <th>Opções</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($investimentos as $row)
                                <tr>
                                    <td>{{ $row->id }}</td>
                                    <td>{{ $row->username }}</td>
                                    <td>{{ $row->ico_price }}</td>
                                    <td>{{ $row->qntd_vulcoins }}</td>
                                    <td>{{ $row->hash_transacao }}</td>
                                    <td>
                                        @if($row->status === 1)
                                        <a class="btn btn-primary btn-xs" href="{{ url('admin/investimentos/ativo/'. $row->id) }}"> Ativo</a>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>                    
            </div>  
        </div>
    </div>        
</section>
<!-- /.content -->
@endsection
@section('page-js')
<!-- This is data table -->
<script src="{{ asset('../assets/assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.flash.min.js') }}"></script>
<script src="{{ asset('../assets/assets/vendor_plugins/DataTables-1.10.15/ex-js/jszip.min.js') }}"></script>
<script src="{{ asset('../assets/assets/vendor_plugins/DataTables-1.10.15/ex-js/pdfmake.min.js') }}"></script>
<script src="{{ asset('../assets/assets/vendor_plugins/DataTables-1.10.15/ex-js/vfs_fonts.js') }}"></script>
<script src="{{ asset('../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('../assets/js/echarts.min.js') }}"></script>
<script type="text/javascript">
$('#extratos').DataTable({
    dom: 'Bfrtip',
    buttons: [
        'copy', 'csv', 'excel', 'pdf', 'print'
    ],
    "order": [[0, "desc"]]
});
</script> 
@endsection