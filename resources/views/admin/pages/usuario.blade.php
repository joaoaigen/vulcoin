@extends('layouts.admin')
@section('title-head')
    Gerenciar Usuários
@endsection
@section('title-body')
    Gerenciar Usuários   
@endsection
@section('page-css')
    <link rel="stylesheet" href="{{ url('https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.css') }}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
@endsection
@section('main-content')
<?php

function isValidMd5($md5 = '') {
    return preg_match('/^[a-f0-9]{32}$/', $md5);
}
?>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-12 col-md-12">
                <div class="box box-solid bg-dark">
                    <div class="box-header with-border">
                        <h3 class="box-title">Usuários</h3>
                        <h6 class="box-subtitle">Lista de usuários do sistema</h6>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table id="extratos" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Nome</th>
                                    <th>Username</th>
                                    <th>Valor Investido</th>
                                    <th>Patrocinador</th>


                                    <th>Saldo</th>
                                    <th>Email</th>
                                    <th>Status</th>


                                    <th>Ações</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php

                                    function getStatus($id) {
                                        switch ($id) {
                                            case 1:
                                                $res = 'Ativo';
                                                break;
                                            default:
                                                $res = 'Inativo';
                                        }
                                        return $res;
                                    }

                                    $pacoteAll = App\Pacote::where('status', 1)->get();
                                    ?>

                                    @foreach($usuarios as $usuario)
                                    <?php
                                    $pacoteData = App\Pacote::where('id', $usuario->pacote)->first();
                                    ?>
                                    @if(isset($_GET['atraso6Meses']))
                                    @if($usuario->atraso6meses())

                                    <tr>
                                        <td>{{$usuario->id}}</td>
                                        <td>{{$usuario->name}}</td>
                                        <td>{{$usuario->username}}</td>
                                        <td>{{$usuario->valorinvestido}}</td>
                                        <td>{{$usuario->getUserPai()->username}}</td>



                                        <td>{{$usuario->saldo}}</td>
                                        <td>{{$usuario->email}}</td>
                                        <td class="text-center">{{getStatus($usuario->ativo)}}</td>

                                        <td>
                                            <a href="{{url('/admin/usuario/'.$usuario->id)}}" class="editarUsr btn btn-success btn-xs">Editar Usuário</a>
                                        </td>
                                    </tr>

                                    @endif
                                    @elseif(isset($_GET['qualificadoCachout']))
                                    @if($usuario->qualificadoPowerBack())

                                    <tr>
                                        <td>{{$usuario->id}}</td>
                                        <td>{{$usuario->name}}</td>
                                        <td>{{$usuario->username}}</td>
                                        <td>{{$usuario->valorinvestido}}</td>
                                        <td>{{$usuario->getUserPai()->username}}</td>

                                        <td>{{$usuario->saldo}}</td>
                                        <td>{{$usuario->email}}</td>
                                        <td class="text-center">{{getStatus($usuario->ativo)}}</td>

                                        <td>
                                            <a href="{{url('/admin/usuario/'.$usuario->id)}}" class="editarUsr btn btn-success btn-xs">Editar Usuário</a>
                                        </td>
                                    </tr>

                                    @endif
                                    @else

                                    <tr>
                                        <td>{{$usuario->id}}</td>
                                        <td>{{$usuario->name}}</td>
                                        <td>{{$usuario->username}}</td>
                                        <td>{{$usuario->valorinvestido}}</td>
                                        <td>{{$usuario->getUserPai()->username}}</td>



                                        <td>{{$usuario->saldo}}</td>
                                        <td>{{$usuario->email}}</td>
                                        <td class="text-center">{{getStatus($usuario->ativo)}}</td>

                                        <td>
                                            <a href="{{url('/admin/usuario/'.$usuario->id)}}" class="editarUsr btn btn-success btn-xs">Editar Usuário</a>
                                        </td>
                                    </tr>

                                    @endif

                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>                    
                </div>  
            </div>
        </div>        
    </section>

    <div id='ativarUsr' class="modal" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <p>
                <form method="post" action={{route('ativar_user')}}>
                    {{ csrf_field() }}
                    {{ method_field('POST') }}
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title"> Ativar usuário</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Username</label>
                            <select name="username" id="username" required class="form-control" style="width: 100%">
                                <?php
                                $usuarios = \App\User::where('ativo', '=', 0)->where('carteira', '<>', '')->get();
                                ?>
                                @foreach($usuarios as $row)
                                <option value="{{$row->username}}"> {{ $row->username }} </option>
                                @endforeach
                            </select>
                            <!-- <input placeholder="Digite o nome" type="text"> -->
                        </div>
                        <div class="form-group">
                            <label>Quantidade de Vulcoins</label>
                            <input class="form-control" name="valor" id="valor">
                        </div>                            
                    </div>

                    <div class="modal-footer">
                        <button type="submit" id="ativar" class="btn btn-primary">Ativar Usuário</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>

    <!-- /.content -->
@endsection
@section('page-js')

    <!-- This is data table -->
    <script src="{{ asset('../assets/assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('../assets/assets/vendor_plugins/DataTables-1.10.15/ex-js/jszip.min.js') }}"></script>
    <script src="{{ asset('../assets/assets/vendor_plugins/DataTables-1.10.15/ex-js/pdfmake.min.js') }}"></script>
    <script src="{{ asset('../assets/assets/vendor_plugins/DataTables-1.10.15/ex-js/vfs_fonts.js') }}"></script>
    <script src="{{ asset('../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('../assets/js/echarts.min.js') }}"></script>
    <script type="text/javascript">
    $('#extratos').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        "order": [[ 0, "desc" ]]
    });
    </script> 
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="{{env('CFURL').('/js/jquery.maskMoney.min.js')}}"></script>

<script>
    $(document).ready(function() {
        $('#valor').maskMoney({
            prefix: '',
            allowNegative: false,
            thousands: '',
            decimal: '.',
            affixesStay: false
        });
        $('#username').select2();
        @if(session('status') == 200)
        swal("Sucesso!", '{{session('
            msg ')}}', 'success');
        @endif
        @if(session('status') == 400)
        swal("Erro!", '{{session('
            msg ')}}', "error");
        @endif
    });

    function fBlockUi() {
        $.blockUI({
            message: "<h4>Por favor aguarde...</h4>",
            css: {
                border: 'none',
                padding: '5px',
                backgroundColor: '#000',
                '-webkit-border-radius': '5px',
                '-moz-border-radius': '5px',
                opacity: .5,
                color: '#fff'
            }
        });
    }
</script>
@endsection