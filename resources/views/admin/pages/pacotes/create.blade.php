<form class="formAjax" method="post" action="{{url('/admin/pacote')}}">

    {{ csrf_field() }}

    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Adicionar Pacote</h4>

    </div>
    <div class="modal-body">
        <div class="form-group">
            <label>Nome</label>
            <input type="text" class="form-control" required placeholder="Nome" name="nome"/>
        </div>
        <div class="form-group">
            <label>Teto diário</label>
            <input  type="number" step="any" class="form-control" required placeholder="Teto diário" name="teto_diario" id='teto_diario' />
        </div>
        <div class="form-group">
            <label>Valor</label>
            <input type="number" step="any" class="form-control" required placeholder="Valor" name="valor" id='valor'/>
        </div>
        <div class="form-group">
            <label>Descrição </label>
            <textarea class="form-control" required placeholder="Descrição" name="descricao">
                
            </textarea>
        </div>
        <div class="form-group">
            <label>Produtos</label>
            <input type="text" class="form-control" step="any" required placeholder=" Ex: 23,33,45" name="produtos"/>
            <p>Separe os ids do pacote da forma:23,33,45 </p>
        </div>

        <div class="form-group">
            <label>Status </label>
            <select class="form-control" name="status">
                <option value="1">Ativo</option>
                <option value="0">Inativo</option>
            </select>
        </div>

        <div class="form-group">
            <label>Valor do ponto binário</label>
            <input type="number" class="form-control" step="any" required placeholder="Ex: 0.50" name="valor_ponto_binario"/>

        </div>
        <div class="form-group">
            <label>Binário </label>
            <select class="form-control" name="binario">
                <option value="1">Ativo</option>
                <option value="0">Inativo</option>
            </select>
        </div>
        <div class="form-group">
            <label>Pontos Binários</label>
            <input type="number" class="form-control" step="any" required placeholder="Pontos Binários" name="valor_binario"/>

        </div>


        <div class="form-group" >
            <label>Valor da indicação</label>
            <input type="number" class="form-control" step="any" required placeholder="Valor da indicação" value="0" name="indicacao_direta"/>
        </div>


        <?php
        for ($i = 1; $i <= 7; $i++) {
            echo '<div class = "form-group">';
            echo"<label>{$i}º Nível</label>";
            echo "<input  type='number' step='any' class = 'form-control money' value='0'  placeholder = '{$i}º Nível' name ='nivel{$i}'/>
            </div>";
        }
        ?>



    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
        <button type="submit" class="btn btn-primary">Adicionar</button>
    </div>
</form>


