@extends('layouts.app')

@section('htmlheader_title')
Pedidos
@endsection

@section('page_css')
<!-- DataTables -->
<link rel="stylesheet" href="{{ env('CFURL').('/plugins/datatables/dataTables.bootstrap.css')}}">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.2.1/css/buttons.dataTables.min.css">

@endsection

@section('contentheader_title')
Todos os pedidos
@endsection

@section('contentheader_description')

@endsection

@section('main-content')
<!-- Small boxes (Stat box) -->
<div class="row">

</div><!-- /.row -->

<div class="row">

    <section class="col-md-12">

        <div class="box">
            <div class="box-body">

                <div id="mensagemAdicionarVouchers">

                </div>

                <table id="ord-carrinho" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Dados do usuário</th>
                            <th>Data</th>
                            <th>Valor</th>
                            <th>Status</th>
                            <th>Mensagem</th>
                                                        <th>Produtos</th>

                            <th>Ação</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $usr = new App\User();
                        if (isset($_GET['status'])) {
                            $pedidos = App\Pedidos::where('status', $_GET['status'])->get();
                        } else {
                            $pedidos = App\Pedidos::all();
                        }
                        ?>
                        @foreach($pedidos as $pedido)
                        <?php
                        $userInfo = $usr->userInfo($pedido['user_id']);
                        $endereco = App\Enderecos::where('id', $pedido['id_endereco'])->first();
                        ?>
                        <tr>
                            <td>{{$pedido['id']}}</td>

                            <td>Nome: {{$userInfo['name']}}<br>Email:{{$userInfo['email']}}<br>Telefone:{{$userInfo['telefone']}}<br>
                                <br>Endereço de entrega: {{$endereco['pais']}},{{$endereco['estado']}},{{$endereco['cidade']}},{{$endereco['endereco']}}-{{$endereco['cep']}}<br>

                            <td>{{Carbon\Carbon::parse($pedido['date'])->format('d/m/Y')}}</td>
                            <td>{{number_format($pedido['preco'], 2,'.', '')}}</td>
                            <td>{{$pedido['status']}}</td>
                            <td><?= $pedido['info'] ?></td>
                            <td>
                                <?php
$produtos = json_decode($pedido['produtos']);

                                ?>
                                                            
                                                            @foreach($produtos as $produto)
                                                           iD: {{$produto->id}} Nome:{{$produto->nome}} Quantidade:{{$produto->quantidade}}
                                                           <br>


@endforeach



                            </td>

                            <td><a href = "{!! url('admin/pedido/edit/'.$pedido['id']) !!}" class = "btn btn-info">Editar</a><a href="{!! url('admin/meus-pedidos/pedido/'.$pedido['id']) !!}" class="btn btn-info">Detalhes</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

            </div><!--/.box-body -->
        </div><!--/.box -->

    </section>

</div>

@endsection

@section('page_scripts')
<!-- DataTables -->
<script src="{{ env('CFURL').('/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="https://cdn.datatables.net/buttons/1.2.1/js/dataTables.buttons.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.2.1/js/buttons.flash.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="//cdn.datatables.net/buttons/1.2.1/js/buttons.html5.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.2.1/js/buttons.print.min.js"></script>

<script src="{{ env('CFURL').('/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script>
    $(function () {

    $('#ord-carrinho').DataTable({
    "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true,
            "oLanguage": {"sZeroRecords": "Você ainda não realizou nenhum pedido",
                    "sEmptyTable": "Você ainda não realizou nenhum pedido"},
            "order": [[0, "desc"]],
            dom: 'Bfrtip',
            buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
            ]
    });
    });
</script>
@endsection
