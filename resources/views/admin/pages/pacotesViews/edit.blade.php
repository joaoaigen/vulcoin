<form class="formAjax" method="post" action="{{url('/admin/pacote_visualizacoes/salvar')}}">

    {{ csrf_field() }}
    <?php

    function getStatus($status, $nm = '') {
        if ($nm <> '') {
            if ($status == 1) {
                return 'Sim';
            } else {
                return 'Não';
            }
        } else {
            if ($status == 1) {
                return 'Ativo';
            } else {
                return 'Inativo';
            }
        }
    }
    ?>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Adicionar Pacote</h4>

    </div>
    <div class="modal-body">
        <div class="form-group">
            <label>Nome</label>
            <input type="text" class="form-control" required placeholder="Nome" value="<?= $dados['nome'] ?>" name="nome"/>
        </div>
        <input type="hidden" class="form-control"  name="id" value="<?= $dados['id'] ?>"/>

        <div class="form-group">
            <label>Status </label>
            <select class="form-control" name="status">
                <option selected="" value="<?= $dados['status'] ?>"><?= getStatus($dados['status']) ?></option>
                <option value="1">Ativo</option>
                <option value="0">Inativo</option>
            </select>
        </div>
        <div class="form-group">
            <label>Quantidade de visualizações</label>
            <input type="number"  class="form-control" required placeholder="Visualizações" value="<?= $dados['views'] ?>" name="views" id='views'/>
        </div>
        <div class="form-group">
            <label>Valor</label>
            <input type="number" step="any" class="form-control" required placeholder="Valor" value="<?= $dados['valor'] ?>" name="valor" id='valor'/>
        </div>

    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
        <button type="submit" class="btn btn-primary">Salvar</button>
    </div>
</form>


