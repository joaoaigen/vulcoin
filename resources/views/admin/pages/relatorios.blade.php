@extends('layouts.admin')
@section('title-head')
    @lang('messages.dashboard')
@endsection
@section('title-body')
    @lang('messages.dashboard')   
@endsection
@section('page-css')
    <link rel="stylesheet" href="{{ url('https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.css') }}">
@endsection
@section('main-content')
@inject('extratos', 'App\extratos')

<?php
if (!isset($_GET['de']) or !isset($_GET['ate'])) {
    $extract = $extratos->where('beneficiado', Auth::user()->id)->where('descricao', '<>', 'Pontos Binários');
    $ganho = $extract->where('valor', '>', 0)->sum('valor');
    $saida = $extract->where('valor', '<', 0)->sum('valor');
    $extract = $extratos->where('beneficiado', Auth::user()->id)->where('descricao', '<>', 'Pontos Binários');

    $allExtratos = $extract->get();
} else {
    ///lucro 
    $extract = $extratos->where('beneficiado', Auth::user()->id)->where('descricao', '<>', 'Pontos Binários')->where('data', '>=', $_GET['de'])->where('data', '<=', $_GET['ate']);
    $ganho = $extract->where('valor', '>', 0)->sum('valor');
    $saida = $extract->where('valor', '<', 0)->sum('valor');
    $extract = $extratos->where('beneficiado', Auth::user()->id)->where('descricao', '<>', 'Pontos Binários')->where('data', '>=', $_GET['de'])->where('data', '<=', $_GET['ate']);

    $allExtratos = $extract->get();
}
$total = $ganho - $saida;
?>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-3">
                <div class="info-box">
                    <span class="info-box-icon bg-orange"><i class="fa fa-users"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Total de Usuários</span>
                        <span class="info-box-number"><?= \App\User::all()->count() ?></span>

                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-3">
                <div class="info-box">
                    <span class="info-box-icon bg-orange"><i class="fa fa-dollar"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Entrada </span>
                        <span class="info-box-number">$<?= number_format($ganho, 2, ',', '.') ?></span>
                    </div>
                </div>
            </div>
            <?php
            $curl = curl_init();
            curl_setopt_array($curl, [
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => "http://157.245.141.235/api/getbalanceapi"
            ]);

            $result = json_decode(curl_exec($curl));
            curl_close($curl);
            ?>
            <div class="col-md-3 col-sm-3 col-xs-3">
                <div class="info-box">
                    <span class="info-box-icon bg-red"><i class="fa fa-viacoin"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Qntd. VLC </span>
                        <span class="info-box-number"><?= number_format(isset($result->quantidade) ? $result->quantidade : 0, 2, ',', '.') ?></span>
                    </div>
                </div>
            </div>
            
            <div class="col-md-3 col-sm-3 col-xs-3">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="fa fa-dollar"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Total </span>
                        <span class="info-box-number">$<?= number_format($total, 2, ',', '.') ?></span>
                    </div>
                </div>
            </div>
            
             <div class="col-12 col-md-12">
                <div class="box box-solid bg-dark">
                    <div class="box-header with-border">
                        <h3 class="box-title">Extrato financeiro</h3>
                        <h6 class="box-subtitle">Listagem de todas as movimentações do usuário: {{ Auth::user()->username }}</h6>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table id="extratos" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
                                <thead>
                                <tr>
                                    <th class="sorting_desc">Id</th>
                                    <th>Usuario</th>
                                    <th>Descrição</th>
                                    <th>Data</th>
                                    <th>Valor</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($allExtratos as $extrato)
                                    <tr>
                                        <td>{{$extrato['id']}}</td>
                                        <td>@if($extrato['descricao']=='Pagamento de binário')
                                                {{'Administração'}}
                                            @endif
                                            @if($extrato['descricao']!='Pagamento de binário')
                                                {{$extrato->userName($extrato['user_id'])}}
                                            @endif
                                        </td>
                                        <td><?= $extrato['descricao']?></td>
                                        <td>{{ date( 'd/m/Y' , strtotime($extrato['data']))}}</td>

                                        <td style="color: {{ substr($extrato['valor'], 0, 1) == '-' ? 'red' : 'green' }};">{{ strpos($extrato['valor'], '-') ? '$ ' . number_format(str_replace('-', '', $extrato['valor']), 2, '.', '') : '$ ' . number_format(str_replace('-', '', $extrato['valor']), 2, '.', '') }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>                    
                </div>  
            </div>
        </div>        
    </section>


    <!-- /.content -->
@endsection
@section('page-js')

    <!-- This is data table -->
    <script src="{{ asset('../assets/assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('../assets/assets/vendor_plugins/DataTables-1.10.15/ex-js/jszip.min.js') }}"></script>
    <script src="{{ asset('../assets/assets/vendor_plugins/DataTables-1.10.15/ex-js/pdfmake.min.js') }}"></script>
    <script src="{{ asset('../assets/assets/vendor_plugins/DataTables-1.10.15/ex-js/vfs_fonts.js') }}"></script>
    <script src="{{ asset('../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('../assets/js/echarts.min.js') }}"></script>
    <script type="text/javascript">
    $('#extratos').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        "order": [[ 0, "desc" ]]
    });
    </script> 
@endsection