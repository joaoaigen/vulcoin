<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8"> <!-- utf-8 works for most cases -->
            <meta name="viewport" content="width=device-width"> <!-- Forcing initial-scale shouldn't be necessary -->
                <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- Use the latest (edge) version of IE rendering engine -->
                    <title></title> 
                    <link href='<?= url('css/email.css') ?>' rel='stylesheet' type='text/css'>
                        <style>
                        </style>
                        </head>
                        <body bgcolor="#d2d6de" width="100%" style="margin: 0;">
                            <center style="width: 100%; background: #d2d6de;">


                                <!-- Visually Hidden Preheader Text : END -->

                                <!-- Email Header : BEGIN -->
                                <table cellspacing="0" cellpadding="0" border="0" align="center" width="600" style="margin: auto;" class="email-container" role="presentation">
                                    <tr>
                                        <td style="padding: 20px 0; text-align: center">
                                            <img src="{{url('img/logo-escritorio.png')}}"  alt="" border="0">
                                        </td>
                                    </tr>
                                </table>
                                <!-- Email Header : END -->

                                <!-- Email Body : BEGIN -->
                                <table   cellspacing="0" cellpadding="0" border="0" align="center" width="600" style="margin: auto;" class="email-container" role="presentation">

                                    <!-- Hero Image, Flush : BEGIN -->
                                    <tr>

                                        <td  bgcolor="#3c8dbc">
                                            <div style="background-color: #3c8dbc;  ">
                                                <center><h1 style="color:#fff;"><?= $subject ?></h1></center>
                                            </div>
                                        </td>
                                    </tr>
                                    <!-- Hero Image, Flush : END -->

                                    <!-- 1 Column Text : BEGIN -->
                                    <tr>
                                        <td bgcolor="#ffffff" style="padding: 40px; text-align: center; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;">
                                            <?= $content ?>
                                            <br><br>

                                                    <!-- Button : END -->
                                                    </td>
                                                    </tr>
                                                    <!-- 1 Column Text : BEGIN -->


                                                    <!-- Background Image with Text : END -->



                                                    <!-- Thumbnail Left, Text Right : END -->


                                                    <!-- 1 Column Text + Button : BEGIN -->

                                                    </table>
                                                    <!-- Email Body : END -->

                                                    <!-- Email Footer : BEGIN -->
                                                    <table cellspacing="0" cellpadding="0" border="0" align="center" width="600" style="margin: auto;" class="email-container" role="presentation">
                                                        <tr>
                                                            <td style="padding: 40px 10px;width: 100%;font-size: 12px; font-family: sans-serif; mso-height-rule: exactly; line-height:18px; text-align: center; color: #888888;">
                                                              TBS SAÙDE<br>
                                                                        <br><br> 
                                                                                <a href="<?= url('/') ?>">Ver site</a>
                                                                                </td>
                                                                                </tr>
                                                                                </table>
                                                                                <!-- Email Footer : END -->

                                                                                </center>
                                                                                </body>
                                                                                </html>