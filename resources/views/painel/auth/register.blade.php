<?php ?>
@extends('layouts.auth')

@section('htmlheader_title')
<?= Lang::trans('Register') ?>
@endsection

@section('content')
<style>
    @media (min-width:320px) { 

    }
    @media (min-width:480px) {

    }
    @media (min-width:600px) { 


    }
    @media (min-width:801px) { 
        .register-box{ width: 650px !important;}

    }
    @media (min-width:1200px) { 
        .register-box{ width: 650px !important;}
    }
</style>
<body class="hold-transition register-page" style="background-image:url('{{url('img/2.jpg')}}');background-size: cover;">
    <div class="register-box" >
        <div class="register-logo">
            <a href="{{ url('/') }}">
                <img src="{{url('img/logo-escritorio.png')}}">
            </a>
        </div>

        @if (isset($errors) && count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong><br>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        
            <div class="register-box-body">
                <?php if (isset($indicador->name)) { ?>
                    <p class="login-box-msg">Você foi indicado por: <b>{{$indicador->name. ' - '. $indicador->email}}</b></p>
                    <form action="{{ url('/cadastro/') }}" method="post" id="formcadastro">
                    <?php } ?>
                    {{ csrf_field() }}
                    {{ method_field('POST') }}
                    <div class="col-sm-12">
                    
	                <input type="hidden" value="<?= @$indicador->username ?>" name="indicador" />
	                <input type="hidden" name="sexo" value="Masculino"/>

                        <div class="form-group has-feedback">
                            <input type="text" class="form-control" id="usuario" required placeholder="LOGIN DESEJADO" maxlength="20" name="username" pattern="[a-zA-Z0-9]+" value="{{old('username')}}"/>
                        </div>
                        
                        <div class="form-group has-feedback">
                            <input type="text" class="form-control" required placeholder="Nome Completo" id="name" name="name" value="{{old('name')}}"/>
                        </div>

                        <div class="form-group has-feedback">
                            <input type="email" class="form-control" required placeholder="Email" name="email" value="{{old('email')}}"/>
                        </div>

                        <div class="form-group has-feedback">
                            <input type="password" class="form-control" required placeholder="Senha" name="password" value=""/>
                        </div>

                        <div class="form-group has-feedback">
                            <input type="password" class="form-control" required placeholder="Repita a senha" name="password_confirmation" value=""/>
                        </div>
                        
                        <div class="form-group has-feedback">
                            <input type="text" class="form-control" required placeholder="Telefone" name="telefone" value="{{old('telefone')}}"/>
                        </div>
                        
                        <div class="form-group has-feedback">
                            <a target="_blank" href="{{ asset('Contrato.pdf') }}">Clique aqui para ler os termos de uso.</a>
                        </div>                        
                        
                        <div class="form-group has-feedback">
                            <select name="" id="termos" class="form-control">
                                <option value="0">Não aceito os termos</option>
                                <option value="1">Aceito os temos de uso</option>
                            </select>
                        </div>
                        
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <button type="submit" class="btn btn-primary btn-block btn-flat" disabled="" required id="btEnviar">Enviar</button>
                        </div><!-- /.col -->
                    </div>

                </form>
            </div><!-- /.form-box -->


    </div><!-- /.register-box -->

</div>

@include('layouts.partials.scripts_auth')
<?php /*if (\App\config::getConf()['recaptcha_status'] == 'sim') { ?>
    <script src ='https://www.google.com/recaptcha/api.js' ></script>

<?php }*/ ?>
<div id="modalLoginSel" class="modal fade" tabindex="-1" role="dialog"  data-backdrop="static" 
     data-keyboard="false" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Insira o login do seu indicador</h4>
            </div>
            <!-- -->
            @if (isset($errors) && count($errors) > 0  )
            <div class="alert alert-danger">
                <strong>Whoops!</strong><br>
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <form onsubmit="url = '<?php echo url('cadastro'); ?>' + '/' + $('#loginPtrocinador').val();
                                location.href = url;
                                return false;">
                <div class="modal-body">
                    <input type="text" id="loginPtrocinador" class="form-control" placeholder="login do seu indicador" required="" pattern="[a-zA-Z0-9]+" min="4">
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Ok!</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
@include('layouts.partials.scripts_auth')

<script>

    // Função para habilitar botão enviar ao clicar no checkbox aceito os termos
    $("#termos").click(function () {
        if ($(this).val() == 1) {
            $("#btEnviar").prop('disabled', false);
        } else {
            $("#btEnviar").prop('disabled', true);
        }
    });

    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });

    });
</script>
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-72220910-1', 'auto');
    ga('send', 'pageview');

</script>
<script src="<?= env('CFURL') ?>/dist/js/jcombo.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/i18n/pt-BR.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
                    
                    @if(session('status') == 200)
                        swal("Sucesso!", '{{session('msg')}}', 'success');        
                    @endif
                    @if(session('status') == 400)
                        swal("Erro!", '{{session('msg')}}', "error");
                    @endif


    })
<?php if (!isset($indicador->name)) { ?>
        $("#modalLoginSel").modal();

<?php } ?>

</script>

</body>

@endsection
