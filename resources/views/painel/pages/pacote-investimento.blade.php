@extends('layouts.app')
@section('title-head')
    Planos de investimento
@endsection
@section('title-body')
    Planos de investimento   
@endsection
@section('page-css')

@endsection
@section('main-content')
    <?php 
        $arr = array('info', 'warning', 'danger', 'success');
        $valor_pacote = DB::table('plano_investimento')->where('usuario', '=', Auth::user()->id)->first();
        
    ?>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-lg-12 col-12">
                
            </div>
            @foreach($dados as $row)
            @if(isset($valor_pacote->valor_pacote) && $valor_pacote->porcentagem_atual == $valor_pacote->porcentagem_maxima)            
            <div class="col-lg-3 col-12">
                <div class="box text-center p-50 box-inverse bg-{{ $arr[shuffle($arr)] }} bg-hexagons-dark pull-up">
                    <div class="box-body">
                        <h5 class="text-uppercase">{{ $row->nome }}</h5>
                        <br>
                        <h3 class="font-weight-100 font-size-30">${{ number_format($row->valor, 2) }}</h3>                        

                        <hr>

                        <p><strong>{{ $row->porcentagem_maxima }} %</strong> Rendimento</p>
                        <!--<p><strong>{{ $row->qntd_dias }}</strong> Dias</p>-->
                        <p><strong>{{ number_format($row->valor / str_replace(',', '.', \App\Ico::all()->last()->price), 2, '.', '') }} VLC</strong> Quantidade de vulcoins</p>
                        <br><br>
                        <a class="btn btn-bold btn-block btn-outline btn-light" href="{{ url('programa-investimentos/?id='.$row->id.'&username='.auth()->user()->username) }}">Selecionar Plano</a>
                    </div>
                </div>
            </div>  
            @elseif(isset($valor_pacote->valor_pacote) && $row->valor > $valor_pacote->valor_pacote)
            <div class="col-lg-3 col-12">
                <div class="box text-center p-50 box-inverse bg-{{ $arr[shuffle($arr)] }} bg-hexagons-dark pull-up">
                    <div class="box-body">
                        <h5 class="text-uppercase">{{ $row->nome }}</h5>
                        <br>
                        <h3 class="font-weight-100 font-size-30">${{ number_format($row->valor, 2) }}</h3>                        

                        <hr>

                        <p><strong>{{ $row->porcentagem_maxima }} %</strong> Rendimento</p>
                        <!--<p><strong>{{ $row->qntd_dias }}</strong> Dias</p>-->
                        <p><strong>{{ number_format($row->valor / str_replace(',', '.', \App\Ico::all()->last()->price), 2, '.', '') }} VLC</strong> Quantidade de vulcoins</p>
                        <br><br>
                        <a class="btn btn-bold btn-block btn-outline btn-light" href="{{ url('programa-investimentos/?id='.$row->id.'&username='.auth()->user()->username) }}">Selecionar Plano</a>
                    </div>
                </div>
            </div>
            @endif            
            @if(!isset($valor_pacote->valor_pacote))            
            <div class="col-lg-3 col-12">
                <div class="box text-center p-50 box-inverse bg-{{ $arr[shuffle($arr)] }} bg-hexagons-dark pull-up">
                    <div class="box-body">
                        <h5 class="text-uppercase">{{ $row->nome }}</h5>
                        <br>
                        <h3 class="font-weight-100 font-size-30">${{ number_format($row->valor, 2) }}</h3>                        

                        <hr>

                        <p><strong>{{ $row->porcentagem_maxima }} %</strong> Rendimento</p>
                        <!--<p><strong>{{ $row->qntd_dias }}</strong> Dias</p>-->
                        <p><strong>{{ number_format($row->valor / str_replace(',', '.', \App\Ico::all()->last()->price), 2, '.', '') }} VLC</strong> Quantidade de vulcoins</p>
                        <br><br>
                        <a class="btn btn-bold btn-block btn-outline btn-light" href="{{ url('programa-investimentos/?id='.$row->id.'&username='.auth()->user()->username) }}">Selecionar Plano</a>
                    </div>
                </div>
            </div>
            @endif
            @endforeach
        </div>
    </section>
    <!-- /.content -->
@endsection
@section('page-js')

    <!-- This is data table -->
    <script src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js') }}"></script>

    <!-- start - This is for export functionality only -->
    <script src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('../../assets/vendor_plugins/DataTables-1.10.15/ex-js/jszip.min.js') }}"></script>
    <script src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/ex-js/pdfmake.min.js') }}"></script>
    <script src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/ex-js/vfs_fonts.js') }}"></script>
    <script src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.print.min.js') }}"></script>
    <!-- end - This is for export functionality only -->

    <!-- Crypto_Admin for Data Table -->
    <script src="{{ asset('../../assets/js/pages/data-table.js') }}"></script>
    
    <script type="text/javascript">
        $( document ).ready(function() {
            $(".change_dir").click(function() {
        var lado = $(this).val();
        $.ajax({
            'url': "muda_lado?lado=" + lado,
            dataType: 'html',
            'success': function(txt) {
                if (txt == 'ok') {
                    swal("Sucesso!", "O lado do binário foi alterado com sucesso!", "success");                    
                    // alert('O lado binário foi alterado com sucesso.');
                }
                if (txt == 'fail') {
                    swal("Erro!", "O lado não binário foi alterado com sucesso.", "error");
                }
            }
        });
    });
        });
    </script>
@endsection

