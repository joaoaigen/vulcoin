<!-- REQUIRED JS SCRIPTS -->
<!-- REQUIRED JS SCRIPTS -->
<div id='lucro' class="modal">
    <div class="modal-dialog">
        <div class="modal-content">

            <p>
            <form id="formLucro" method="post" action="{{url('/admin/saque')}}">


                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Dividir Lucro</h4>

                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <label>Valor </label>
                        <input class="form-control"  type="number" min="1" max="100" name="valorDist" id="valorDist" required>
                        <p>

                            <?php
                            $usrTotal = App\User::where('ativo', 1)->count();
                            $lucro = App\User::where('id', 1)->first();
                            $meuLucro = number_format($lucro['saldo'] * 0.7, 2, ',', '.');
                            $valorSeguro2 = $lucro['saldo'] / $usrTotal;
                            $valorSeguro = number_format($valorSeguro2, 2, ',', '.');
                            ?>
                            Total de usuários:<b>{{$usrTotal}}</b> Meu lucro Aproximado: <b>R${{$meuLucro}}</b>   </p>

                    </div>
                    <div class="modal-footer">
                        <div id="resUpdate">
                        </div>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        <button type="submit" id="distribuir" class="btn btn-primary">Distribuir</button>
                        </p>
                    </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<!-- jQuery 2.1.4 -->
<script src="{{env('CFURL').('/plugins/jQuery/jQuery-2.1.4.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
$.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.5 -->
<script src="{{env('CFURL').('/bootstrap/js/bootstrap.min.js')}}"></script>
<!-- Morris.js charts -->
<?php /*
  <script src="{{asset('https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js')}}"></script>
  <script src="{{asset('/plugins/morris/morris.min.js')}}"></script>
 */
?>
<!-- Sparkline -->
<script src="{{env('CFURL').('/plugins/sparkline/jquery.sparkline.min.js')}}"></script>
<!-- jvectormap -->
<script src="{{env('CFURL').('/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
<script src="{{env('CFURL').('/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
<!-- jQuery Knob Chart -->
<script src="{{env('CFURL').('/plugins/knob/jquery.knob.js')}}"></script>
<!-- PACE -->
<script src="{{env('CFURL').('/plugins/pace/pace.min.js')}}"></script>
<!-- daterangepicker -->
<script src="{{env('CFURL').('/plugins/moment.js/moment.min.js')}}"></script>
<script src="{{env('CFURL').('/plugins/daterangepicker/daterangepicker.js')}}"></script>
<!-- datepicker -->
<script src="{{env('CFURL').('/plugins/datepicker/bootstrap-datepicker.js')}}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{env('CFURL').('/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
<!-- Slimscroll -->
<script src="{{env('CFURL').('/plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{env('CFURL').('/plugins/fastclick/fastclick.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{env('CFURL').('/dist/js/app.min.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{env('CFURL').('/dist/js/pages/dashboard.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{env('CFURL').('/dist/js/demo.js')}}"></script>
<script src="{{env('CFURL').('/js/scripts-custom.js')}}"></script>


<script>
(function (i, s, o, g, r, a, m) {
    i['GoogleAnalyticsObject'] = r;
    i[r] = i[r] || function () {
        (i[r].q = i[r].q || []).push(arguments)
    }, i[r].l = 1 * new Date();
    a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
    a.async = 1;
    a.src = g;
    m.parentNode.insertBefore(a, m)
})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

ga('create', 'UA-72220910-1', 'auto');
ga('send', 'pageview');

</script>
<script>

    // To make Pace works on Ajax calls
    $(document).ajaxStart(function () {
        Pace.restart();
    });

    $('.ajax').click(function () {
        $.ajax({
            url: '#', success: function (result) {
                $('.ajax-content').html('<hr>Ajax Request Completed !');
            }
        });
    });
</script>


@yield('page_scripts')
<script src="http://plentz.github.io/jquery-maskmoney/javascripts/jquery.maskMoney.min.js"></script>
<script>
    function distribuirLucro(valor, senha) {
        alert('Por favor aguarde');
        $('#distribuir').html('Por favor aguarde...');
        $.ajax({
            'url': "divisaoLucro",
            "type": "POST",
            "dataType": 'html',
            "data": "valor=" + valor + "&_token=<?php echo csrf_token(); ?>",
            'success': function (txt) {
                $('#distribuir').html('Distribuir');
                alert(txt);
            }});
    }

    $("#formLucro").submit(function () {
        var valorDist = $("#valorDist").val();

        if (confirm("Deseja realmente realizar essa operação?")) {
            if (valorDist > 100) {
                if (confirm("O valor da transferência é maior que seu lucro,tem certeza que deseja fazer isso?")) {
                    distribuirLucro(valorDist);
                }
            } else {
                distribuirLucro(valorDist);
            }
        }
        return false;

    });

    $(document).ready(function () {
        $('#valorDist').maskMoney({prefix: 'R$ ', allowNegative: false, thousands: '.', decimal: ',', affixesStay: false});
    });
   $(document).ready(function () {
        $('#valorSaque').maskMoney({prefix: 'R$ ', allowNegative: false, thousands: '.', decimal: ',', affixesStay: false});
    });
</script>
</script>
