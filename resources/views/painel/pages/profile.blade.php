@extends('layouts.app')
@section('title-head')
    Meus Dados
@endsection
@section('title-body')
    Meus Dados
@endsection
@section('page-css')

@endsection
@section('main-content')
    <!-- Main content -->
    <section class="content">
       <div class="row">
           
       <div class="col-md-12 col-12">
             <div class="box box-solid bg-dark">
            <div class="box-header with-border">
              <h3 class="box-title">Imagem de perfil</h3>              
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-md-2">
                        <div class="form-group has-feedback">
                            <img onclick="" class="img-circle" src="{{Auth::user()->photo}}" width="128" data-container="body" alt="User Avatar" data-html='true'  title="Mudar Foto"  data-toggle="popover" data-content="">
                        </div>       
                    </div>
                    <form action="{{ url('painel/mudar_foto') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputFile">Upload de imagem</label>
                                <input type="file" id="exampleInputFile" name="image">

                                <p class="help-block text-red">Selecione uma imagem para fazer o upload no perfil.</p>
                            </div>
                        </div> 
                        
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-success">Upload</button>
                        </div>                        
                    </form>
                </div>                
            </div>
          </div>
        </div>  
           
        <section class="col-lg-12">

            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="false">Dados Pessoais</a></li>
                    <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">Dados de Saque</a></li>

                    <li class=""><a href="#tab_3" data-toggle="tab" aria-expanded="false">Senha de segurança</a></li>
                    <li class=""><a href="#tab_4" data-toggle="tab" aria-expanded="false">Senha de acesso</a></li>
                </ul>

                <form role="form" method="post" action="" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    {{ method_field('POST') }}
                    <div class="box-body">
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_1">                                                                
                                <div class="form-group has-feedback">
                                    <input type="text" class="form-control"  placeholder="Nome Completo" disabled="" name="name" value="{{old('name') ? old('name') : Auth::user()->name}}"/>
                                </div>

                                <div class="form-group has-feedback">
                                    <input type="email" class="form-control" readonly  placeholder="Email" name="email" value="{{ old('email') ? old('email') : Auth::user()->email}}"/>
                                </div>

                                <div class="form-group has-feedback">
                                    <label>Direção da Rede:</label>
                                    <select name="direcao" class="form-control">
                                        <option {{ (old('direcao') == 'esquerda' OR Auth::user()->direcao == 'esquerda') ? 'selected' : '' }} value="esquerda">
                                            Esquerda
                                        </option>
                                        <option {{ (old('direcao') == 'direita' OR Auth::user()->direcao == 'direita') ? 'selected' : '' }} value="direita">
                                            Direita
                                        </option>
                                    </select>
                                </div>



                                <div class="form-group has-feedback">
                                    <label>Sexo:</label>
                                    <select name="sexo" class="form-control">
                                        <option {{ (old('sexo') == 'Masculino' OR Auth::user()->sexo == 'Masculino') ? 'selected' : '' }} value="Masculino">
                                            Masculino
                                        </option>
                                        <option {{ (old('sexo') == 'Feminino' OR Auth::user()->sexo == 'Feminino') ? 'selected' : '' }} value="Feminino">
                                            Feminino
                                        </option>
                                    </select>
                                </div>
                                <div class="form-group has-feedback">
                                    <input type="text" class="form-control"  placeholder="Nome do segundo titular" name="segtitular_nm" value="{{old('segtitular_nm') ? old('segtitular_nm') : Auth::user()->segtitular_nm}}"/>
                                </div>
                                <div class="form-group has-feedback">
                                    <input type="text" class="form-control cpf"  placeholder="CPF do segundo titular" name="segtitula_cpf" value="{{old('segtitula_cpf') ? old('segtitula_cpf') : Auth::user()->segtitula_cpf}}"/>
                                </div>

                                <div class="form-group has-feedback">
                                    <input type="text" class="form-control cpf"  placeholder="CPF" disabled="" name="cpf" value="{{old('cpf') ? old('cpf') : Auth::user()->cpf}}"/>
                                </div>

                                <div class="form-group has-feedback">
                                    <input type="text" class="form-control data" placeholder="Data de Nascimento" disabled="" name="nascimento" value="{{old('nascimento') ? old('nascimento') : Auth::user()->getNascimento()}}"/>
                                </div>

                                <div class="form-group has-feedback">
                                    <input type="text" class="form-control telefone" data-inputmask="'mask': ['99-9999-9999[9]', '+99 99 9999-9999[9]']" data-mask placeholder="Telefone" name="telefone" value="{{old('telefone') ? old('telefone') : Auth::user()->telefone}}"/>
                                </div>


                                <div class="form-group has-feedback">
                                    <input type="text" class="form-control" placeholder="Endereço" name="endereco" value="{{old('endereco') ? old('endereco') : Auth::user()->endereco}}"/>
                                </div>

                                <div class="form-group has-feedback">
                                    <input type="text" class="form-control" placeholder="Bairro" name="bairro" value="{{old('bairro') ? old('bairro') : Auth::user()->bairro}}"/>
                                </div>

                                <div class="form-group has-feedback">
                                    <input type="text" class="form-control" placeholder="Cidade" name="cidade" value="{{old('cidade') ? old('cidade') : Auth::user()->cidade}}"/>
                                </div>

                                <div class="form-group has-feedback">
                                    <input type="text" class="form-control" placeholder="Estado" name="estado" value="{{old('estado') ? old('estado') : Auth::user()->estado}}"/>
                                </div>
                                
                                <div class="form-group has-feedback">
                                    <input type="text" class="form-control" placeholder="Pin" name="pin"/>
                                </div>
                                
                                <div class="form-group has-feedback">
                                    <label>Autenticação em dois fatores</label>
                                    <select type="text" class="form-control" name="fa_ativo">
                                        <option value="1" {{Auth::user()->fa_ativo == 1 ? 'selected' : ''}}>Sim</option>
                                        <option value="0" {{Auth::user()->fa_ativo == 0 ? 'selected' : ''}}>Não</option>
                                    </select>
                                </div>

                            </div>
                            <!-- /.tab-pane -->
                            <div class="tab-pane" id="tab_2">

                                <div class="form-group has-feedback">
                                    <label>Carteira para Saque</label>
                                    <input type="text" class="form-control" placeholder="Código da sua carteira Vulcoin" name="bitzpayer_id" value="{{old('bitzpayer_id') ? old('bitzpayer_id') : Auth::user()->bitzpayer_id}}"/>
                                </div>

                                <div class="form-group has-feedback">
                                    <label>Carteira BO</label>
                                    <input type="text" class="form-control" readonly value="{{Auth::user()->carteira}}"/>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab_3">
                                <div class="btn btn-danger" onclick="codigoSeg()">Redefinir o código de segurança e enviar via e-mail</div>
                                <br>
                                <div class="col-md-2"> <a href="/novo-qrcode" class="btn btn-primary btn-block text-uppercase">Novo QRCode</a></div>
                                <br><br>
                            </div>
                            <div class="tab-pane" id="tab_4">
                                <div class="form-group has-feedback">
                                    <input type="password" class="form-control" placeholder="Senha Atual" name="current_password"/>
                                </div>
                                <div class="form-group has-feedback">
                                    <input type="password" class="form-control" placeholder="Senha" name="password"/>
                                </div>

                                <div class="form-group has-feedback">
                                    <input type="password" class="form-control" placeholder="Repita a senha" name="password_confirmation"/>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">Atualizar</button>
                        </div>

                        <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
            </div>
            <!-- /.box-body -->


            </form>
    </div><!-- /.box -->
    </section>


    <!-- /.content -->
@endsection
@section('page-js')
 <!-- InputMask -->
    <script src="{{ env('CFURL').('/plugins/input-mask/jquery.inputmask.js') }}"></script>
    <script src="{{ env('CFURL').('/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
    <script src="{{ env('CFURL').('/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
    <script>
        $(function () {
            $('.data').inputmask("99-99-9999");
            $('.cpf').inputmask("999.999.999-99");
            $("[data-mask]").inputmask();
        });
        function codigoSeg() {
            if (confirm('Tem certeza?Sò continue essa operação se você tiver acesso ao e-mail vinculado a esse conta.')) {

                $.ajax({
                    url: 'novaChave', success: function (result) {
                        alert(result);
                    }
                });
            }
            return false;
        }
    </script>
@endsection