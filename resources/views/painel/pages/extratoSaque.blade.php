@extends('layouts.app')
@section('title-head')
    Extrato de Saque
@endsection
@section('title-body')
    Extrato de Saque
@endsection
@section('page-css')

@endsection
@section('main-content')
    <section class="content">
        <div class="row">
            <div class="col-12">

                <div class="box box-solid bg-dark">
                    <div class="box-header with-border">
                        <h3 class="box-title">Extrato de Saque</h3>
                        <h6 class="box-subtitle">Listagem de todas os saques efetuados do usuário: {{ Auth::user()->username }}</h6>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table id="extrato_saque" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Valor Sacado</th>
                                    <th>Status</th>
                                    <th>Data de solicitação</th>
                                    <th>Data depositada</th>
                                    <th>Carteira do depósito</th>
                                    <th>Hash de comprovação</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach(Auth::user()->saqueLog as $s)
                                    <?php
                                    if(isset($s->comprovante)){
                                        $curl = curl_init();
                                        curl_setopt_array($curl, [
                                            CURLOPT_RETURNTRANSFER => 1,
                                            CURLOPT_URL => "http://68.183.161.149/api/gettransaction?hash=".$s->comprovante
                                        ]);
                                        $result = json_decode(curl_exec($curl));
                                        curl_close($curl);
                                    }

                                    ?>
                                    <tr>
                                        <td>{{$s->id}}</td>
                                        <td>{{$s->saque_realizado}}</td>
                                        <td>
                                            @if($s->status == 0)
                                                <label class="btn-xs btn-danger"> Cancelado </label>
                                            @endif
                                            @if($s->status == 1)
                                                <label class="btn-xs btn-warning"> Aguardando aprovação </label>
                                            @endif
                                            @if($s->status == 2)
                                                <label class="btn-xs btn-success"> Aprovado </label>
                                            @endif
                                        </td>
                                        <td>{{date ('d/m/Y H:i:s', strtotime($s->created_at))}}</td>
                                        <td>
                                            @if($s->status == 2)
                                                {{date ('d/m/Y H:i:s', strtotime($s->updated_at))}}
                                            @endif
                                        </td>
                                        <td>
                                            {{ isset($s->comprovante) && isset($result) && $result->status != 500 ? $result->para : '' }}
                                        </td>
                                        <td>
                                            {{ isset($s->comprovante) ? $s->comprovante : ''}}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>

@endsection
@section('page-js')

    <!-- This is data table -->
    <script src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js') }}"></script>

    <!-- start - This is for export functionality only -->
    <script src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('../../assets/vendor_plugins/DataTables-1.10.15/ex-js/jszip.min.js') }}"></script>
    <script src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/ex-js/pdfmake.min.js') }}"></script>
    <script src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/ex-js/vfs_fonts.js') }}"></script>
    <script src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.print.min.js') }}"></script>
    <!-- end - This is for export functionality only -->

    <!-- Crypto_Admin for Data Table -->
    <script src="{{ asset('../../assets/js/pages/data-table.js') }}"></script>
    <script type="text/javascript">
    $('#extrato_saque').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        "order": [[ 0, "desc" ]]
    });
    </script> 
@endsection

