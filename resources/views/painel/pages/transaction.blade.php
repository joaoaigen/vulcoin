@extends('layouts.app')
@section('title-head')
Histórico de Transações
@endsection
@section('title-body')
Histórico de Transações
@endsection
@section('page-css')

@endsection
@section('main-content')    

<?php
$curl = curl_init();
curl_setopt_array($curl, [
    CURLOPT_RETURNTRANSFER => 1,
    CURLOPT_URL => "http://68.183.161.149/api/listtransactions?account=" . Auth::user()->username
]);
$result = json_decode(curl_exec($curl));
curl_close($curl);

date_default_timezone_set('America/Sao_Paulo');
?>
<section class="content">
    <div class="row">            
        <div class="col-12">
            <div class="box box-solid bg-dark">
                <div class="box-header with-border">
                    <h3 class="box-title">Histórico de transações</h3>
                    <h6 class="box-subtitle">Listagem de todas as transações do usuário: {{ Auth::user()->username }}</h6>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table id="extratos" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
                            <thead>
                                <tr>                                    
                                    <th>Usuário</th>
                                    <th>Tipo</th>
                                    <th>Quantidade</th>
                                    <th>Horário</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($result->info as $row)
                                @if(isset($row->otheraccount))
                                <tr>
                                    <td>{{ $row->otheraccount == '' ? 'Carteira Vulcoin' : $row->otheraccount}}</td>                                    
                                    <td style="color: {{ isset($row->amount) && substr($row->amount, 0, 1) == '-' ? 'red' : 'green' }}" >{{$row->category == 'move' && substr($row->amount, 0, 1) == '-' ? 'Enviado' : 'Recebido'}}</td>                                    
                                    <td style="color: {{ isset($row->amount) && substr($row->amount, 0, 1) == '-' ? 'red' : 'green' }}">{{$row->amount}}</td> 
                                    <td>{{ date('d/m/Y H:i:s', $row->time) }}
                                </tr>
                                @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>

@endsection
@section('page-js')

<!-- This is data table -->
<script src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('../../assets/assets/vendor_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<!-- start - This is for export functionality only -->
<script src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.flash.min.js') }}"></script>
<script src="{{ asset('../../assets/vendor_plugins/DataTables-1.10.15/ex-js/jszip.min.js') }}"></script>
<script src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/ex-js/pdfmake.min.js') }}"></script>
<script src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/ex-js/vfs_fonts.js') }}"></script>
<script src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.print.min.js') }}"></script>
<!-- end - This is for export functionality only -->

<!-- Crypto_Admin for Data Table -->
<script src="{{ asset('../../assets/js/pages/data-table.js') }}"></script>    
<script type="text/javascript">
$('#extratos').DataTable({
    dom: 'Bfrtip',
    buttons: [
        'copy', 'csv', 'excel', 'pdf', 'print'
    ],
    "order": [[ 3, "desc" ]]
});
</script>    
@endsection
