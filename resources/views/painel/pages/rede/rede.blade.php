@extends('layouts.app')

@section('page_css')
<link rel="stylesheet" href="{{env('CFURL').('/css/tree.css')}}">
<style>
    .img-responsive {
        margin: 0 auto;
        width: 50px;
        margin-bottom: 5px;
    }

    .popover {
        min-width: 300px;
        max-width: 500px;
    }

    .popover .popover-content {
        padding: 10px;
    }

    .popover .popover-title {
        padding: 10px;
    }

    .tree li a {
        color: #000;
    }

    @media screen and (max-width: 1024px) {
        .arvore {

        }
    }

    @media screen and (min-width: 1250px) {
        .arvore {
            margin: 0 auto;
        }
    }

    .tree li a {
        border: none;
        padding: 0;
        margin: 0;
    }

    .tree li a:hover, .tree li a:hover + ul li a {
        background: none;
        border: none;
    }

    /*Connector styles on hover*/
    .tree li a:hover + ul li::after,
    .tree li a:hover + ul li::before,
    .tree li a:hover + ul::before,
    .tree li a:hover + ul ul::before {
        border-color: #6C7A89;
    }

    .tree .init:before {
        border: none;
    }

</style>
@endsection

@section('htmlheader_title')
Minha Rede
@endsection

@section('contentheader_title')
Minha Rede
@endsection

@section('breadcrumb')
<li class="active">Minha Rede</li>
@endsection

@section('contentheader_description')

@endsection

@section('main-content')

<?php

function printPopOver($id, $login, $nome, $status) {
    $usr = new \App\User();
    $userInfo = $usr->userInfo($id);
    if (isset(App\Pacote::where('id', $userInfo['pacote'])->first()->nome)) {
        $pacote = App\Pacote::where('id', $userInfo['pacote'])->first()->nome;
        $status = $status ? 'Ativo' : 'Inativo';
        return "ID:<b>$id</b><br/> Login:<b>$login</b><br/> Nome:<b>$nome</b></br> Status: </b>$status</b><br>Pacote: $pacote";
    }
}
?>
<!-- Small boxes (Stat box) -->
<div class="row" id="tree-family">

    <div class="col-md-12">
        <div class="" style="">


            <div class="col-lg-2 col-xs-3">
                <!-- small box -->
                <div class="small-box bg-gray">

                    <div class="inner">
                        <h3>{{Auth::user()->totalEsquerda()}}</h3>
                        <p>Total Esquerda</p>
                    </div>

                </div>
            </div><!-- ./col -->
            <div class="col-lg-2 col-xs-3 pull-right">
                <!-- small box -->
                <div class="small-box bg-gray">

                    <div class="inner">
                        <h3>{{Auth::user()->totalDireita()}}</h3>
                        <p>Total Direita</p>
                    </div>


                </div>
            </div><!-- ./col -->

            <div class="col-md-8">

                <div class="small-box bg-gray">
                    <div class="inner">
                        <p> Buscar Usuario</p>
                        <form class="formAjax form-horizontal" action="{{url('painel/minha-rede/busca')}}" method="POST">
                            {{csrf_field()}}
                            <label>
                                <input name="busca" class="form-control" placeholder="Login ou ID" style="width: 500px; border-radius: 3px;height: 35px">
                            </label>
                            <button type="submit" class="btn btn-primary">Buscar</button>
                        </form>
                    </div>

                    <div class="icon">
                        <i class="ion ion-search"></i>
                    </div>

                    <small> &nbsp;</small>
                </div>

            </div>

            <!-- /.box-header -->
            <div class="box-body">
                <div class="">
                    <div class="tree container">
                        <div class="">
                            <ul class="arvore">
                                <li>
                                    <ul class="init">
                                        <li>
                                            <!-- Meu Usuario -->
                                            <a href="{{url('/painel/minha-rede/'.(Auth::user()->id))}}">
                                                <div id="level-0" data-toggle="popover" data-content="{{printPopOver(Auth::user()->id,Auth::user()->username,Auth::user()->name,Auth::user()->ativo)}}">
                                                    <img src="{{(Auth::user()->ativado() ? url('/img/pacote-'.Auth::user()->pacote.'.png') : url('/img/user_inativo.png'))}}" class="img-responsive">
                                                </div>
                                            </a>

                                            <ul>
                                                <?php $filho = Auth::user()->getFilhos();
                                                    dd($filho);
                                                ?>
                                               
                                                @if(isset($filho[0]) && $filho[0]->direcao == 'esquerda')
                                                <li>

                                                    <a href="{{url('/painel/minha-rede/'.($filho[0]->id))}}">
                                                        <div id="level-1" data-toggle="popover" data-content="{{printPopOver($filho[0]->id,$filho[0]->username,$filho[0]->name,$filho[0]->ativo)}}">
                                                            <img src="{{($filho[0]->ativado() ? url('/img/pacote-'.$filho[0]->pacote.'.png') : url('/img/user_inativo.png'))}}" class="img-responsive">
                                                        </div>
                                                    </a>
                                                    <ul>

                                                        <?php $filho1 = $filho[0]->getFilhos(); ?>

                                                        @if(isset($filho1[0]) && $filho1[0]->direcao == 'esquerda')
                                                        <li>

                                                            <a href="{{url('/painel/minha-rede/'.($filho1[0]->id))}}">
                                                                <div id="level-2" data-toggle="popover" data-content="{{printPopOver($filho1[0]->id,$filho1[0]->username,$filho1[0]->name,$filho1[0]->ativo)}}">
                                                                    <img src="{{($filho1[0]->ativado() ? url('/img/pacote-'.$filho1[0]->pacote.'.png') : url('/img/user_inativo.png'))}}" class="img-responsive">
                                                                </div>
                                                            </a>
                                                            <ul>
                                                                <?php $filho2 = $filho1[0]->getFilhos(); ?>

                                                                @if(isset($filho2[0]) && $filho2[0]->direcao == 'esquerda')

                                                                <li>
                                                                    <a href="{{url('/painel/minha-rede/'.($filho2[0]->id))}}">
                                                                        <div id="level-3" data-toggle="popover" data-content="{{printPopOver($filho2[0]->id,$filho2[0]->username,$filho2[0]->name,$filho2[0]->ativo)}}">
                                                                            <img src="{{($filho2[0]->ativado() ? url('/img/pacote-'.$filho2[0]->pacote.'.png') : url('/img/user_inativo.png'))}}" class="img-responsive">
                                                                        </div>
                                                                    </a>
                                                                    <ul>
                                                                        <?php $filho3 = $filho2[0]->getFilhos(); ?>

                                                                        @if(isset($filho3[0]) && $filho3[0]->direcao == 'esquerda')
                                                                        <li>
                                                                            <a href="{{url('/painel/minha-rede/'.($filho3[0]->id))}}">
                                                                                <div id="level-1" data-toggle="popover" data-content="{{printPopOver($filho3[0]->id,$filho3[0]->username,$filho3[0]->name,$filho3[0]->ativo)}}">
                                                                                    <img src="{{($filho3[0]->ativado() ? url('/img/pacote-'.$filho3[0]->pacote.'.png') : url('/img/user_inativo.png'))}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                        @else
                                                                        <li>
                                                                            <a href="#">
                                                                                <div>
                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                        @endif

                                                                        <?php
                                                                        if (isset($filho3[0]) && $filho3[0]->direcao == 'direita'):
                                                                            $filho3[1] = $filho3[0];
                                                                        endif;
                                                                        ?>

                                                                        @if(isset($filho3[1]) && $filho3[1]->direcao == 'direita')
                                                                        <li>
                                                                            <a href="{{url('/painel/minha-rede/'.($filho3[1]->id))}}">
                                                                                <div id="level-1" data-toggle="popover" data-content="{{printPopOver($filho3[1]->id,$filho3[1]->username,$filho3[1]->name,$filho3[1]->ativo)}}">
                                                                                    <img src="{{($filho3[1]->ativado() ? url('/img/pacote-'.$filho3[1]->pacote.'.png') : url('/img/user_inativo.png'))}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                        @else
                                                                        <li>
                                                                            <a href="#">
                                                                                <div>
                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                        @endif
                                                                    </ul>
                                                                </li>
                                                                @else
                                                                <li>
                                                                    <a href="#">
                                                                        <div>
                                                                            <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                        </div>
                                                                    </a>
                                                                    <ul>
                                                                        <li>
                                                                            <a href="#">
                                                                                <div>
                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="#">
                                                                                <div>
                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                </li>
                                                                @endif
                                                                <?php
                                                                if (isset($filho2[0]) && $filho2[0]->direcao == 'direita'):
                                                                    $filho2[1] = $filho2[0];
                                                                endif;
                                                                ?>
                                                                @if(isset($filho2[1]) && $filho2[1]->direcao == 'direita')
                                                                <li>
                                                                    <a href="{{url('/painel/minha-rede/'.($filho2[1]->id))}}">
                                                                        <div id="level-1" data-toggle="popover" data-content="{{printPopOver($filho2[1]->id,$filho2[1]->username,$filho2[1]->name,$filho2[1]->ativo)}}">
                                                                            <img src="{{($filho2[1]->ativado() ?url('/img/pacote-'.$filho2[1]->pacote.'.png') : url('/img/user_inativo.png'))}}" class="img-responsive">
                                                                        </div>
                                                                    </a>
                                                                    <ul>
                                                                        <?php $filho4 = $filho2[1]->getFilhos(); ?>

                                                                        @if(isset($filho4[0]) && $filho4[0]->direcao == 'esquerda')
                                                                        <li>
                                                                            <a href="{{url('/painel/minha-rede/'.($filho4[0]->id))}}">
                                                                                <div id="level-1" data-toggle="popover" data-content="{{printPopOver($filho4[0]->id,$filho4[0]->username,$filho4[0]->name,$filho4[0]->ativo)}}">
                                                                                    <img src="{{($filho4[0]->ativado() ?url('/img/pacote-'.$filho4[0]->pacote.'.png') : url('/img/user_inativo.png'))}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                        @else
                                                                        <li>
                                                                            <a href="#">
                                                                                <div>
                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                        @endif
                                                                        <?php
                                                                        if (isset($filho4[0]) && $filho4[0]->direcao == 'direita'):
                                                                            $filho4[1] = $filho4[0];
                                                                        endif;
                                                                        ?>
                                                                        @if(isset($filho4[1]) && $filho4[1]->direcao == 'direita')
                                                                        <li>
                                                                            <a href="{{url('/painel/minha-rede/'.($filho4[1]->id))}}">
                                                                                <div id="level-1" data-toggle="popover" data-content="{{printPopOver($filho4[1]->id,$filho4[1]->username,$filho4[1]->name,$filho4[1]->ativo)}}">
                                                                                    <img src="{{($filho4[1]->ativado() ? url('/img/pacote-'.$filho4[1]->pacote.'.png') : url('/img/user_inativo.png'))}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                        @else
                                                                        <li>
                                                                            <a href="#">
                                                                                <div>
                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                        @endif
                                                                    </ul>
                                                                </li>
                                                                @else
                                                                <li>
                                                                    <a href="#">
                                                                        <div>
                                                                            <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                        </div>
                                                                    </a>
                                                                    <ul>
                                                                        <li>
                                                                            <a href="#">
                                                                                <div>
                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="#">
                                                                                <div>
                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                </li>
                                                                @endif
                                                            </ul>
                                                        </li>
                                                        @else
                                                        <li>
                                                            <a href="#">
                                                                <div>
                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                </div>
                                                            </a>
                                                            <ul>
                                                                <li>
                                                                    <a href="#">
                                                                        <div>
                                                                            <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                        </div>
                                                                    </a>
                                                                    <ul>
                                                                        <li>
                                                                            <a href="#">
                                                                                <div>
                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="#">
                                                                                <div>
                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                </li>
                                                                <li>
                                                                    <a href="#">
                                                                        <div>
                                                                            <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                        </div>
                                                                    </a>
                                                                    <ul>
                                                                        <li>
                                                                            <a href="#">
                                                                                <div>
                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="#">
                                                                                <div>
                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        @endif
                                                        <?php
                                                        if (isset($filho1[0]) && $filho1[0]->direcao == 'direita'):
                                                            $filho1[1] = $filho1[0];
                                                        endif;
                                                        ?>
                                                        @if(isset($filho1[1]) && $filho1[1]->direcao == 'direita')
                                                        <li>

                                                            <a href="{{url('/painel/minha-rede/'.($filho1[1]->id))}}">
                                                                <div id="level-4" data-toggle="popover" data-content="{{printPopOver($filho1[1]->id,$filho1[1]->username,$filho1[1]->name,$filho1[1]->ativo)}}">
                                                                    <img src="{{($filho1[1]->ativado() ? url('/img/pacote-'.$filho1[1]->pacote.'.png') : url('/img/user_inativo.png'))}}" class="img-responsive">
                                                                </div>
                                                            </a>
                                                            <ul>
                                                                <?php $filho5 = $filho1[1]->getFilhos(); ?>

                                                                @if(isset($filho5[0]) && $filho5[0]->direcao == 'esquerda')
                                                                <li>
                                                                    <a href="{{url('/painel/minha-rede/'.($filho5[0]->id))}}">
                                                                        <div id="level-5" data-toggle="popover" data-content="{{printPopOver($filho5[0]->id,$filho5[0]->username,$filho5[0]->name,$filho5[0]->ativo)}}">
                                                                            <img src="{{($filho5[0]->ativado() ? url('/img/pacote-'.$filho5[0]->pacote.'.png') : url('/img/user_inativo.png'))}}" class="img-responsive">
                                                                        </div>
                                                                    </a>
                                                                    <ul>


                                                                        <?php $filho12 = $filho5[0]->getFilhos(); ?>

                                                                        @if(isset($filho12[0]) && $filho12[0]->direcao == 'esquerda')
                                                                        <li>
                                                                            <a href="{{url('/painel/minha-rede/'.($filho12[0]->id))}}">
                                                                                <div id="level-12-esquerda" data-toggle="popover" data-content="{{printPopOver($filho12[0]->id,$filho12[0]->username,$filho12[0]->name,$filho12[0]->ativo)}}">
                                                                                    <img src="{{($filho12[0]->ativado() ? url('/img/pacote-'.$filho12[0]->pacote.'.png') : url('/img/user_inativo.png'))}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                        @else
                                                                        <li>
                                                                            <a href="#">
                                                                                <div>
                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                        @endif
                                                                        <?php
                                                                        if (isset($filho12[0]) && $filho12[0]->direcao == 'direita'):
                                                                            $filho12[1] = $filho12[0];
                                                                        endif;
                                                                        ?>
                                                                        @if(isset($filho12[1]) && $filho12[1]->direcao == 'direita')
                                                                        <li>
                                                                            <a href="{{url('/painel/minha-rede/'.($filho12[1]->id))}}">
                                                                                <div id="level-12-direita" data-toggle="popover" data-content="{{printPopOver($filho12[1]->id,$filho12[1]->username,$filho12[1]->name,$filho12[1]->ativo)}}">
                                                                                    <img src="{{($filho12[1]->ativado() ? url('/img/pacote-'.$filho12[1]->pacote.'.png') : url('/img/user_inativo.png'))}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                        @else
                                                                        <li>
                                                                            <a href="#">
                                                                                <div>
                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                        @endif


                                                                    </ul>
                                                                </li>
                                                                @else
                                                                <li>
                                                                    <a href="#">
                                                                        <div>
                                                                            <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                        </div>
                                                                    </a>
                                                                    <ul>
                                                                        <li>
                                                                            <a href="#">
                                                                                <div>
                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="#">
                                                                                <div>
                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                </li>
                                                                @endif
                                                                <?php
                                                                if (isset($filho5[0]) && $filho5[0]->direcao == 'direita'):
                                                                    $filho5[1] = $filho5[0];
                                                                endif;
                                                                ?>
                                                                @if(isset($filho5[1]) && $filho5[1]->direcao == 'direita')
                                                                <li>
                                                                    <a href="{{url('/painel/minha-rede/'.($filho5[1]->id))}}">
                                                                        <div id="level-6" data-toggle="popover" data-content="{{printPopOver($filho5[1]->id,$filho5[1]->username,$filho5[1]->name,$filho5[1]->ativo)}}">
                                                                            <img src="{{($filho5[1]->ativado() ? url('/img/pacote-'.$filho5[1]->pacote.'.png') : url('/img/user_inativo.png'))}}" class="img-responsive">
                                                                        </div>
                                                                    </a>
                                                                    <ul>
                                                                        <?php $filho13 = $filho5[1]->getFilhos(); ?>

                                                                        @if(isset($filho13[0]) && $filho13[0]->direcao == 'esquerda')
                                                                        <li>
                                                                            <a href="{{url('/painel/minha-rede/'.($filho13[0]->id))}}">
                                                                                <div id="level-13-esquerda" data-toggle="popover" data-content="{{printPopOver($filho13[0]->id,$filho13[0]->username,$filho13[0]->name,$filho13[0]->ativo)}}">
                                                                                    <img src="{{($filho13[0]->ativado() ? url('/img/pacote-'.$filho13[0]->pacote.'.png') : url('/img/user_inativo.png'))}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                        @else
                                                                        <li>
                                                                            <a href="#">
                                                                                <div>
                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                        @endif
                                                                        <?php
                                                                        if (isset($filho13[0]) && $filho13[0]->direcao == 'direita'):
                                                                            $filho13[1] = $filho13[0];
                                                                        endif;
                                                                        ?>
                                                                        @if(isset($filho13[1]) && $filho13[1]->direcao == 'direita')
                                                                        <li>
                                                                            <a href="{{url('/painel/minha-rede/'.($filho13[1]->id))}}">
                                                                                <div id="level-13-direita" data-toggle="popover" data-content="{{printPopOver($filho13[1]->id,$filho13[1]->username,$filho13[1]->name,$filho13[1]->ativo)}}">
                                                                                    <img src="{{($filho13[1]->ativado() ? url('/img/pacote-'.$filho13[1]->pacote.'.png') : url('/img/user_inativo.png'))}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                        @else
                                                                        <li>
                                                                            <a href="#">
                                                                                <div>
                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                        @endif
                                                                    </ul>
                                                                </li>
                                                                @else
                                                                <li>
                                                                    <a href="#">
                                                                        <div>
                                                                            <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                        </div>
                                                                    </a>
                                                                    <ul>
                                                                        <li>
                                                                            <a href="#">
                                                                                <div>
                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="#">
                                                                                <div>
                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                </li>
                                                                @endif
                                                            </ul>
                                                        </li>
                                                        @else
                                                        <li>
                                                            <a href="#">
                                                                <div>
                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                </div>
                                                            </a>
                                                            <ul>
                                                                <li>
                                                                    <a href="#">
                                                                        <div>
                                                                            <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                        </div>
                                                                    </a>
                                                                    <ul>
                                                                        <li>
                                                                            <a href="#">
                                                                                <div>
                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="#">
                                                                                <div>
                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                </li>
                                                                <li>
                                                                    <a href="#">
                                                                        <div>
                                                                            <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                        </div>
                                                                    </a>
                                                                    <ul>
                                                                        <li>
                                                                            <a href="#">
                                                                                <div>
                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="#">
                                                                                <div>
                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        @endif
                                                    </ul>
                                                </li>
                                                @else
                                                <li>
                                                    <a href="#">
                                                        <div>
                                                            <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                        </div>
                                                    </a>
                                                    <ul>
                                                        <li id="filho">
                                                            <a href="#">
                                                                <div>
                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                </div>
                                                            </a>
                                                            <ul>
                                                                <li id="filho">
                                                                    <a href="#">
                                                                        <div>
                                                                            <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                        </div>
                                                                    </a>
                                                                    <ul>
                                                                        <li id="filho">
                                                                            <a href="#">
                                                                                <div>
                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                        <li id="filho">
                                                                            <a href="#">
                                                                                <div>
                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                </li>
                                                                <li id="filho">
                                                                    <a href="#">
                                                                        <div>
                                                                            <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                        </div>
                                                                    </a>
                                                                    <ul>
                                                                        <li id="filho">
                                                                            <a href="#">
                                                                                <div>
                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                        <li id="filho">
                                                                            <a href="#">
                                                                                <div>
                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li id="filho">
                                                            <a href="#">
                                                                <div>
                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                </div>
                                                            </a>
                                                            <ul>
                                                                <li id="filho">
                                                                    <a href="#">
                                                                        <div>
                                                                            <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                        </div>
                                                                    </a>
                                                                    <ul>
                                                                        <li id="filho">
                                                                            <a href="#">
                                                                                <div>
                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                        <li id="filho">
                                                                            <a href="#">
                                                                                <div>
                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                </li>
                                                                <li id="filho">
                                                                    <a href="#">
                                                                        <div>
                                                                            <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                        </div>
                                                                    </a>
                                                                    <ul>
                                                                        <li id="filho">
                                                                            <a href="#">
                                                                                <div>
                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                        <li id="filho">
                                                                            <a href="#">
                                                                                <div>
                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </li>
                                                @endif

                                                <?php
                                                if (isset($filho[0]) && $filho[0]->direcao == 'direita'):
                                                    $filho[1] = $filho[0];
                                                endif;
                                                ?>

                                                @if(isset($filho[1]) && $filho[1]->direcao == 'direita')
                                                <li>
                                                    <a href="{{url('/painel/minha-rede/'.($filho[1]->id))}}">
                                                        <div id="level-1" data-toggle="popover" data-content="{{printPopOver($filho[1]->id,$filho[1]->username,$filho[1]->name,$filho[1]->ativo)}}">
                                                            <img src="{{($filho[1]->ativado() ? url('/img/pacote-'.$filho[1]->pacote.'.png') : url('/img/user_inativo.png'))}}" class="img-responsive">
                                                        </div>
                                                    </a>
                                                    <ul>

                                                        <?php $filho6 = $filho[1]->getFilhos(); ?>

                                                        @if(isset($filho6[0]) && $filho6[0]->direcao == 'esquerda')
                                                        <li>

                                                            <a href="{{url('/painel/minha-rede/'.($filho6[0]->id))}}">
                                                                <div id="level-6" data-toggle="popover" data-content="{{printPopOver($filho6[0]->id,$filho6[0]->username,$filho6[0]->name,$filho6[0]->ativo)}}">
                                                                    <img src="{{($filho6[0]->ativado() ? url('/img/pacote-'.$filho6[0]->pacote.'.png') : url('/img/user_inativo.png'))}}" class="img-responsive">
                                                                </div>
                                                            </a>
                                                            <ul>
                                                                <?php $filho7 = $filho6[0]->getFilhos(); ?>

                                                                @if(isset($filho7[0]) && $filho7[0]->direcao == 'esquerda')

                                                                <li>
                                                                    <a href="{{url('/painel/minha-rede/'.($filho7[0]->id))}}">
                                                                        <div id="level-7" data-toggle="popover" data-content="{{printPopOver($filho7[0]->id,$filho7[0]->username,$filho7[0]->name,$filho7[0]->ativo)}}">
                                                                            <img src="{{($filho7[0]->ativado() ? url('/img/pacote-'.$filho7[0]->pacote.'.png') : url('/img/user_inativo.png'))}}" class="img-responsive">
                                                                        </div>
                                                                    </a>
                                                                    <ul>
                                                                        <?php $filho8 = $filho7[0]->getFilhos(); ?>

                                                                        @if(isset($filho8[0]) && $filho8[0]->direcao == 'esquerda')
                                                                        <li>
                                                                            <a href="{{url('/painel/minha-rede/'.($filho8[0]->id))}}">
                                                                                <div id="level-8" data-toggle="popover" data-content="{{printPopOver($filho8[0]->id,$filho8[0]->username,$filho8[0]->name,$filho8[0]->ativo)}}">
                                                                                    <img src="{{($filho8[0]->ativado() ? url('/img/pacote-'.$filho8[0]->pacote.'.png') : url('/img/user_inativo.png'))}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                        @else
                                                                        <li>
                                                                            <a href="#">
                                                                                <div>
                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                        @endif

                                                                        <?php
                                                                        if (isset($filho8[0]) && $filho8[0]->direcao == 'direita'):
                                                                            $filho8[1] = $filho8[0];
                                                                        endif;
                                                                        ?>

                                                                        @if(isset($filho8[1]) && $filho8[1]->direcao == 'direita')
                                                                        <li>
                                                                            <a href="{{url('/painel/minha-rede/'.($filho8[1]->id))}}">
                                                                                <div id="level-1" data-toggle="popover" data-content="{{printPopOver($filho8[1]->id,$filho8[1]->username,$filho8[1]->name,$filho8[1]->ativo)}}">
                                                                                    <img src="{{($filho8[1]->ativado() ? url('/img/pacote-'.$filho8[1]->pacote.'.png') : url('/img/user_inativo.png'))}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                        @else
                                                                        <li>
                                                                            <a href="#">
                                                                                <div>
                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                        @endif
                                                                    </ul>
                                                                </li>
                                                                @else
                                                                <li>
                                                                    <a href="#">
                                                                        <div>
                                                                            <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                        </div>
                                                                    </a>
                                                                    <ul>
                                                                        <li>
                                                                            <a href="#">
                                                                                <div>
                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="#">
                                                                                <div>
                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                </li>
                                                                @endif
                                                                <?php
                                                                if (isset($filho7[0]) && $filho7[0]->direcao == 'direita'):
                                                                    $filho7[1] = $filho7[0];
                                                                endif;
                                                                ?>
                                                                @if(isset($filho7[1]) && $filho7[1]->direcao == 'direita')
                                                                <li>
                                                                    <a href="{{url('/painel/minha-rede/'.($filho7[1]->id))}}">
                                                                        <div id="level-1" data-toggle="popover" data-content="{{printPopOver($filho7[1]->id,$filho7[1]->username,$filho7[1]->name,$filho7[1]->ativo)}}">
                                                                            <img src="{{($filho7[1]->ativado() ?  url('/img/pacote-'.$filho7[1]->pacote.'.png') : url('/img/user_inativo.png'))}}" class="img-responsive">
                                                                        </div>
                                                                    </a>
                                                                    <ul>
                                                                        <?php $filho9 = $filho7[1]->getFilhos(); ?>

                                                                        @if(isset($filho9[0]) && $filho9[0]->direcao == 'esquerda')
                                                                        <li>
                                                                            <a href="{{url('/painel/minha-rede/'.($filho9[0]->id))}}">
                                                                                <div id="level-1" data-toggle="popover" data-content="{{printPopOver($filho9[0]->id,$filho9[0]->username,$filho9[0]->name,$filho9[0]->ativo)}}">
                                                                                    <img src="{{($filho9[0]->ativado() ? url('/img/pacote-'.$filho9[0]->pacote.'.png') : url('/img/user_inativo.png'))}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                        @else
                                                                        <li>
                                                                            <a href="#">
                                                                                <div>
                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                        @endif
                                                                        <?php
                                                                        if (isset($filho9[0]) && $filho9[0]->direcao == 'direita'):
                                                                            $filho9[1] = $filho9[0];
                                                                        endif;
                                                                        ?>
                                                                        @if(isset($filho9[1]) && $filho9[1]->direcao == 'direita')
                                                                        <li>
                                                                            <a href="{{url('/painel/minha-rede/'.($filho9[1]->id))}}">
                                                                                <div id="level-1" data-toggle="popover" data-content="{{printPopOver($filho9[1]->id,$filho9[1]->username,$filho9[1]->name,$filho9[1]->ativo)}}">
                                                                                    <img src="{{($filho9[1]->ativado() ?  url('/img/pacote-'.$filho9[1]->pacote.'.png') : url('/img/user_inativo.png'))}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                        @else
                                                                        <li>
                                                                            <a href="#">
                                                                                <div>
                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                        @endif
                                                                    </ul>
                                                                </li>
                                                                @else
                                                                <li>
                                                                    <a href="#">
                                                                        <div>
                                                                            <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                        </div>
                                                                    </a>
                                                                    <ul>
                                                                        <li>
                                                                            <a href="#">
                                                                                <div>
                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="#">
                                                                                <div>
                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                </li>
                                                                @endif
                                                            </ul>
                                                        </li>
                                                        @else
                                                        <li>
                                                            <a href="#">
                                                                <div>
                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                </div>
                                                            </a>
                                                            <ul>
                                                                <li>
                                                                    <a href="#">
                                                                        <div>
                                                                            <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                        </div>
                                                                    </a>
                                                                    <ul>
                                                                        <li>
                                                                            <a href="#">
                                                                                <div>
                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="#">
                                                                                <div>
                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                </li>
                                                                <li>
                                                                    <a href="#">
                                                                        <div>
                                                                            <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                        </div>
                                                                    </a>
                                                                    <ul>
                                                                        <li>
                                                                            <a href="#">
                                                                                <div>
                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="#">
                                                                                <div>
                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        @endif

                                                        <?php
                                                        if (isset($filho6[0]) && $filho6[0]->direcao == 'direita'):
                                                            $filho6[1] = $filho6[0];
                                                        endif;
                                                        ?>

                                                        @if(isset($filho6[1]) && $filho6[1]->direcao == 'direita')
                                                        <li>

                                                            <a href="{{url('/painel/minha-rede/'.($filho6[1]->id))}}">
                                                                <div id="level-1" data-toggle="popover" data-content="{{printPopOver($filho6[1]->id,$filho6[1]->username,$filho6[1]->name,$filho6[1]->ativo)}}">
                                                                    <img src="{{($filho6[1]->ativado() ?  url('/img/pacote-'.$filho6[1]->pacote.'.png') : url('/img/user_inativo.png'))}}" class="img-responsive">
                                                                </div>
                                                            </a>

                                                            <ul>
                                                                <?php $filho10 = $filho6[1]->getFilhos(); ?>

                                                                @if(isset($filho10[0]) && $filho10[0]->direcao == 'esquerda')
                                                                <li id="filho-10-esquerda">
                                                                    <a href="{{url('/painel/minha-rede/'.($filho10[0]->id))}}">
                                                                        <div id="level-1" data-toggle="popover" data-content="{{printPopOver($filho10[0]->id,$filho10[0]->username,$filho10[0]->name,$filho10[0]->ativo)}}">
                                                                            <img src="{{($filho10[0]->ativado() ?  url('/img/pacote-'.$filho10[0]->pacote.'.png') : url('/img/user_inativo.png'))}}" class="img-responsive">
                                                                        </div>
                                                                    </a>

                                                                    <ul>
                                                                        <?php $filho11 = $filho10[0]->getFilhos(); ?>
                                                                        @if(isset($filho11[0]) && $filho11[0]->direcao == 'esquerda')
                                                                        <li>
                                                                            <a href="{{url('/painel/minha-rede/'.($filho11[0]->id))}}">
                                                                                <div id="level-11" data-toggle="popover" data-content="{{printPopOver($filho11[0]->id,$filho11[0]->username,$filho11[0]->name,$filho11[0]->ativo)}}">
                                                                                    <img src="{{($filho10[0]->ativado() ?  url('/img/pacote-'.$filho10[0]->pacote.'.png') : url('/img/user_inativo.png'))}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                        @else
                                                                        <li>
                                                                            <a href="#">
                                                                                <div>
                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                        @endif

                                                                        <?php
                                                                        if (isset($filho11[0]) && $filho11[0]->direcao == 'direita'):
                                                                            $filho11[1] = $filho11[0];
                                                                        endif;
                                                                        ?>

                                                                        @if(isset($filho11[1]) && $filho11[1]->direcao == 'direita')
                                                                        <li id="filho-11">
                                                                            <a href="{{url('/painel/minha-rede/'.($filho11[0]->id))}}">
                                                                                <div id="level-1" data-toggle="popover" data-content="{{printPopOver($filho11[0]->id,$filho11[0]->username,$filho11[0]->name,$filho11[0]->ativo)}}">
                                                                                    <img src="{{($filho10[0]->ativado() ?  url('/img/pacote-'.$filho10[0]->pacote.'.png'): url('/img/user_inativo.png'))}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                        @else
                                                                        <li>
                                                                            <a href="#">
                                                                                <div>
                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                        @endif
                                                                    </ul>

                                                                </li>
                                                                @else
                                                                <li>
                                                                    <a href="#">
                                                                        <div>
                                                                            <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                        </div>
                                                                    </a>
                                                                    <ul>
                                                                        <li>
                                                                            <a href="#">
                                                                                <div>
                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="#">
                                                                                <div>
                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                </li>
                                                                @endif
                                                                <?php
                                                                if (isset($filho10[0]) && $filho10[0]->direcao == 'direita'):
                                                                    $filho10[1] = $filho10[0];
                                                                endif;
                                                                ?>
                                                                @if(isset($filho10[1]) && $filho10[1]->direcao == 'direita')
                                                                <li id="filho-10">
                                                                    <a href="{{url('/painel/minha-rede/'.($filho10[1]->id))}}">
                                                                        <div id="level-1" data-toggle="popover" data-content="{{printPopOver($filho10[1]->id,$filho10[1]->username,$filho10[1]->name,$filho10[1]->ativo)}}">
                                                                            <img src="{{($filho10[1]->ativado() ?  url('/img/pacote-'.$filho10[1]->pacote.'.png') : url('/img/user_inativo.png'))}}" class="img-responsive">
                                                                        </div>
                                                                    </a>
                                                                    <ul>
                                                                        <?php $filho14 = $filho10[1]->getFilhos(); ?>
                                                                        @if(isset($filho14[0]) && $filho14[0]->direcao == 'esquerda')
                                                                        <li>
                                                                            <a href="{{url('/painel/minha-rede/'.($filho14[0]->id))}}">
                                                                                <div id="level-11" data-toggle="popover" data-content="{{printPopOver($filho14[0]->id,$filho14[0]->username,$filho14[0]->name,$filho14[0]->ativo)}}">
                                                                                    <img src="{{($filho14[0]->ativado() ?  url('/img/pacote-'.$filho14[0]->pacote.'.png') : url('/img/user_inativo.png'))}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                        @else
                                                                        <li>
                                                                            <a href="#">
                                                                                <div>
                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                        @endif

                                                                        <?php
                                                                        if (isset($filho14[0]) && $filho14[0]->direcao == 'direita'):
                                                                            $filho14[1] = $filho14[0];
                                                                        endif;
                                                                        ?>

                                                                        @if(isset($filho14[1]) && $filho14[1]->direcao == 'direita')
                                                                        <li id="filho-11">
                                                                            <a href="{{url('/painel/minha-rede/'.($filho14[0]->id))}}">
                                                                                <div id="level-1" data-toggle="popover" data-content="{{printPopOver($filho14[0]->id,$filho14[0]->username,$filho14[0]->name,$filho14[0]->ativo)}}">
                                                                                    <img src="{{($filho14[0]->ativado() ?  url('/img/pacote-'.$filho14[0]->pacote.'.png') : url('/img/user_inativo.png'))}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                        @else
                                                                        <li>
                                                                            <a href="#">
                                                                                <div>
                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                        @endif
                                                                    </ul>
                                                                </li>
                                                                @else
                                                                <li>
                                                                    <a href="#">
                                                                        <div>
                                                                            <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                        </div>
                                                                    </a>

                                                                    <ul>
                                                                        <li>
                                                                            <a href="#">
                                                                                <div>
                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="#">
                                                                                <div>
                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                </li>
                                                                @endif
                                                            </ul>
                                                        </li>
                                                        @else
                                                        <li>
                                                            <a href="#">
                                                                <div>
                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                </div>
                                                            </a>
                                                            <ul>
                                                                <li>
                                                                    <a href="#">
                                                                        <div>
                                                                            <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                        </div>
                                                                    </a>
                                                                    <ul>
                                                                        <li>
                                                                            <a href="#">
                                                                                <div>
                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="#">
                                                                                <div>
                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                </li>
                                                                <li>
                                                                    <a href="#">
                                                                        <div>
                                                                            <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                        </div>
                                                                    </a>
                                                                    <ul>
                                                                        <li>
                                                                            <a href="#">
                                                                                <div>
                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="#">
                                                                                <div>
                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        @endif
                                                    </ul>
                                                </li>
                                                @else
                                                <li>
                                                    <a href="#">
                                                        <div>
                                                            <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                        </div>
                                                    </a>
                                                    <ul>
                                                        <li id="filho">
                                                            <a href="#">
                                                                <div>
                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                </div>
                                                            </a>
                                                            <ul>
                                                                <li id="filho">
                                                                    <a href="#">
                                                                        <div>
                                                                            <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                        </div>
                                                                    </a>
                                                                    <ul>
                                                                        <li id="filho">
                                                                            <a href="#">
                                                                                <div>
                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                        <li id="filho">
                                                                            <a href="#">
                                                                                <div>
                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                </li>
                                                                <li id="filho">
                                                                    <a href="#">
                                                                        <div>
                                                                            <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                        </div>
                                                                    </a>
                                                                    <ul>
                                                                        <li id="filho">
                                                                            <a href="#">
                                                                                <div>
                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                        <li id="filho">
                                                                            <a href="#">
                                                                                <div>
                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li id="filho">
                                                            <a href="#">
                                                                <div>
                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                </div>
                                                            </a>
                                                            <ul>
                                                                <li id="filho">
                                                                    <a href="#">
                                                                        <div>
                                                                            <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                        </div>
                                                                    </a>
                                                                    <ul>
                                                                        <li id="filho">
                                                                            <a href="#">
                                                                                <div>
                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                        <li id="filho">
                                                                            <a href="#">
                                                                                <div>
                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                </li>
                                                                <li id="filho">
                                                                    <a href="#">
                                                                        <div>
                                                                            <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                        </div>
                                                                    </a>
                                                                    <ul>
                                                                        <li id="filho">
                                                                            <a href="#">
                                                                                <div>
                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                        <li id="filho">
                                                                            <a href="#">
                                                                                <div>
                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                </div>
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </li>
                                                @endif
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
</div>


<!-- Main row -->
<div class="row">

    <!-- Left col -->
    <section class="col-lg-6 connectedSortable">

    </section><!-- /.Left col -->


</div><!-- /.row (main row) -->

@endsection

@section('page_scripts')
<script src="{{env('CFURL').('/plugins/zoomooz/jquery.zoomooz.min.js')}}"></script>
<script>

$(document).ready(function () {
    var lengths = $('.direito').map(function () {
        return $(this).find('li').length;
    }).get();
});

$(function () {

    $('[data-toggle="popover"]').popover({
        html: true,
        trigger: 'manual',
        container: $(this).attr('id'),
        placement: 'top',
        content: function () {
            $return = '<div class="hover-hovercard"></div>';
        }
    }).on("mouseenter", function () {
        var _this = this;
        $(this).popover("show");
        $(this).siblings(".popover").on("mouseleave", function () {
            $(_this).popover('hide');
        });
    }).on("mouseleave", function () {
        var _this = this;
        setTimeout(function () {
            if (!$(".popover:hover").length) {
                $(_this).popover("hide")
            }
        }, 100);
    });
})

</script>
@endsection
