<?php

/**
 * Step 5
 *
 * if some communication issue, this will be useful, this page check database status show us invoice payment status
 *
 */
define('MY_SHOP_LOGIN', 'xxxxxxxxxxxxxx');
define('MY_SHOP_SECRET', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa');

$shop_secret = MY_SHOP_SECRET;

$message = 'C' . $_POST['shop_login'] . $_POST['invoice_id'];
$signature = strtoupper(hash_hmac('sha256', pack('A*', $message), pack('A*', $shop_secret)));

if ($signature === $_POST['signature']) {

    # load your invoice
    $invoice = mysql_query('SELECT * FROM my_invoice_table WHERE invoice_id = :invoice_id');

    # check if payment was confirmed
    if ($invoice['payment_confirmed'] == true) {

        # if confirmed, say to bitzpayer: yes, we know the payment was received
        print 'CONFIRMED';

    } else {

        # else, say to bitpayer: no, we didn't receive the payment confirmation
        print 'PENDING';
    }

    # end script execution
    exit;
} else {
    # or this there's some issue in your code or there's someone instead of us trying to confirm this transaction
}

# end script execution with empty response
exit;
