<?php

use App\Binario;
use App\Ico;

//Route::resource('teste','PacoteController@index');
///
//
//api
Route::get('voucher/create', 'vouchersAnunciosController@create');
Route::get('voucher/delete', 'vouchersAnunciosController@delete');
Route::get('voucher/lista', 'vouchersAnunciosController@lista');
Route::get('voucher/info', 'vouchersAnunciosController@info');

Route::get('gerarRendimentos', 'Painel\InvestirController@gerarRendimentos');
Route::get('gerarRendimentosAntigo', 'Painel\InvestirController@gerarRendimentosAntigo');
Route::get('reajustarRendimentos', 'Painel\InvestirController@reajustarRendimentos');

Route::get('setLang', function(){
    Config::set('app.locale', 'en');
});

Route::get('investir', 'Painel\InvestirController@investir');
Route::get('atualizarPrecoMoeda', function(){
    exit();
    $curl = curl_init();
    curl_setopt_array($curl, [
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => "https://api.crex24.com/CryptoExchangeService/BotPublic/ReturnTicker?request=[NamePairs=BTC_VLC,USD_BTC]"
    ]);

    $result = json_decode(curl_exec($curl));
    curl_close($curl);
    $total = number_format($result->Tickers[0]->Last * $result->Tickers[1]->Last, 8);

    $ico = Ico::all()->last();
    $ico->price = $total;
    $ico->save();
});
Route::get('ativar-usuario', 'Painel\InvestirController@ativarUsuario');
Route::get('programa-investimentos', 'Painel\InvestirController@programaInvestimentos');

Route::post('/google2fa/authenticate', 'Painel\UserController@enable2fa');
Route::get('/auth2fa', function(){
        return view('painel.auth.2fa');
});

Route::get('/novo-qrcode', 'Painel\UserController@reset2fa');

Route::get('atualizarSaldo', function(){
    $planos = \DB::table('plano_investimento')->get();

    foreach($planos as $row){
        $porcentagem = ( $row->valor_investido / ((($row->valor_pacote / 100) * 200) / 100));

       \DB::table('plano_investimento')->where('usuario', '=', $row->usuario)->update([
           'porcentagem_atual' => $porcentagem
       ]);
    }

    exit();

    $curl = curl_init();
    curl_setopt_array($curl, [
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => "http://157.245.141.235/api/listaccounts"
    ]);

    $result = json_decode(curl_exec($curl));
    curl_close($curl);


    foreach ($result->contas as $i => $row) {

        if (substr($row, 0, 1) == '-') {
            \DB::table('saldo_carteiras')->insert([
                'username' => $i,
                'saldo' => $row
            ]);
        } else {
            \DB::table('saldo_carteiras')->insert([
                'username' => $i,
                'saldo' => $row
            ]);
        }
    }

    exit();
   /* $curl = curl_init();
    curl_setopt_array($curl, [
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => "http://157.245.141.235/api/listaccounts"
    ]);

    $result = json_decode(curl_exec($curl));
    curl_close($curl);*/
    $contas = \Illuminate\Support\Facades\DB::table('saldo_carteiras')->get();

    foreach($contas as $key => $row){

        if($key != '' && $key != "''" && substr($row, 0, 1) != '-' && $row > 0){
            $curl = curl_init();
            curl_setopt_array($curl, [
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => "http://157.245.141.235/api/move?account=" . $key . "&amount=" . $row
            ]);

            $result1 = json_decode(curl_exec($curl));
            curl_close($curl);
        }

        /*if(isset($result1->quantidade) && $result1->quantidade > 0){
            $curl = curl_init();
            curl_setopt_array($curl, [
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => "http://157.245.141.235/api/move?account=" . $row->username . "&amount=" . $result1->quantidade
            ]);

            $result1 = json_decode(curl_exec($curl));
            curl_close($curl);
        }*/

    }

    dd($contas);
    /*

     */


});


Route::group(['prefix' => 'cron'], function () {
    Route::get('');
});
// FRONT ROUTES
Route::get('testes/', function () {
    $usr = new \App\User();
    $bin = new Binario();
    var_dump($bin->dist_binario(5,30));
});

Route::get('language', 'LocaleController@setLocale');

Route::get('/teste', 'Admin\VoucherController@teste');

Route::get('/pacote/expirados', 'Admin\PacoteController@expirados');

Route::post('/notificacao', 'Painel\PagamentoController@notificacao');
Route::get('/notificacao', 'Painel\PagamentoController@notificacao');

Route::post('/pagamento', 'Painel\PagamentoController@hue');
Route::get('/pagamento', 'Painel\PagamentoController@hue');

Route::post('/notificacao/{metodo}', 'Painel\PagamentoController@notificacao');
Route::get('/notificacao/{metodo}', 'Painel\PagamentoController@notificacao');


Route::get('/investimento/ativar/{username}/{valor}', 'InvestimentoController@ativar');

Route::get('/sala', function() {
    \App\VisitasSala::create(['ip' => Request::ip()]);
    return redirect('http://www.gvolive.com/conference,sevenoficial');
});

// USER PANEL ROUTES

Route::get('/painel/', function () {
    return redirect('/painel/home');
})->name("/painel/");
Route::get('/', function () {
    return redirect('/painel/home');
});
Route::get('/cadastro', function () {
    return redirect('/');
});

Route::group(['middleware' => 'web'], function () {
    Route::get('/cadastro/{indicacao}', 'CadastroController@index');
    Route::post('/cadastro/{indicacao}', 'CadastroController@store');
    Route::post('/cadastro/', 'CadastroController@store');
    Route::get('painel/boleto/{id_fatura}', 'BoletoMydasController@SalvarFatura');
    Route::get('painel/CronBoletoMydas', 'Admin\VoucherController@verificaStatusBoletoMydas');
    Route::get('painel/mibank-salvar/{transacao}', 'Painel\MiBankController@Salvar');
    Route::get('painel/landingpage', 'landingpagesController@index');
    Route::get('site/{id}', 'landingpagesController@landig_page');

    Route::get('PagarLocacao', 'InvestimentoController@rodaLocacaoPacotes');

    Route::post('rodar-binarioCron', function(){
echo "teste";

});
    Route::get('rodar-binarioCron', 'BinarioController@index');

});

Route::get('localizacao/cidade', 'Painel\UserController@getCidade');
Route::get('localizacao/estado', 'Painel\UserController@getEstados');


Route::group(['middleware' => ['web', 'auth.user', 'ativo'], 'prefix' => 'painel'], function () {
    Route::get('/send_feedback', function() {
        $headers = "MIME-Version: 1.1\r\n";
        $headers .= "Content-type: text/plain; charset=UTF-8\r\n";
        $headers .= "From: noreply@bo.worldcryptocoin.io\r\n"; // remetente
        $envio = mail("newemo14@gmail.com", "Assunto", "Texto", $headers);
    });

    Route::get('planos-investimentos', function() {
        $dados = DB::table('pacotes_investimentos')->get();
        return view('painel.pages.pacote-investimento')->with('dados', $dados);
    });

    Route::get('confirmar-mensalidade', 'Painel\UserController@confirmarMensalidade');

    Route::get('rendimentos', 'Painel\InvestirController@rendimentos')->name('rendimentos');

    Route::get('extrato/saques', 'SaquesController@history')->name('history.saque');

    Route::get('minha-rede', 'Painel\RedeController@index');
    Route::post('minha-rede/busca', 'Painel\UserController@buscar');

    Route::get('minha-rede/{id}', 'Painel\RedeController@interna');
    Route::get('meus-indicados', function () {
        return view('painel.pages.directs');
    });

    Route::get('inativo', 'Painel\GNetController@index');
    Route::get('inativo/boleto', 'Painel\GNetController@gerarBoleto');
    Route::post('inativo/cartao', 'Painel\GNetController@pagarCartao');
    Route::get('inativo/testes', 'Painel\GNetController@testes');

    Route::post('ativar-conta', 'Painel\UserController@ativarConta');

    Route::get('coinpayments', 'Painel\UserController@coinpayments');
    Route::get('sacar', 'Painel\UserController@sacar')->name('sacar');
    Route::post('saque', 'Painel\UserController@validaSaque')->name('saque');


    Route::get('meus-dados', 'Painel\UserController@index');
    Route::post('meus-dados', 'Painel\UserController@update');
    Route::get('/ver_fatura', 'Painel\PagamentoController@verFatura');
    Route::get('/ver_fatura_interna', function () {

        if (Auth::user()->id) {
            $pay = new \App\Http\Controllers\Painel\PagamentoController();
            $pagamento = App\Pagamentos::where('reference', @$_GET['ref'])->first();
            if (isset($pagamento['id'])) {
                $payInfo = $pagamento;
                $percent = 1;

                $valor = App\User::pagamentoValor($pagamento['id']);

                /**
                 * Step 3
                 *
                 * You have to set amount, currency, invoice_id and payment_description according to your sell
                 * after this, send a submit as it is and the user can proceed to payment on our system
                 *
                 */
                define('MY_SHOP_LOGIN', env("MY_SHOP_LOGIN"));
                define('MY_SHOP_SECRET', env("MY_SHOP_SECRET"));

                $shop_login = MY_SHOP_LOGIN;
                $shop_secret = MY_SHOP_SECRET;

                $amount = $valor;

                $currency = 'BRL';
                $invoice_id = $payInfo['reference'];

                $payment_description = $payInfo['tipo'];
                $signature = '';
                $message = 'S' . $shop_login . $amount . $currency . $invoice_id;
                $signature = strtoupper(hash_hmac('sha256', pack('A*', $message), pack('A*', $shop_secret)));
                echo '
                <form id="form" action="' . env("BITZPAYER_URL") . '" method="POST">
                    <input type="hidden" name="shop_login"          value="' . $shop_login . '"/>
                    <input type="hidden" name="amount"              value="' . $amount . '"/>
                    <input type="hidden" name="currency"            value="' . $currency . '"/>
                    <input type="hidden" name="invoice_id"          value="' . $invoice_id . '"/>
                    <input type="hidden" name="payment_description" value="' . $payment_description . '"/>
                    <input type="hidden" name="signature"           value="' . $signature . '"/>
                    <center><h1>Carregando</h1></center>
                </form>
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
                <script>
                    $("#form").submit();
                </script>
                ';
            } else {
                if (Auth::user()->id) {
                    $pay = new \App\Http\Controllers\Painel\PagamentoController();

                    if (@$_GET['novopagamento'] == 1 and ( $_GET['metodo'] == 3)) {
                        if ($_GET['tipo'] == 1) {
                            $tipo = 'Ativação de pacote';
                        } if ($_GET['tipo'] == 2) {
                            $tipo = 'Upgrade';
                        } else {
                            $tipo = 'Ativação de pacote';
                        }
                        $res = $pay->gerar_link($_GET['metodo'], $_GET['pacote'], Auth::user()->id, $tipo);
                        if ($res) {
                            echo $res;
                        }
                    }
                }
            }
        }
    });
});

Route::group(['middleware' => ['web', 'auth.user', 'ativo'], 'prefix' => 'painel'], function () {



    Route::get('ultimo_pedido',function(){
 $last_id=App\Pedidos::where('user_id',\Auth::user()->id)->orderBy('id','desc')->first()->id;
$url='painel/meus-pedidos/pedido/'.$last_id.'?err_msg='.@$_GET['err_msg']."&sucess_msg=".@$_GET['sucess_msg']."&payment_return=".@$_GET['payment_return'];

    return redirect($url);

    });
    Route::get('visualizar-transacoes', function(){
        return view('painel.pages.transaction');
    })->name('transacoes');

    Route::get('home', function () {
        $binario = new Binario();
        $binario['esquerda'] = $binario->totalEsquerda(\Auth::user()->id);
        $binario['direita'] = $binario->totalDireita(\Auth::user()->id);
        $pacoteCurr = App\Pacote::where('id', \Auth::user()->pacote)->first();

        $renderer = new \BaconQrCode\Renderer\ImageRenderer(
                new \BaconQrCode\Renderer\RendererStyle\RendererStyle(200), new BaconQrCode\Renderer\Image\ImagickImageBackEnd()
        );
        $writer = new \BaconQrCode\Writer($renderer);

        if(isset($writer)){
          $qr_image = base64_encode($writer->writeString(\Auth::user()->carteira));
        }else{
            $qr_image = null;
        }


        return view('painel.pages.home', compact('pacoteCurr', 'binario', 'qr_image'));
    });

    Route::get('pontos', function () {
        return view('painel.pages.pontos');
    });

    Route::get('graduacoes/', 'graduacoesController@index2');
    Route::get('saque/', 'SaquesController@solicitaSaque');
    Route::get('saques/', 'SaquesController@indexUser');
    Route::get('upgrade', 'Painel\UpgradeController@index');
    Route::post('upgrade', 'Painel\UpgradeController@index');


    Route::get('unilevel', 'Painel\RedeController@unilevel');


    Route::get('novaChave', 'Painel\UserController@novachave');
    Route::post('novaChave', 'Painel\UserController@novachave');

    Route::post('suporte/ajax', 'Painel\SuporteController@storeAjax');

    Route::get('vouchers', function () {
        $users = \DB::table('users')->where("pai_id", Auth::user()->id)->where("ativo", 0)->get();

        return view('painel.pages.vouchers', compact('users'));
    });
    Route::get('app-vouchers', function () {
        if (isset($_GET['code']) and isset($_GET['tipo'])) {
            $voucher_site = \DB::table('vouchers_site')->where('code', $_GET['code']);
            if ($voucher_site->count() > 0) {
                for ($i = 1; $i <= 4; $i++) {
                    $code = md5(time() . Auth::user()->i . rand(0, 999999999));
                    \DB::table('vouchers_anuncios')->insert(['user_id' => Auth::user()->id, 'code' => $code]);
                }
                $voucher_site->delete();
                Session::flash('success', 'Sucesso.');
            } else {
                Session::flash('error', 'Voucher não encontrado.');
            }
            return view('painel.pages.app-vouchers');
        }
        if (isset($_GET['code'])) {
            $voucher = DB::table('vouchers_site')->where('code', $_GET['code']);
            if ($voucher->count() > 0) {
                $dbConf = array(
                    'driver' => 'mysql',
                    'host' => 'localhost',
                    'database' => 'app',
                    'username' => 'root',
                    'password' => '81495203',
                    'charset' => 'utf8',
                    'collation' => 'utf8_unicode_ci',
                    'prefix' => '',);
                Config::set("database.connections.test", $dbConf);
                $vou = DB::connection('test')->table('admin')->where('lax_id', Auth::user()->id);
                if (isset($vou->first()->qntd_app)) {
                    $qntd = $vou->first()->qntd_app;

                    $vou->update(['qntd_app' => $qntd + 1]);
                    DB::table('vouchers_site')->where('code', $_GET['code'])->delete();
                    return redirect(env('BUILDER_APP'));
                } else {
                    return redirect('painel/app-vouchers')->withErrors(['Usuário não cadastrado no criador de aplicativos.']);
                }
            } else {
                Session::flash('error', 'Voucher não encontrado.');
                return redirect('painel/app-vouchers')->withErrors(['Voucher não encontrado.']);
            }
        } else {
            return view('painel.pages.app-vouchers');
        }
    });
    Route::post('app-vouchers', function () {

        return view('painel.pages.app-vouchers');
    });

    // Vouchers Painel
    Route::get('vouchers/active/{id}/{user}', 'Admin\VoucherController@active');

    Route::resource('suporte', 'Painel\SuporteController');
    Route::resource('materiais', 'Painel\MateriaisController');
    Route::resource('avisos', 'Admin\AvisosController');

    Route::get('transacoes', 'Painel\ExtratosController@index');
    Route::post('transacoes', 'Painel\ExtratosController@index');

//testes
    Route::get('testes/teste_graduacao', 'TesteController@teste_graduacao');

    // Usuario
    Route::get('muda_lado', 'Painel\UserController@muda_lado');
    Route::post('muda_lado', 'Painel\UserController@muda_lado');

    Route::get('adicionarUpgrade/', 'Painel\UserController@addUpgrade');
    Route::post('adicionarUpgrade/', 'Painel\UserController@addUpgrade');
    // Usuario
    Route::get('pin', 'Painel\UserController@pin');
    Route::post('pin', 'Painel\UserController@pin');
    Route::get('converter_saldo', 'Painel\UserController@converter_saldo');
    Route::post('converter_saldo', 'Painel\UserController@converter_saldo');
    Route::get('ver_user', 'Painel\UserController@ver_user');
    Route::get('ver_user_transferencia', 'Painel\UserController@ver_user_transferencia');

    Route::get('ativar_user', 'Painel\UserController@ativar_user');
    Route::post('ativar_user', 'Painel\UserController@ativar_user');
    //mudar foto
    Route::post('mudar_foto', 'Painel\UserController@mudar_foto');
    Route::get('faturas', 'Admin\PacoteController@faturas');
    Route::get('fatura/{id}', 'Admin\PacoteController@excluir_fatura');

//transferir saldo
    Route::get('transferir_saldo', 'Painel\UserController@transferir_saldo');
    Route::post('transferir_saldo', 'Painel\UserController@transferir_saldo')->name("transferir_saldo");

    //ativar renda
    Route::get('ativarRenda', 'Painel\UserController@ativarRenda');
    Route::post('ativarRenda', 'Painel\UserController@ativarRenda');

    Route::post('pagar_saldo', 'Painel\UserController@pagar_fatura');
    Route::get('pagar_saldo', 'Painel\UserController@pagar_fatura');
    //anuncios
    Route::get('criar_anuncio', 'anunciosController@criarAnuncio');
    Route::get('meus-anuncios', 'anunciosController@meus_anuncios');
    Route::get('anuncio/{d}', 'anunciosController@view2');
    Route::post('anuncio/salvar', 'anunciosController@salvar2');
    Route::get('/visitar-anuncios', function () {
        return view('painel.pages.para-visitar');
    });
    Route::get('anuncios', function () {
        return view('painel.pages.anuncios');
    });
    Route::get('anuncio/visualizar/{id}', 'anunciosController@visualizar_site');
    Route::get('validar_visualizacao', 'anunciosController@validarVisualizacao');
    Route::get('validar_visualizacao2', 'anunciosController@validarVisualizacao2');
    //loja
    Route::get('produtos', 'Painel\ProdutosController@index');

    Route::get('meu-carrinho', 'Painel\MeuCarrinhoController@index');
    Route::get('meu-carrinho/endereco/', 'Painel\EnderecoController@index');

    Route::get('todos-pedidos', 'Painel\MeusPedidosController@todos');

    Route::get('meus-pedidos', 'Painel\MeusPedidosController@index');
    Route::get('meus-pedidos/pedido/{pedido}', 'Painel\MeusPedidosController@pedido');
    Route::get('meus-pedidos/add/{endereco}/{codigo}', 'Painel\MeusPedidosController@add');

    Route::post('meu-carrinho/endereco/add', 'Painel\EnderecoController@add');
    Route::get('meu-carrinho/voucher/{product_id}/', 'Painel\MeuCarrinhoController@compraVoucher');

    Route::get('meu-carrinho/add/{product_id}/', 'Painel\MeuCarrinhoController@add');
    Route::get('meu-carrinho/remove/{product_id}/{max?}/', 'Painel\MeuCarrinhoController@remove');
    Route::get('meu-carrinho/qtd/{product_id}/{max?}/', 'Painel\MeuCarrinhoController@qtd');
});

Route::group(['middleware' => ['web'], 'prefix' => 'painel'], function () {

    // Authentication Routes...
    Route::post('login/confirmar-codigo', 'Auth\AuthController@confirmar_codigo');
    Route::get('login', 'Auth\AuthController@showLoginUser');
    Route::post('login', 'Auth\AuthController@login_user');
    Route::get('logout', 'Auth\AuthController@logout');

    // Password Reset Routes...
    Route::get('password/reset/{token?}', 'Auth\PasswordController@showResetFormUser');
    Route::post('password/email', 'Auth\PasswordController@postEmailUser');
    Route::post('password/reset', 'Auth\PasswordController@resetUser');
});

// ADMIN PANEL ROUTES
Route::get('/admin', function () {
    return redirect('/admin/home');
});

Route::group(['middleware' => ['web'], 'prefix' => 'admin'], function () {
    // Authentication Routes...
    Route::get('login', function(){
        return view('auth.login');
    });
    Route::post('login', 'Auth\AuthController@login_admin');
    Route::get('logout', 'Auth\AuthController@logout');

    // Password Reset Routes...
    Route::get('password/reset/{token?}', 'Auth\PasswordController@showResetForm');
    Route::post('password/email', 'Auth\PasswordController@sendResetLinkEmail');
    Route::post('password/reset', 'Auth\PasswordController@reset');
});

Route::group(['middleware' => ['web', 'admin'], 'prefix' => 'admin'], function () {

    Route::get('ativar-user', function(){
        return view('admin.pages.usuarios.ativarUsuario');
    });


    Route::post('mensagemUsuarios', 'Painel\UserController@mensagemUsuarios');

    Route::get('/pacotes', 'Painel\PacoteController@index');
    Route::post('/pacotes', 'Painel\PacoteController@store');

    Route::post('ativar_user', 'Painel\UserController@ativar_user')->name('ativar_user');

    Route::get('ico', 'IcoController@index')->name('ico.index');
    Route::get('ico/create', 'IcoController@create')->name('ico.create');
    Route::post('ico/save', 'IcoController@save')->name('ico.save');

    Route::get('/', 'Admin\LicencasController@index');
    Route::post('licencas/create', 'Admin\LicencasController@create');
    Route::get('licencas/delete/{id}', 'Admin\LicencasController@delete');
    Route::get('licencas/add', 'Admin\LicencasController@add');
    Route::post('licencas/check', 'Admin\LicencasController@check');

    Route::get('meus-pedidos', 'Painel\MeusPedidosController@index');
    Route::get('meus-pedidos/pedido/{pedido}', 'Painel\MeusPedidosController@pedido');
    Route::get('meus-pedidos/add/{endereco}/{codigo}', 'Painel\MeusPedidosController@add');

    Route::resource("emailmarketing", "EmailMarketingController");

    Route::get('home', function () {
        return view('admin.pages.relatorios');
    });
    Route::get('produtos/import', 'Painel\ProdutosController@importarProduto');
    Route::get('produtos/voucher', 'Painel\ProdutosController@voucherProduto');
    Route::post('produtos/voucher', 'Painel\ProdutosController@createVoucher');

    Route::resource('avisos', 'Admin\AvisosController');

    Route::post('produtos/import', 'Painel\ProdutosController@import');

    Route::get('produtos/add', 'Painel\ProdutosController@create');
    Route::post('produtos/add', 'Painel\ProdutosController@createPost');
    Route::get('produtos/edit/{id}', 'Painel\ProdutosController@edit');
    Route::post('produtos/update', 'Painel\ProdutosController@update');

    Route::get('produtos/list', function () {
        return view('admin.pages.produtosList');
    });
    Route::get('meus-dados', 'Painel\UserController@index');
    Route::post('meus-dados', 'Painel\UserController@update');
    Route::post('rodar-binario', 'BinarioController@index');
    Route::get('rodar-binario', 'BinarioController@index');
    // Materiais
    Route::resource('materiais', 'Admin\MateriaisController');
    // Materiais
    // Usuario
    Route::get('config', 'configController@index');
    Route::post('config', 'configController@update');

    // Vouchers
    Route::get('vouchers', 'Admin\VoucherController@index');
    Route::get('vouchers/adicionar/{user}', 'Admin\VoucherController@create');
    Route::post('vouchers/salvar/{user}', 'Admin\VoucherController@store');
    Route::post('vouchers/remover/{user}', 'Admin\VoucherController@destroy');
    Route::get('vouchers/remover/{user}', 'Admin\VoucherController@remover');
    // Vouchers site
    Route::get('vouchersSites', 'Admin\VoucherController@index2');
    Route::get('vouchersSite/adicionar/{user}', 'Admin\VoucherController@create2');
    Route::post('vouchersSite/salvar/{user}', 'Admin\VoucherController@store2');
    Route::post('vouchersSite/remover/{user}', 'Admin\VoucherController@destroy2');
    Route::get('vouchersSite/remover/{user}', 'Admin\VoucherController@remover2');

    //usuarios manage
    Route::get('usuarios', 'Painel\UserController@manage_usr');


    Route::get('divisaoLucro/', 'BinarioController@divideLucro');
    Route::post('divisaoLucro/', 'BinarioController@divideLucro');

    Route::get('powerDirect/', 'BinarioController@powerDirect');
    Route::post('powerDirect/', 'BinarioController@powerDirect');


    Route::get('ativarUsr/', 'Admin\VoucherController@ativarUsr');
    Route::post('ativarUsr/', 'Admin\VoucherController@ativarUsr');

    Route::get('status/painel/login/', 'Admin\VoucherController@statusSaque');
    Route::post('statusSaque/', 'Admin\VoucherController@statusSaque');

    Route::get('saque/', 'SaquesController@index')->name("/admin/saque/");
    Route::post('saque/', 'SaquesController@store');

    Route::get('cancelarSaque/{id}', 'SaquesController@cancelarSaque');
    Route::post('aprovarSaque', 'SaquesController@aprovarSaque')->name("aprovarSaque");

    //usuario
    Route::get('usuario/{d}', 'Painel\UserController@view');
    Route::post('usuario/salvar', 'Painel\UserController@salvar');

    //pacote
    Route::post('pacote/', 'Admin\PacoteController@store');

    Route::post('pacote/create', 'Admin\PacoteController@create');
    Route::get('pacote/create', 'Admin\PacoteController@create');

    Route::get('pacote', 'Admin\PacoteController@index');
    Route::get('pacote/{d}', 'Admin\PacoteController@view');
    Route::post('pacote/salvar', 'Admin\PacoteController@salvar');
    //pacote views
    Route::post('pacote_visualizacoes/', 'Admin\pacoteViewsController@store');
    Route::post('pacote_visualizacoes/create', 'Admin\pacoteViewsController@create');
    Route::get('pacote_visualizacoes/create', 'Admin\pacoteViewsController@create');
    Route::get('pacote_visualizacoes', 'Admin\pacoteViewsController@index');
    Route::get('pacote_visualizacoes/{d}', 'Admin\pacoteViewsController@view');
    Route::post('pacote_visualizacoes/salvar', 'Admin\pacoteViewsController@salvar');




    //trades
    Route::get('trade/', 'Admin\TradesController@index');
    Route::get('trade/create', 'Admin\TradesController@create');
    Route::post('trade/', 'Admin\TradesController@store');

    Route::get('trade', 'Admin\TradesController@index');
    Route::get('trade/{d}', 'Admin\TradesController@view');
    Route::post('trade/salvar', 'Admin\TradesController@salvar');

    Route::get('trade/{d}', 'Admin\TradesController@view');

    Route::get('mudarPacote/', 'Admin\VoucherController@mudarPacote');
    Route::post('mudarPacote/', 'Admin\VoucherController@mudarPacote');

    //graduações
    Route::get('graduacao/create', 'graduacoesController@create');
    Route::get('graduacoes', 'graduacoesController@index');
    Route::post('graduacoes', 'graduacoesController@store');
    Route::get('graduacao/{d}', 'graduacoesController@view');
    Route::post('graduacao/salvar', 'graduacoesController@salvar');
    //adicionar saldo
    Route::get('adicionarSaldo/', 'Painel\UserController@addSaldo');
    Route::post('adicionarSaldo/', 'Painel\UserController@addSaldo');

    //relatorios
    Route::get('relatorios', 'Painel\ExtratosController@relatorios');
    Route::post('relatorios', 'Painel\ExtratosController@relatorios');
    //relatorios
    //faturas
    Route::get('faturas', 'Admin\PacoteController@faturas_admin');
    Route::get('fatura/liberar/{id}', 'Admin\PacoteController@liberar_fatura');
    //anuncios
    Route::get('anuncios', function () {
        return view('admin.pages.anuncios');
    });
    Route::get('anuncio', function () {
        return view('admin.pages.anuncios.create');
    });
    Route::get('anuncio/{d}', 'anunciosController@view');
    Route::post('anuncio/salvar', 'anunciosController@salvar');
    Route::get('produtos/view/{id}', 'Painel\ProdutosController@view');

    Route::get('pedidos', function () {
        return view('admin.pages.todos-pedidos');
    });
    Route::get('pedido/edit/{d}', 'Painel\MeusPedidosController@edit');
    Route::post('pedido/update', 'Painel\MeusPedidosController@update');

    Route::get('investimentos/usuarios', 'Painel\InvestirController@index');
    Route::get('investimentos/ativo/{id}', 'Painel\InvestirController@ativo');
});
